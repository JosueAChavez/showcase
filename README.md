# Welcome! #

This showcase is meant to display some of the projects that I've been working on in school.

### Cache Organization Simulator ###
* A project for my computer architecture class
* A cache simulator that uses memory reference traces to replicates movement of data in and out of the cache
* Has adjustable cache properties (size, type, associativity)
* Project was written in C

### MeeshQuest (Mapquest-esque Application) ###
* A mapping project based on topics covered in my data structures class
* Uses various spatial data structures (PR, PM Quadtrees)
* Data dictionary built using B+ Tree
* Built application using MVC design pattern
* Input and output are given as XML
* Features of the program include:
	* Map image drawing of a general area
	* Calculating shortest routes (based on time or distance)
	* Generating driving instructions 
	* Displaying a highlighted route
	* Determining the closest points of interest within a radius of a given point

### MIPS Integer/Floating Point Pipeline ###
* Another project for my computer architecture class
* An in-order, cycle-accurate simulator of a MIPS processor
* Project was written in C