
/*
 * 
 * pipeline.h
 * 
 * Donald Yeung
 */

#define TRUE 1
#define FALSE 0


/* fetch/decode pipeline register */
typedef struct _if_id_t {
	int instr;
	unsigned long pc;
} if_id_t;


/* Register state */
typedef struct _rf_int_t {
	int_t reg_int[NUMREGS];
} rf_int_t;

typedef struct _rf_fp_t {
	float reg_fp[NUMREGS];
} rf_fp_t;


/* Overall processor state */
typedef struct _state_t {
	/* memory */
	unsigned char mem[MAXMEMORY]; /* using unsigned because want it to be 8 bits long */

	/* register files AKA arrays (32) */
	rf_int_t rf_int;
	rf_fp_t rf_fp;

	/* pipeline registers */
	unsigned long pc;
	if_id_t if_id;

	fu_int_t *fu_int_list;
	fu_fp_t *fu_add_list;
	fu_fp_t *fu_mult_list;
	fu_fp_t *fu_div_list;

	wb_t int_wb;
	wb_t fp_wb;

	int fetch_lock;

	/* scoreboards */
	int sb_int[NUMREGS + 1], sb_fp[NUMREGS + 1];

} state_t;

extern state_t *state_create(int *, FILE *, FILE *);

extern int writeback(state_t *, int *, int halt_flag);
extern void execute(state_t *);
extern int decode(state_t *);
extern void fetch(state_t *);

int being_used(int *sb, int reg);
int waw_check(int *sb, int reg, int num_cycles);
int to_write(int sb[]);

void perform_evaluation(state_t *state, int instr, unsigned long pc, int reg);
void print_sb(int *sb, int is_int);
void stall(state_t *state);

void update_int_rf(int_t *rf, int key, operand_t value);
void update_fp_rf(float *rf, int key, operand_t value);

extern void update_sb(int *sb, int loc, int *done, int *sb2);
void print_instruction(int instr);
int same_cycle(int *sb, int num_cycles);