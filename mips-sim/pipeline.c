
/* pipeline.c
 * 
 * Josue A. Chavez
 * ENEE 446
 * The control of the pipeline. */

#include <stdlib.h>
#include <string.h>
#include "fu.h"
#include "pipeline.h"

int hop_flag = 0;

int writeback(state_t *state, int *num_insn, int halt_flag) {

	int int_reg, fp_reg, is_fp_done, is_int_done;
	/*operand_t result;*/

	/* pull out the registers that need to be written, if any */
	/* console */
	/*printf("WB: to write for int\n");*/
	int_reg = to_write(state->sb_int);
	/*printf("WB: to write for fp\n");*/
	fp_reg = to_write(state->sb_fp);

	/* show the scoreboard */
	/*print_sb(state->sb_int, 1);*/ /* console */
	/*print_sb(state->sb_fp, 0);*/ /* console */

	/* case for l.s and s.s cause they use int_fu but store in fp regs*/
	if(state->fp_wb.instr == 0 && fp_reg != -1) {
		state->fp_wb.instr = state->int_wb.instr;
		state->int_wb.instr = 0;
	}

	/* perform the operation */
	perform_evaluation(state, state->int_wb.instr, state->if_id.pc, int_reg);
	perform_evaluation(state, state->fp_wb.instr, state->if_id.pc, fp_reg);

	/* write result of instruction in int_wb or fp_wb to RF */
	if (int_reg != -1) {
		(*num_insn)++;
		state->int_wb.instr = 0;
	}

	if (fp_reg != -1) {
		(*num_insn)++;
		state->fp_wb.instr = 0;
	}

	if (halt_flag == TRUE) {
		is_fp_done = fu_fp_done(state->fu_add_list) && 
			fu_fp_done(state->fu_mult_list) &&
			fu_fp_done(state->fu_div_list);
		is_int_done = fu_int_done(state->fu_int_list);

		return is_int_done && is_int_done && hop_flag++;
	}

	return FALSE;

}


void execute(state_t *state) {

	int done = 0;
	/* advance all FUs one cycle */
	advance_fu_int(state->fu_int_list, &state->int_wb, state->sb_int, &done, state->sb_fp);
	advance_fu_fp(state->fu_add_list, &state->fp_wb, state->sb_fp, &done, state->sb_fp);
	advance_fu_fp(state->fu_mult_list, &state->fp_wb, state->sb_fp, &done, state->sb_fp);
	advance_fu_fp(state->fu_div_list, &state->fp_wb, state->sb_fp, &done, state->sb_fp);
}


int decode(state_t *state) {

	const op_info_t *op_info;
	int use_imm, instr, ls_flag = 0;

	operand_t o1, o2, res;

	fu_int_t *int_curr = state->fu_int_list;
	fu_fp_t *fp_add_curr = state->fu_add_list,
	*fp_mult_curr = state->fu_mult_list,
	*fp_div_curr = state->fu_div_list;

	int int_cycles = 2,
	fp_add_cycles = 3,
	fp_mult_cycles = 4,
	fp_div_cycles = 8;

	int *sb, reg, cycles, is_fp = FALSE, r1, r2, df;
	int control_flag = 0, raw_flag = 0, waw_flag = 0;

	fu_fp_t *fu_fp_list;
	fu_int_t *fu_int_list;

	/* examine instruction in if_id */
	instr = state->if_id.instr;
	
	/* decode instruction */
	op_info = decode_instr(instr, &use_imm);

	/* determine whether or not to issue to FU */

	/* check if it has hazards BY checking what kind of instruc we have */
	/* issues if there aren't any hazards */
	switch(op_info->fu_group_num) {
		case FU_GROUP_INT: /* Integer Arithmetic */
			switch(op_info->operation) {
				case OPERATION_ADD:
				case OPERATION_SUB:
				case OPERATION_SLL:
				case OPERATION_SRL:
				case OPERATION_AND:
				case OPERATION_OR:
				case OPERATION_XOR:
				case OPERATION_SLT:
				case OPERATION_SGT:
				case OPERATION_ADDU:
				case OPERATION_SUBU:
				case OPERATION_SLTU:
				case OPERATION_SGTU:
					sb = state->sb_int;

					if(use_imm) {
						reg = FIELD_R2(instr);
						r1 = FIELD_R1(instr);
						raw_flag = being_used(sb, reg) || being_used(sb, r1);
					} else {
						reg = FIELD_R3(instr);
						r1 = FIELD_R1(instr);
						r2 = FIELD_R2(instr);
						raw_flag = being_used(sb, reg) || being_used(sb, r1) ||
							being_used(sb, r2);
					}

					cycles = int_cycles;
					fu_int_list = state->fu_int_list;
				break;
			}
		break;
		case FU_GROUP_ADD: /* Floating Point Arithmetic */
				/* TODO group them just have diff fu_fp_list
				sb = state->sb_fp;
				reg = FIELD_R3(instr);
				r1 = FIELD_R1(instr);
				r2 = FIELD_R2(instr);
				cycles = fp_add_cycles;
				is_fp = TRUE; 
				raw_flag = being_used(sb, reg) || being_used(sb, r1) ||
							being_used(sb, r2);*/
			if (op_info->operation == OPERATION_ADD) {
				sb = state->sb_fp;
				reg = FIELD_R3(instr);
				r1 = FIELD_R1(instr);
				r2 = FIELD_R2(instr);
				cycles = fp_add_cycles;
				fu_fp_list = state->fu_add_list;
				is_fp = TRUE; 
				raw_flag = being_used(sb, reg) || being_used(sb, r1) ||
							being_used(sb, r2);
			}else if (op_info->operation == OPERATION_SUB){
				sb = state->sb_fp;
				reg = FIELD_R3(instr);
				cycles = fp_add_cycles;
				r1 = FIELD_R1(instr);
				r2 = FIELD_R2(instr);
				fu_fp_list = state->fu_add_list;
				is_fp = TRUE;
				raw_flag = being_used(sb, reg) || being_used(sb, r1) ||
							being_used(sb, r2);
			}
		break;
		case FU_GROUP_MULT:
			if (op_info->operation == OPERATION_MULT) {
				sb = state->sb_fp;
				reg = FIELD_R3(instr);
				cycles = fp_mult_cycles;
				r1 = FIELD_R1(instr);
				r2 = FIELD_R2(instr);
				fu_fp_list = state->fu_mult_list;
				is_fp = TRUE;
				raw_flag = being_used(sb, reg) || being_used(sb, r1) ||
							being_used(sb, r2);
			}
		break;
		case FU_GROUP_DIV:
			if (op_info->operation == OPERATION_DIV) {
				sb = state->sb_fp;
				reg = FIELD_R3(instr);
				r1 = FIELD_R1(instr);
				r2 = FIELD_R2(instr);
				cycles = fp_div_cycles;
				fu_fp_list = state->fu_div_list;
				is_fp = TRUE;
				raw_flag = being_used(sb, reg) || being_used(sb, r1) ||
							being_used(sb, r2);
			}
		break;
		case FU_GROUP_MEM:
			switch(op_info->data_type) {
				case DATA_TYPE_W:
					sb = state->sb_int;
					reg = FIELD_R2(instr);
					r1 = FIELD_R1(instr);
					cycles = int_cycles;
					fu_int_list = state->fu_int_list;
					raw_flag = being_used(sb, reg) || being_used(sb, r1);
				break;
				case DATA_TYPE_F:
					sb = state->sb_fp;
					reg = FIELD_R2(instr);
					r1 = FIELD_R1(instr);
					cycles = int_cycles;
					fu_int_list = state->fu_int_list;
					
					raw_flag = being_used(sb, reg) || being_used(state->sb_int, r1);
					ls_flag = same_cycle(state->sb_fp, cycles);
				break;
			}
		break;
		case FU_GROUP_BRANCH:
			control_flag = 1;

			cycles = int_cycles;
			fu_int_list = state->fu_int_list;
			switch(op_info->operation) {
				case OPERATION_J:
				case OPERATION_JAL:
					sb = state->sb_int;
					reg = NUMREGS;
				break;

				case OPERATION_JR:
				case OPERATION_JALR:
				case OPERATION_BEQZ:
				case OPERATION_BNEZ:
					sb = state->sb_int;
					reg = FIELD_R1(instr);
					
					raw_flag = being_used(sb, reg);
				break;
			}
		break;
		case FU_GROUP_HALT:
			/*empty the pipeline by finishing remaining instructions*/
			state->fetch_lock = TRUE;
			return 1;
		case FU_GROUP_NONE:
			return 0;
		case FU_GROUP_INVALID:
			fprintf(stderr, "decode error: invalid opcode (instr = %d)\n", instr);
			return 0;
	}


	/* RAW hazard */
	if (raw_flag) {
		/* console */
		/*printf("ISSUE: RAW ");*/
		stall(state);
	} else {
		/* console */
		/*printf("is_fp %d ls_flag %d\n", is_fp, ls_flag);*/
		/* structural hazard */
		if (is_fp == TRUE){
			if(ls_flag) {
				/* console */
				/*printf("ISSUE: FP Structural ");*/
				stall(state);
			} else if (waw_check(sb, reg, cycles)){
				/* console */
				/*printf("ISSUE: FP WAW ");*/
				stall(state);
			} else if (issue_fu_fp(fu_fp_list, instr) == -1) {
				stall(state);
			} else {
				sb[reg] = cycles;
				/* console */
				/*printf("ISSUE: F%d's cycle length: %d \n", reg, sb[reg]);*/
			}
		} else {
			if (ls_flag == 1) {
				/*printf("ISSUE: FP 2 Structural ");*/
				stall(state);
			}else if(issue_fu_int(fu_int_list, instr) == -1) {
				/*printf("ISSUE: INT Structural ");*/
				stall(state);
			} else {
				sb[reg] = cycles;
				/* console */
				/*printf("ISSUE: R%d's cycle length: %d \n", reg, sb[reg]);*/

				/* control hazazrd */
				if (control_flag) {
					/*set fetch_lock to true */
					state->fetch_lock = TRUE;
					/* console */
					/*printf("ISSUE: Locking the fetch\n");*/
				}
			}
		}
	}
		
	return 0;
}


void fetch(state_t *state) {

	/* go to memory */
	/* fetch the current instruction given by PC */
	/* put instruction into if_id */
	memcpy(&state->if_id.instr, &state->mem[state->pc], sizeof(unsigned long));
	
	/* make a copy of the current PC counter for control */
	state->if_id.pc = state->pc;
	state->pc += 4; 
}

/* ensure max of 1 int and 1 FP instruction WB is on same cycle*/
/* should be checked ONLY if dealing with FP registers */
	/* if((get_execute_cycles_remain() - 1) == latency(instr) && result goes to same RF) */\



/* okay since wb is performed before decode */
int being_used(int *sb, int reg) {

		/*printf("sb[%d]: %d\n", reg, sb[reg]);*/ /* console */
		return sb[reg] != -1;
}

/* pickup the register thats ready to be written */
int to_write(int sb[]) {
	int i;

	for(i = 0; i < NUMREGS + 1; i++) {
		/*printf("Value at sb[%d]: %d\n", i, sb[i]);*/ /* console */
		if (sb[i] == 0) {
			sb[i] = -1; /* reset it */
			return i;
		}
	}

		return -1;
}

/* Groups instructions by their types and prepares to send them
to perform_operation by evaluating thier values at that location */
void perform_evaluation(state_t *state, int instr, unsigned long pc, int reg) {

	const op_info_t *op_info;
	int use_imm; /* used to see if instruction is an immediate */
	operand_t result, operand1, operand2;

	op_info = decode_instr(instr, &use_imm);

	/* console */
	/*printf("WB: GIVEN INSTRUCTION: "); 
	printInstruction(instr);
	printf("\n");*/

	switch(op_info->fu_group_num) {
		case FU_GROUP_INT: /* Integer Arithmetic / Logic */
			
			operand1.integer = state->rf_int.reg_int[FIELD_R1(instr)];
			
			if(use_imm) {
				operand2.integer.w = FIELD_IMM(instr);
			} else {
				operand2.integer = state->rf_int.reg_int[FIELD_R2(instr)];
			}
			/* console */
			/*printf("WB: reg[R%d] <- %d op %d \n", reg, operand1.integer, operand2.integer);*/

			result = perform_operation(instr, pc, operand1, operand2);
			update_int_rf(state->rf_int.reg_int, reg, result);

			/*state->rf_int.reg_int[int_reg] = result.integer;*/
		break;
		case FU_GROUP_ADD: /* Floating Point Arithmetic */
		case FU_GROUP_MULT:
		case FU_GROUP_DIV:
			operand1.flt = state->rf_fp.reg_fp[FIELD_R1(instr)];
			operand2.flt = state->rf_fp.reg_fp[FIELD_R2(instr)];

			/* console */
			/*printf("WB: reg[F%d] <- %f op %f \n", reg, operand1.flt, operand2.flt);*/

			result = perform_operation(instr, pc, operand1, operand2);
			update_fp_rf(state->rf_fp.reg_fp, reg, result);
		break;
		case FU_GROUP_MEM: /* Memory */
			operand1.integer = state->rf_int.reg_int[FIELD_R1(instr)]; /* reg[r1] */
			operand2.integer.w = FIELD_IMM(instr); /* imm */

			result = perform_operation(instr, pc, operand1, operand2); /* reg[r1] + imm */
			switch(op_info->operation) {
				case OPERATION_LOAD: /* lw */
					/* console */
					/*printf("reg[rs1 = R%d]: %d\n", FIELD_R1(instr), operand1.integer);
					printf("imm: %d\n", operand2.integer.w);*/
					if(op_info->data_type == DATA_TYPE_W) {
						result.integer.wu = state->mem[result.integer.wu]; /* reg[rd] = Mem[reg[r1] + imm] */
						update_int_rf(state->rf_int.reg_int, reg, result);
					}else {
						/* reg[rd] = Mem[reg[r1] + imm] */
						memcpy(&result.flt, &state->mem[result.integer.w], 8);
						/* console */
						/*printf("WB: DATA AT MEM: %f\n", result.flt);*/
						update_fp_rf(state->rf_fp.reg_fp, reg, result);
					}
	 			break;
				case OPERATION_STORE:

					/* console */
					/*printf("WB: The value @ rf_int[%d]: %d\n", FIELD_R1(instr), operand1.integer);
					printf("WB: The value @ rf_int[%d]: %d\n", FIELD_R2(instr), state->rf_int.reg_int[FIELD_R2(instr)]);*/
					
					/*printf("WB: imm %d\n", operand2.integer.w);
					printf("WB: The value @ mem[imm = %d]: %d\n", operand2.integer.w, state->mem[operand2.integer.w]);*/

					/* console */
					/*printf("WB: mem[%d] = %d\n", result.integer.wu, state->rf_int.reg_int[FIELD_R2(instr)].wu);*/
					
					if(op_info->data_type == DATA_TYPE_W) {
						state->mem[result.integer.w]
							= state->rf_int.reg_int[FIELD_R2(instr)].wu; /* mem <- reg */
					} else {
						state->mem[result.integer.w]
							= state->rf_fp.reg_fp[FIELD_R2(instr)];
					}
				break;
			}
			
		break;

		case FU_GROUP_BRANCH: /* Control */

			/* control (not sure if right place) */
			/* if control is "taken" -> clear if_id and set pc to target address 
				state->pc = target_address;
				state->if_id = 0;?*/
			/* control */
			/*printf("WB: Unlocking the fetch\n");*/
			state->fetch_lock = FALSE;
			switch(op_info->operation) {
				case OPERATION_JAL:/* moved jal to top because same as j */
					state->rf_int.reg_int[31].wu = pc;
				case OPERATION_J:
					operand1.integer.w = FIELD_OFFSET(instr);

					state->pc = perform_operation(instr, pc, operand1, operand2).integer.w;
					state->int_wb.instr = 0; /* squash instruction */
					state->if_id.instr = 0; /* squash instruction in decode */
				break;

				case OPERATION_JALR:
					state->if_id.instr = 0;
					state->rf_int.reg_int[31].wu = state->pc;
				case OPERATION_JR:
					state->if_id.instr = 0;
					state->pc = state->rf_int.reg_int[FIELD_R1(instr)].wu;
				break;

				case OPERATION_BEQZ:
					operand1.integer = state->rf_int.reg_int[FIELD_R1(instr)];
					operand2.integer.w = FIELD_IMM(instr);

					/*console*/
					/*printf("WB: Evaluation %d == 0?\n", operand1.integer);*/

					result = perform_operation(instr, pc, operand1, operand2);

					if(!operand1.integer.w) {
						/* console */
						/*printf("WB: SQUASHING\n");*/
						state->if_id.instr = 0; /* squash instruction in decode */
						state->pc = result.integer.wu; /* set pc to target address */
					}
				break;
				case OPERATION_BNEZ:
					operand1.integer = state->rf_int.reg_int[FIELD_R1(instr)];
					operand2.integer.w = FIELD_IMM(instr);

					/*console*/
					/*printf("WB: Evaluation %d != 0?\n", operand1.integer);*/

					result = perform_operation(instr, pc, operand1, operand2);

					if(operand1.integer.w) {
						/* console */
						/*printf("--------WB: SQUASHING\n");*/
						state->if_id.instr = 0; /* squash instruction in decode */
						state->pc = result.integer.wu; /* set pc to target address */
					}

					/*state->pc = perform_operation(instr, pc, operand1, operand2).integer.wu;*/
				break;
			}
		break;

		case FU_GROUP_HALT:
		break;
		case FU_GROUP_NONE:
		break;
		case FU_GROUP_INVALID:
			fprintf(stderr, "perform eval error: invalid opcode (instr = %d)\n", instr);
		break;
	}
}


/* decreases cycles remaining in scoreboard */
void update_sb(int *sb, int instr, int *done, int *sb2) {

	const op_info_t *op_info;
	int use_imm, reg;

	op_info = decode_instr(instr, &use_imm);

	switch(op_info->fu_group_num) {
		case FU_GROUP_INT: /* Integer Arithmetic */
			switch(op_info->operation) {
			case OPERATION_ADD:
			case OPERATION_SUB:
			case OPERATION_SLL:
			case OPERATION_SRL:
			case OPERATION_AND:
			case OPERATION_OR:
			case OPERATION_XOR:
			case OPERATION_SLT:
			case OPERATION_SGT:
			case OPERATION_ADDU:
			case OPERATION_SUBU:
			case OPERATION_SLTU:
			case OPERATION_SGTU:
				/* console */
				if(use_imm) {
					reg = FIELD_R2(instr);
					/*printf("EXE SB(R%d): %d to %d \n", reg, sb[reg], (sb[reg]-1));*/
					sb[reg]--; /* I-Type */
				}else {
					reg = FIELD_R3(instr);
					/*printf("EXE SB(R%d): %d to %d \n", reg, sb[reg], (sb[reg]-1));*/
					sb[reg]--; /* R-Type */
				}
			break;
			}
		break;
		case FU_GROUP_ADD: /* Floating Point Arithmetic */
		case FU_GROUP_MULT:
		case FU_GROUP_DIV:
				reg = FIELD_R3(instr);
				/* console */
				/*printf("EXE SB(F%d): %d to %d \n", reg, sb[reg], (sb[reg]-1));*/
				sb[reg]--; /* R-Type */
		break;
		case FU_GROUP_MEM:
			switch(op_info->operation) {
				case OPERATION_LOAD: 
				case OPERATION_STORE:
					reg = FIELD_R2(instr);
					if(op_info->data_type == DATA_TYPE_W) {
						/* console */
						/*printf("EXE SB(%d): %d to %d \n", reg, sb[reg], (sb[reg]-1));*/
						sb[reg]--; /* I-Type */
					} else {
						/* console */
						/*printf("EXE SB2(%d): %d to %d \n", reg, sb2[reg], (sb2[reg]-1));*/
						sb2[reg]--; /* I-Type */
					}
					
				break;
			}
			
			break;
		case FU_GROUP_BRANCH:
			switch(op_info->operation) {
				case OPERATION_J:
				case OPERATION_JAL:
					reg = NUMREGS;
					/* console */
					/*printf("EXE SB(%d): %d to %d \n", reg, sb[reg], (sb[reg]-1));*/
					sb[reg]--; /* Fake-Type */
					*done = sb[reg] == 0;
				break;
				case OPERATION_JR:
				case OPERATION_JALR:
					reg = FIELD_R1(instr);
					/* console */
					/*printf("EXE SB(%d): %d to %d \n", reg, sb[reg], (sb[reg]-1));*/
					sb[reg]--; /* R-Type */
					*done = sb[reg] == 0;
				break;

				case OPERATION_BEQZ:
				case OPERATION_BNEZ:
					reg = FIELD_R1(instr);
					/* console */
					/*printf("EXE SB(%d): %d to %d \n", reg, sb[reg], (sb[reg]-1));*/
					sb[reg]--; /* I-Type */
					*done = sb[reg] == 0;
				break;
			}
			break;
		default:
			fprintf(stderr, "updatesb error: invalid opcode (instr = %d)\n", instr);
			break;
	}
}

void print_sb(int *sb, int is_int) {

	int i;

	printf("Scoreboard(");
	if(is_int) {
		printf("Integer): \n");
	}else {
		printf("Floating Point): \n");
	}

	for (i = 0; i < NUMREGS; i += 4) {
		printf("\tsb[%.2d] = %d\tsb[%.2d] = %d\tsb[%.2d] = %d\tsb[%.2d] = %d\n",
		i, sb[i],
		i+1, sb[i+1],
		i+2, sb[i+2],
		i+3, sb[i+3]);
	}
}

void stall(state_t *state) {
	/*printf("hazard from %d to %d \n", state->pc, state->if_id.pc);*/ /* console */
		 state->pc = state->if_id.pc;  /*make it fetch same instr*/
}

void update_int_rf(int_t *rf, int key, operand_t value) {
	rf[key] = value.integer;
	/*printf("WB: reg_int[R%d] = %d \n", key, value.integer);*/ /* console */
}

void update_fp_rf(float *rf, int key, operand_t value) {
	rf[key] = value.flt;
	/*printf("WB: reg_fp[F%d] = %f \n", key, value.flt);*/ /* console */
}

int waw_check(int *sb, int reg, int num_cycles) {
	return sb[reg] > num_cycles;
}

int same_cycle(int *sb, int num_cycles) {
	int i;
	for (i = 0; i < NUMREGS; i ++) {
		if (sb[i] == num_cycles) {
			return 1;
		}
	}

	return 0;
}