/* cache.c
 * Josue A. Chavez
 */


#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#include "cache.h"
#include "main.h"

/* cache configuration parameters */
static int cache_split = 0;
static int cache_usize = DEFAULT_CACHE_SIZE;
static int cache_isize = DEFAULT_CACHE_SIZE; 
static int cache_dsize = DEFAULT_CACHE_SIZE;
static int cache_block_size = DEFAULT_CACHE_BLOCK_SIZE;
static int words_per_block = DEFAULT_CACHE_BLOCK_SIZE / WORD_SIZE;
static int cache_assoc = DEFAULT_CACHE_ASSOC;
static int cache_writeback = DEFAULT_CACHE_WRITEBACK;
static int cache_writealloc = DEFAULT_CACHE_WRITEALLOC;

/* cache model data structures */
static Pcache icache;
static Pcache dcache;
static cache c1;
static cache c2;
static cache_stat cache_stat_inst = {0};
static cache_stat cache_stat_data = {0};

/************************************************************/
void set_cache_param(param, value)
	int param;
	int value;
{

	switch (param) {
	case CACHE_PARAM_BLOCK_SIZE:
		cache_block_size = value;
		words_per_block = value / WORD_SIZE;
		break;
	case CACHE_PARAM_USIZE:
		cache_split = FALSE;
		cache_usize = value;
		break;
	case CACHE_PARAM_ISIZE:
		cache_split = TRUE;
		cache_isize = value;
		break;
	case CACHE_PARAM_DSIZE:
		cache_split = TRUE;
		cache_dsize = value;
		break;
	case CACHE_PARAM_ASSOC:
		cache_assoc = value;
		break;
	case CACHE_PARAM_WRITEBACK:
		cache_writeback = TRUE;
		break;
	case CACHE_PARAM_WRITETHROUGH:
		cache_writeback = FALSE;
		break;
	case CACHE_PARAM_WRITEALLOC:
		cache_writealloc = TRUE;
		break;
	case CACHE_PARAM_NOWRITEALLOC:
		cache_writealloc = FALSE;
		break;
	default:
		printf("error set_cache_param: bad parameter value\n");
		exit(-1);
	}

}
/************************************************************/

/************************************************************/
/* initialize the cache, and cache statistics data structures */
void init_cache() {

	int i;

	/*printf("CONSOLE: -----init_cache-----\n");*/
	
	/* cache initialization */

	if (cache_split) {
		icache = &c1;
		dcache = &c2;

		icache->size = cache_isize / WORD_SIZE;
		icache->associativity = cache_assoc;
		icache->n_sets = cache_isize/(cache_block_size * icache->associativity);

		dcache->size = cache_dsize / WORD_SIZE;
		dcache->associativity = cache_assoc;
		dcache->n_sets = cache_dsize/(cache_block_size * dcache->associativity);

		construct_cache(icache);
		construct_cache(dcache);
	} else {
		icache = &c1;
		dcache = &c1;

		icache->size = cache_usize / WORD_SIZE;
		icache->associativity = cache_assoc;
		icache->n_sets = cache_usize/(cache_block_size * icache->associativity);

		construct_cache(icache);
	}
	/* cache statistics is already initalized */
}
/************************************************************/

/************************************************************/
/* handle an access to the cache */
void perform_access(addr, access_type)
	unsigned addr, access_type;
{
	int index;
	unsigned addr_tag;
	Pcache_line c_line, *head, *tail;
	Pcache_stat c_stat;
	Pcache c_cache;

	switch (access_type) {
		case TRACE_DATA_STORE:
		case TRACE_DATA_LOAD:
			c_cache = dcache;
			c_stat = &cache_stat_data;
		break;
		case TRACE_INST_LOAD:
			c_cache = icache;
			c_stat = &cache_stat_inst;
		break;
	}

	index = (addr & c_cache->index_mask) >> c_cache->index_mask_offset;
	c_line = c_cache->LRU_head[index];
	head = &c_cache->LRU_head[index];
	tail = &c_cache->LRU_tail[index];

	addr_tag = addr >> (c_cache->index_mask_offset + LOG2(c_cache->n_sets));

	if(c_line == NULL) { /* never filled */
		c_stat->misses++;
		
		if (access_type != TRACE_DATA_STORE || cache_writealloc) {
			/*printf("CONSOLE: Complusary Miss\n");*/
			c_cache->set_contents[index]++;

			c_line = create_node(addr_tag);
			insert(head, tail, c_line);

			c_stat->demand_fetches += words_per_block;
			/*printf("CONSOLE: New Cache Line #%d\n", index);*/
			
			write_hit(access_type, c_line, c_stat);
		} else {
			/*printf("CONSOLE: WNA NO CACHE FOR YOU\n");*/
			c_stat->copies_back += 1;
		}

	} else if(get_line(&c_line, addr_tag)) { 	/* hit */
		/*printf("CONSOLE: Hit\n");*/
		
		write_hit(access_type, c_line, c_stat);

		/* delete line in LRU list and put it on the top */
		delete(head, tail, c_line);
		insert(head, tail, c_line);

	} else {	/* miss */
		/*printf("CONSOLE: Miss\n");*/
		if (access_type != TRACE_DATA_STORE || cache_writealloc) { /* !wna */
			if (c_cache->set_contents[index] < c_cache->associativity) {
				c_cache->set_contents[index]++;
				/*printf("CONSOLE: New Cache Line #%d\n", c_cache->set_contents[index]);*/
				
				c_line = create_node(addr_tag);
				insert(head, tail, c_line);

			} else {
				/*print_line(c_cache->LRU_head[index]);*/
				c_line = c_cache->LRU_tail[index];
				/*printf("\nCONSOLE: Evicting %-12x from LRU\n", c_line->tag);*/

				if(c_line->dirty) {
					/*printf("CONSOLE: Dirty cache line copied to MM\n");*/
					c_stat->copies_back += words_per_block;
				}

					c_line->tag = addr_tag;
					c_line->dirty = 0;
					c_stat->replacements++;
					/* delete line in LRU list and put it on the top */
					delete(head, tail, c_line);
					insert(head, tail, c_line);
			}

			write_hit(access_type, c_line, c_stat);

			c_stat->demand_fetches += words_per_block;
		} else {
			/*printf("CONSOLE: WNA NO CACHE FOR YOU\n");*/
			c_stat->copies_back += 1;
		}
		c_stat->misses++;
	}

	c_stat->accesses++;

}
/************************************************************/

/************************************************************/
/* flush the cache */
void flush() {
	destroy_cache(icache, &cache_stat_inst);

	if(cache_split) {
		destroy_cache(dcache, &cache_stat_data);
	}
}
/************************************************************/

/************************************************************/
void delete(head, tail, item)
	Pcache_line *head, *tail;
	Pcache_line item;
{
	if (item->LRU_prev) {
		item->LRU_prev->LRU_next = item->LRU_next;
	} else {
		/* item at head */
		*head = item->LRU_next;
	}

	if (item->LRU_next) {
		item->LRU_next->LRU_prev = item->LRU_prev;
	} else {
		/* item at tail */
		*tail = item->LRU_prev;
	}
}
/************************************************************/

/************************************************************/
/* inserts at the head of the list */
void insert(head, tail, item)
	Pcache_line *head, *tail;
	Pcache_line item;
{
	item->LRU_next = *head;
	item->LRU_prev = (Pcache_line)NULL;

	if (item->LRU_next)
		item->LRU_next->LRU_prev = item;
	else
		*tail = item;

	*head = item;
}
/************************************************************/

/************************************************************/
void dump_settings() {
	printf("Cache Settings:\n");
	if (cache_split) {
		printf("\tSplit I- D-cache\n");
		printf("\tI-cache size: \t%d\n", cache_isize);
		printf("\tD-cache size: \t%d\n", cache_dsize);
	} else {
		printf("\tUnified I- D-cache\n");
		printf("\tSize: \t%d\n", cache_usize);
	}
	printf("\tAssociativity: \t%d\n", cache_assoc);
	printf("\tBlock size: \t%d\n", cache_block_size);
	printf("\tWrite policy: \t%s\n", 
	 cache_writeback ? "WRITE BACK" : "WRITE THROUGH");
	printf("\tAllocation policy: \t%s\n",
	 cache_writealloc ? "WRITE ALLOCATE" : "WRITE NO ALLOCATE");
}
/************************************************************/

/************************************************************/
void print_stats() {
	printf("*** CACHE STATISTICS ***\n");
	printf("  INSTRUCTIONS\n");
	printf("  accesses:  %d\n", cache_stat_inst.accesses);
	printf("  misses:    %d\n", cache_stat_inst.misses);
	printf("  miss rate: %f\n", 
	 (float)cache_stat_inst.misses / (float)cache_stat_inst.accesses);
	printf("  replace:   %d\n", cache_stat_inst.replacements);

	printf("  DATA\n");
	printf("  accesses:  %d\n", cache_stat_data.accesses);
	printf("  misses:    %d\n", cache_stat_data.misses);
	printf("  miss rate: %f\n", 
	 (float)cache_stat_data.misses / (float)cache_stat_data.accesses);
	printf("  replace:   %d\n", cache_stat_data.replacements);

	printf("  TRAFFIC (in words)\n");
	printf("  demand fetch:  %d\n", cache_stat_inst.demand_fetches + 
	 cache_stat_data.demand_fetches);
	printf("  copies back:   %d\n", cache_stat_inst.copies_back +
	 cache_stat_data.copies_back);
}
/************************************************************/

/* create a node and intialize it  */
Pcache_line create_node(unsigned addr_tag) {

	Pcache_line c_line;

	c_line = malloc(sizeof(cache_line));

	c_line->tag = addr_tag;
	c_line->dirty = 0;

	return c_line;
}

/* takes a n cache line(s) and sees if it has the tag */
int get_line(cache_line **c_line, unsigned addr_tag) {

	while(*c_line != NULL && (*c_line)->tag != addr_tag) {
		*c_line = (*c_line)->LRU_next;
	}
	/*if(c_line == NULL) 
		printf("CONSOLE: Coudln't find it\n");
	else 
		printf("CONSOLE: Found %d == %d\n", addr_tag, c_line->tag);*/

	return *c_line != NULL;
}

/* makes a generic cache. Assumes some of the parameters are filled */
void construct_cache(Pcache c_cache) {

	int i, num_blocks;

	c_cache->index_mask_offset = LOG2(cache_block_size);
	c_cache->index_mask = (c_cache->n_sets - 1) << c_cache->index_mask_offset;

/*	printf("CONSOLE: ****cache stats*** \n");
	printf("CONSOLE: size: \t%d\n", c_cache->size);
	printf("CONSOLE: assoc: \t%d\n", c_cache->associativity);
	printf("CONSOLE: n_sets: \t%d\n", c_cache->n_sets);
	printf("CONSOLE: words_per_block \t%d\n", words_per_block);



	printf("CONSOLE: tag length: \t%d\n",  32 - (c_cache->index_mask_offset + 
		LOG2(c_cache->n_sets)));
*/
	/*printf("CONSOLE: block no: \t%d\n", LOG2(c_cache->n_sets));
	printf("CONSOLE: offset: \t%d\n", c_cache->index_mask_offset);*/
	c_cache->LRU_head = 
		(Pcache_line *)malloc(sizeof(Pcache_line)*c_cache->n_sets);
	c_cache->LRU_tail = 
		(Pcache_line *)malloc(sizeof(Pcache_line)*c_cache->n_sets);
	c_cache->set_contents = (int *)calloc(c_cache->n_sets, sizeof(int));

	for (i = 0; i < c_cache->n_sets; i++) {
		c_cache->LRU_head[i] = NULL;
		c_cache->LRU_tail[i] = NULL;
	}
}

void destroy_cache(Pcache c_cache, Pcache_stat c_stat) {

	Pcache_line c_line, temp;
	int i;

	for (i = 0; i < c_cache->n_sets; i++) {
		c_line = c_cache->LRU_head[i];

		while(c_line != NULL) {
			if(c_line->dirty) {
				/*printf("CONSOLE: Dirty cache line flushed\n");*/
				c_stat->copies_back += words_per_block;
			}

			temp = c_line;
			c_line = c_line->LRU_next;
			free(temp);
		}
	}

	free(c_cache->LRU_head);
	free(c_cache->LRU_tail);
	free(c_cache->set_contents);
}

void print_line(Pcache_line c_line) {
	
	Pcache_line temp = c_line;
	int size = 0;

	printf("CONSOLE: **print_line**\n");
	while(temp != NULL) {
		printf("CONSOLE: Tag - %-12x | Dirty bit - %d\n", temp->tag, temp->dirty);
		size++;
		temp = temp->LRU_next;
	}

	printf("CONSOLE: Size: %d\n", size);

}

/* performs task based on write hit policy */
void write_hit(unsigned access_type, Pcache_line c_line, Pcache_stat c_stat) {

	if (access_type == TRACE_DATA_STORE) {
		if (cache_writeback) {
			/*printf("CONSOLE: Setting Dirty Bit even doe in miss\n");*/
			c_line->dirty = 1;
		} else {
			/*printf("CONSOLE: Writing to cache and memory\n");*/
			c_stat->copies_back += 1;
		}
	}	
}