package view;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import cmsc420.xml.XmlUtility;
import controller.RoadMap;
import model.Outcome;
import model.Variables;

public class CommandParser {
	private RoadMap roadmap;
	private Document writer;
	
	public CommandParser() {
		 try {
			writer = XmlUtility.getDocumentBuilder().newDocument();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	public void processMap() {
		try {
        	Document doc = XmlUtility.validateNoNamespace(System.in);
        	Element result = writer.createElement("results"), commandNode = doc.getDocumentElement();
        	int localHeight, localWidth, remoteHeight = 0, remoteWidth = 0, pmOrder = 3, leafOrder = 0;

        	if(commandNode.hasAttribute("localSpatialHeight") 
        			&& commandNode.hasAttribute("localSpatialWidth")) {
            	localHeight = Integer.parseInt(commandNode.getAttribute("localSpatialHeight"));
    			localWidth = Integer.parseInt(commandNode.getAttribute("localSpatialWidth"));
    			
    			remoteHeight = Integer.parseInt(commandNode.getAttribute("remoteSpatialHeight"));
    			remoteWidth = Integer.parseInt(commandNode.getAttribute("remoteSpatialWidth"));
        	} else {
        		localHeight = Integer.parseInt(commandNode.getAttribute("spatialHeight"));
        		localWidth = Integer.parseInt(commandNode.getAttribute("spatialWidth"));
        	}
			if(commandNode.hasAttribute("pmOrder"))
				pmOrder = Integer.parseInt(commandNode.getAttribute("pmOrder"));
			if(commandNode.hasAttribute("leafOrder"))
				leafOrder = Integer.parseInt(commandNode.getAttribute("leafOrder"));
			
			roadmap = new RoadMap(remoteWidth, remoteHeight, localWidth, localHeight, pmOrder, leafOrder);
			roadmap.setWriter(writer);
			        	
        	final NodeList nl = commandNode.getChildNodes();
        	for (int i = 0; i < nl.getLength(); i++) {
        		if (nl.item(i).getNodeType() == Document.ELEMENT_NODE) {
        			commandNode = (Element) nl.item(i);
        			Outcome outcome = Variables.FATAL_OUTCOME;
        			String name = null, start, end, tag = commandNode.getTagName();
        			int localX, localY, radius, id, remoteX = 0, remoteY = 0;
        			boolean saveMap = false;
    				id = Integer.parseInt(commandNode.getAttribute("id"));

        			switch(tag){
        				case "createCity":
        					name = commandNode.getAttribute("name");
        					localX = Integer.parseInt(commandNode.getAttribute("localX"));
        					localY = Integer.parseInt(commandNode.getAttribute("localY"));
        					remoteX = Integer.parseInt(commandNode.getAttribute("remoteX"));
        					remoteY = Integer.parseInt(commandNode.getAttribute("remoteY"));
        					radius = Integer.parseInt(commandNode.getAttribute("radius"));
        					String color = commandNode.getAttribute("color"); 					
        					
        					outcome = roadmap.createCity(name, localX, localY, radius, color, remoteX, remoteY);
        					break;
        				case "listCities":
        					outcome = roadmap.listCities(commandNode.getAttribute("sortBy"), writer);
        					break;
        				case "clearAll":
        					outcome = roadmap.clearAll();
        					break;
        				case "deleteCity":
        					name = commandNode.getAttribute("name");

        					outcome = roadmap.deleteCity(name);
        					break;
        				case "mapCity":
        					name = commandNode.getAttribute("name");
        					
        					outcome = roadmap.mapCity(name);
        					break;
        				case "unmapCity":
        					name = commandNode.getAttribute("name");
        					
        					outcome = roadmap.unmapCity(name);
        					break;
        				case "printPRQuadtree":
        					outcome = roadmap.printSpatial();
        					break;
        				case "saveMap":
        					name = commandNode.getAttribute("name");
        					remoteX = Integer.parseInt(commandNode.getAttribute("remoteX"));
        					remoteY = Integer.parseInt(commandNode.getAttribute("remoteY"));
        					
        					outcome = roadmap.saveMap(name, remoteX, remoteY);
        					break;
        				case "rangeCities":
        					localX = Integer.parseInt(commandNode.getAttribute("x"));
        					localY = Integer.parseInt(commandNode.getAttribute("y"));
        					radius = Integer.parseInt(commandNode.getAttribute("radius"));
        					saveMap = commandNode.hasAttribute("saveMap");
        					
        					outcome = roadmap.rangeCities(localX, localY, radius, saveMap, false);
        					
        					if (saveMap) {
	    						name = commandNode.getAttribute("saveMap");
        						outcome.addParameter("saveMap", name);
        						
        						if(outcome.isSuccessful()) {
        							roadmap.saveMap(name, 0, 0);
        						}
        					}
        					break;
        				case "nearestCity":
        				case "nearestIsolatedCity": // Part 2
        				case "nearestRoad":
        					
        					if(commandNode.hasAttribute("remoteX")) {
            					remoteX = Integer.parseInt(commandNode.getAttribute("remoteX"));
            					remoteY = Integer.parseInt(commandNode.getAttribute("remoteY"));
        						localX = Integer.parseInt(commandNode.getAttribute("localX"));
            					localY = Integer.parseInt(commandNode.getAttribute("localY"));
        					} else {
            					localX = Integer.parseInt(commandNode.getAttribute("x"));
            					localY = Integer.parseInt(commandNode.getAttribute("y"));
        					}
        					
        					outcome = roadmap.nearestPart(remoteX, remoteY, localX, localY, tag);
        					break;
        				case "printDodekaTrie":
        					outcome = roadmap.printDodekaTrie(writer);
        					break;
        				case "mapRoad":
        					start = commandNode.getAttribute("start");
        					end = commandNode.getAttribute("end");

        					outcome = roadmap.mapRoad(start, end);
        					break;
        				case "rangeRoads":
        					localX = Integer.parseInt(commandNode.getAttribute("x"));
        					localY = Integer.parseInt(commandNode.getAttribute("y"));
        					radius = Integer.parseInt(commandNode.getAttribute("radius"));
        					saveMap = commandNode.hasAttribute("saveMap");
        					
        					outcome = roadmap.rangeRoads(localX, localY, radius, writer, saveMap);
        					
        					if (saveMap) {
	    						name = commandNode.getAttribute("saveMap");
        						outcome.addParameter("saveMap", name);
        						
        						if(outcome.isSuccessful()) {
        							roadmap.saveMap(name, 0, 0);
        						}
        					}
        					break;
        				case "nearestCityToRoad":
        					start = commandNode.getAttribute("start");
        					end = commandNode.getAttribute("end");

        					outcome = roadmap.nearestCityToRoad(start, end);
        					break;
        				case "shortestPath":
        					start = commandNode.getAttribute("start");
        					end = commandNode.getAttribute("end");
        					
        					saveMap = commandNode.hasAttribute("saveMap");
        					boolean saveHTML = commandNode.hasAttribute("saveHTML");
        					
        					outcome = roadmap.shortestPath(start, end, saveMap);
        					
        					if (saveMap) {
	    						name = commandNode.getAttribute("saveMap");
	    						outcome.addParameter("saveMap", name);
	    						
        						if(outcome.isSuccessful())
        							roadmap.saveMapBlank(name, outcome);
        					}
        					
        					if (saveHTML) {
	    						name = commandNode.getAttribute("saveHTML");
        						outcome.addParameter("saveHTML", name);
        						
        						if(outcome.isSuccessful()) {
            						org.w3c.dom.Document shortestPathDoc = 
            								XmlUtility.getDocumentBuilder().newDocument();
            						org.w3c.dom.Node spNode = 
            								shortestPathDoc.importNode(outcome.generateElement(writer), true);
            						shortestPathDoc.appendChild(spNode);
										XmlUtility.transform(shortestPathDoc, 
												new File("shortestPath.xsl"), new File(name + ".html"));
        						}
        					}
        					
        					break;
        				case "mapTerminal":
        					name = commandNode.getAttribute("name");
        					localX = Integer.parseInt(commandNode.getAttribute("localX"));
        					localY = Integer.parseInt(commandNode.getAttribute("localY"));
        					remoteX = Integer.parseInt(commandNode.getAttribute("remoteX"));
        					remoteY = Integer.parseInt(commandNode.getAttribute("remoteY"));
        					
        					String cityName = commandNode.getAttribute("cityName");
        					String airportName = commandNode.getAttribute("airportName");
        					
        					outcome = roadmap.mapTerminal(name, localX, localY, remoteX, remoteY, cityName, airportName);        					
        					break;
        				case "mapAirport": // Part 3
        					name = commandNode.getAttribute("name");
        					localX = Integer.parseInt(commandNode.getAttribute("localX"));
        					localY = Integer.parseInt(commandNode.getAttribute("localY"));
        					remoteX = Integer.parseInt(commandNode.getAttribute("remoteX"));
        					remoteY = Integer.parseInt(commandNode.getAttribute("remoteY"));
        					String terminalName = commandNode.getAttribute("terminalName");
        					int terminalX = Integer.parseInt(commandNode.getAttribute("terminalX"));
        					int terminalY = Integer.parseInt(commandNode.getAttribute("terminalY"));
        					String terminalCity = commandNode.getAttribute("terminalCity");
        					outcome = roadmap.mapAirport(name, localX, localY, remoteX, remoteY, 
        							terminalName, terminalX, terminalY, terminalCity);        					
        					break;
        				case "printPMQuadtree":
        					
        					if(commandNode.hasAttribute("remoteX")) {
            					remoteX = Integer.parseInt(commandNode.getAttribute("remoteX"));
            					remoteY = Integer.parseInt(commandNode.getAttribute("remoteY"));
            					outcome = roadmap.printPMQuadtree(remoteX, remoteY);
        					} else {
        						outcome = roadmap.printPMQuadtree();
        					}
        					
        					break;
        				case "unmapRoad":
        					start = commandNode.getAttribute("start");
        					end = commandNode.getAttribute("end");
        					outcome = roadmap.unmapRoad(start, end);
        					break;
        				case "unmapAirport":
        					name = commandNode.getAttribute("name");
        					outcome = roadmap.unmapAirport(name);
        					break;
        				case "unmapTerminal":
        					name = commandNode.getAttribute("name");
        					outcome = roadmap.unmapTerminal(name);
        					break;
        				case "globalRangeCities":
        					remoteX = Integer.parseInt(commandNode.getAttribute("remoteX"));
        					remoteY = Integer.parseInt(commandNode.getAttribute("remoteY"));
        					radius = Integer.parseInt(commandNode.getAttribute("radius"));
        					
        					outcome = roadmap.rangeCities(remoteX, remoteY, radius, false, true);
        					break;
        				case "mst":
        					start = commandNode.getAttribute("start");
        					outcome = new Outcome(Variables.ERROR, "mst", "cityDoesNotExist");
        					break;
        				default:
        					System.err.println("Unknown command: " + commandNode.getTagName());
        					outcome = Variables.FATAL_OUTCOME;
        			}
        			outcome.setID(id);
        			result.appendChild(outcome.generateElement(writer));
        		}
        	}
        
        	writer.appendChild(result);
        	
        } catch (SAXException | IOException | ParserConfigurationException | TransformerException e) {
        	try {
				writer = XmlUtility.getDocumentBuilder().newDocument();
			} catch (ParserConfigurationException e1) { 
				e1.printStackTrace();
			}
			Element fatalError = writer.createElement(Variables.FATAL_ERROR);
        	writer.appendChild(fatalError);
		} finally {
            try {
				XmlUtility.print(writer);
			} catch (TransformerException e) {
				Element undefinedError = writer.createElement(Variables.UNDEFINED_ERROR);
	        	writer.appendChild(undefinedError);
				e.printStackTrace();
			}
        }
	}
}