Josue A. Chavez
jachavez

Sources Used:

cmsc420util.jar

---Part 1---
PRQuadtree insertion algorithm taken from The Design and Analysis of Spatial Data Structures, Hanan Samet, page 94-97

---Part 2---
PMQuadtree insertion algorithm taken from 420 spec
B+ tree Interface layout taken from Data Structures and Algorithm Analysis, Clifford A. Shaffer, pages 359-360
Dijkstra's algorithm pseudocode taken from CMSC 132
Some of the comparators taken from Part 1 Canonical

---Part 3---
B+ tree deletion taken from Data Structures and Algorithm Analysis, Clifford A. Shaffer, pages 360-364
