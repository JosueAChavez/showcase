package controller;

import java.awt.Color;
import java.awt.geom.Arc2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import cmsc420.drawing.CanvasPlus;
import cmsc420.geom.Circle2D;
import cmsc420.geom.Inclusive2DIntersectionVerifier;
import controller.comparators.CityGraphComparator;
import controller.comparators.MapPartComparator;
import controller.comparators.MapPartNameComparator;
import model.Outcome;
import model.Variables;
import model.adj_list.Graph;
import model.dictionary.Dictionary;
import model.mappart.MapPart;
import model.mappart.Road;
import model.mappart.cities.Airport;
import model.mappart.cities.City;
import model.mappart.cities.CityType;
import model.mappart.cities.Terminal;
import model.spatial.Spatial;
import model.spatial.pmquadtree.PMQuadTree;
import model.spatial.prquadtree.PRQuadTree;
import view.CommandParser;

/**
 * What the {@link CommandParser} uses to 
 * communicate with the data structures 
 * and creates the outcome as XML
 * @author Josue A. Chavez
 *
 */
public class RoadMap {

	private Dictionary dict, airDict, metroDict;
	private CanvasPlus canvas;
	private Spatial<City> metropoleSpatial;
	private Document writer;
	private Graph<City> adjList;
	private Map<Point2D, PMQuadTree<MapPart>> metropoleMap;
	int localWidth, localHeight, pmOrder;
	
	 public RoadMap(int remoteWidth, int remoteHeight, int localWidth, int localHeight, int pmOrder, int leafOrder) {
			dict = new Dictionary(leafOrder);
			airDict = new Dictionary(leafOrder);
			metroDict = new Dictionary(leafOrder);
			canvas = new CanvasPlus("MeeshQuest");
			canvas.addRectangle(0, 0, localWidth, localHeight, Color.BLACK, false);
			metropoleSpatial = new PRQuadTree<City>(remoteWidth, remoteHeight);
			adjList = new Graph<>(new CityGraphComparator());
			metropoleMap = new HashMap<>();
			this.localWidth = localWidth;
			this.localHeight = localHeight;
			this.pmOrder = pmOrder;
	}

	/**
     * Creates a city. Returns true upon successful creation, otherwise false.
     * @return Outcome
     */
    public Outcome createCity(String name, int x, int y, int radius, String color) {
    	City city = new City(name, x, y, radius, color);
    	
    	return dict.put(city);
    }
    
	public Outcome createCity(String name, int localX, int localY, int radius, String color, int remoteX, int remoteY) {
		City city = new City(name, localX, localY, radius, color, remoteX, remoteY);
    	
    	return dict.put(city);
	}

    /**
     * Lists all cities in dictionary.
     * @param attribute
     * @param results
     * @return Outcome
     */
	public Outcome listCities(String attribute, Document results) {
		return dict.getCities(attribute, results);
	}

	/**
	 * Clears the map
	 * @return output
	 */
	public Outcome clearAll() {
		metropoleSpatial = metropoleSpatial.reset();
		metropoleMap.clear();
		airDict.clear();
		adjList = new Graph<City>(new CityGraphComparator());
		metroDict.clear();
		return dict.clear();
	}
	
	/**
	 * Deletes a city from dictionary and adjacency list. In addition, 
	 * if the city was mapped in the spatial structure, it would be 
	 * remove from there as well. 
	 * @param cityName
	 * @return
	 */
	public Outcome deleteCity(String cityName) {
		
		City city = dict.get(cityName);
		Map<MapPart, Element> unmappings = new TreeMap<>(new MapPartComparator());

		// delete city from spatial if its contains it
		if(dict.has(cityName) && adjList.isVertex(city)) {
			Collection<City> neighbors = adjList.getNeighbors(city);
			neighbors = new ArrayList<City>(neighbors); // prevent concurrent exception
			
			// find what cities are going to be unmapped as a result of removing city
			for(City adjCity: neighbors) {
				Road road = new Road(city, dict.get(adjCity.getName()));
				unmappings.put(road, road.getXML(writer, "roadUnmapped"));
				
				// means only city was connected to it
				/*if(adjList.getPredecessors(adjCity).size() == 1)
					unmappings.put(adjCity, adjCity.getXML(writer, "cityUnmapped"));*/
			}
			
			// now delete the roads from the PM data structure
			for(City adjCity: neighbors)
				unmapRoad(cityName, adjCity.getName()).isSuccessful();
			
			adjList.removeVertex(city);
			unmappings.put(city, city.getXML(writer, "cityUnmapped"));
		}
		
		// remove from the data dictionary
		Outcome outcome = dict.remove(cityName);

		// add the cities and roads unmapped to the results
		for(Element element: unmappings.values())
			outcome.addOutput(element);
			
		return outcome;
	}

	/**
	 * Maps a city to the spatial dictionary
	 * @param name
	 * @return
	 */
	public Outcome mapCity(String name) {
		City city = dict.get(name);
		Outcome outcome;
		
		if(isOlderSpatial()) { // make it support older spec
			if(city == null) {
				return new Outcome(Variables.ERROR, "mapCity", "nameNotInDictionary");
			} else if(city.getCityType() != CityType.NEITHER) {
				return new Outcome(Variables.ERROR, "mapCity", "cityAlreadyMapped");
			}
		}

		if(city == null) {
			outcome = new Outcome(Variables.ERROR, "mapCity", "nameNotInDictionary");
		} else if(!metropoleSpatial.cityIntersects(city)) { 
			outcome = new Outcome(Variables.ERROR, "mapCity", "cityOutOfBounds");
		} else if(city.getCityType() != CityType.NEITHER) {
			outcome = new Outcome(Variables.ERROR, "mapCity", "cityAlreadyMapped");
		} else {
			outcome = metropoleSpatial.map(city);
			city.changeState(CityType.ISOLATED);
			adjList.addVertex(city);
		}
		
		return outcome.addParameter("name", name);
	}

	/**
	 * Get the structure of the current spatial structure in a printable format
	 * @return
	 */
	public Outcome printSpatial() {
		return metropoleSpatial.toOutcome(writer);
	}

	public Outcome saveMap(String name, int remoteX, int remoteY) {
		canvas.clear();
		Point2D pt = new Point2D.Double(remoteX, remoteY);
		PMQuadTree<MapPart> metropole = metropoleMap.get(pt);
		Outcome outcome;
		if (!metropole.isEmpty())
			outcome = metropole.saveMap(name, canvas);			
		 else 
			 outcome = new Outcome(Variables.ERROR, "saveMap", "metropoleIsEmpty");
			
		outcome.addParameter("remoteX", Integer.toString(remoteX))
				.addParameter("remoteY", Integer.toString(remoteY));
		
		// TODO check if this works with saveMapBlank
		/*try {
			canvas.save(name);
			outcome.addParameter("name", name);
		} catch (IOException e) {
			e.printStackTrace();
			return Variables.FATAL_OUTCOME;
		} finally {
			canvas.dispose();
 		}
		
		return outcome;*/
		return saveMapBlank(name, outcome);
	}
	
	public Outcome saveMapBlank(String name, Outcome outcome) {
		try {
			canvas.save(name);
			outcome.addParameter("name", name);
		} catch (IOException e) {
			e.printStackTrace();
			return Variables.FATAL_OUTCOME;
		} finally {
			canvas.dispose();
 		}
		
		return outcome;
	}
	
	/**
	 * Uses the radius and the x and y coordinates to 
	 * determine what cities are within that range.
	 * @param x
	 * @param y
	 * @param radius
	 * @param saveMap
	 * @param global
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Outcome rangeCities(int x, int y, int radius, boolean saveMap, boolean global) {
		
		Outcome outcome;
		String command, xS = "remoteX", yS = "remoteY";
		Set<MapPart> cities = null;
		Set<String> cityNames = null;
		
		if(global) {
			command = "globalRangeCities";
			if(metropoleSpatial instanceof PRQuadTree) 
				cityNames = ((PRQuadTree)metropoleSpatial).rangeCities(x, y, radius, metroDict);
		} else {
			xS = "x";
			yS = "y";
			command = "rangeCities";
			cities = metropoleSpatial.rangeCities(x, y, radius);
		}
				
		if((cities != null && cities.isEmpty()) || (cityNames != null && cityNames.isEmpty())) {
			outcome = new Outcome(Variables.ERROR, command, "noCitiesExistInRange");
		} else {
			outcome = new Outcome(Variables.SUCCESS, command);
			outcome.setOutputTag("cityList");
			
			if (saveMap)
				canvas.addCircle(x, y, radius, Color.BLUE, false);
			
			if(global) {
				cities = new TreeSet<>(new MapPartNameComparator());
				// visit each PM metropole
				for (String cityName: cityNames) {
					PMQuadTree<MapPart> pm = metropoleMap.get(metroDict.get(cityName).getLocal());
					// retrieve and insert all of current PM's cities 
					cities.addAll(pm.rangeCities(pm.getWidth()/2, pm.getHeight()/2, Integer.MAX_VALUE));
				}
			}
			
			if(!cities.isEmpty()) { // have metropoles but they're empty inside
				for (MapPart part: cities)
					outcome.addOutput(part.getXML(writer, "city"));
			} else {
				outcome = new Outcome(Variables.ERROR, command, "noCitiesExistInRange");
			}
		}
		
		outcome.addParameter(xS, Integer.toString(x));
		outcome.addParameter(yS, Integer.toString(y));
		outcome.addParameter("radius", Integer.toString(radius));
		
		return outcome;
	}

	
	public Outcome nearestPart(int remoteX, int remoteY, int localX, int localY, String command) {
		PMQuadTree<MapPart> metropole = metropoleMap.get(new Point2D.Double(remoteX, remoteY));
		
		if(metropole == null) // do it to save time modifying something we know is working
			metropole = new PMQuadTree<>(0, 0, 3);
		
		return nearestPart(localX, localY, command, metropole)
				.addParameter("remoteX", Integer.toString(remoteX))
				.addParameter("remoteY", Integer.toString(remoteY));
	}
	
	/**
	 * Returns the name and location of closest Part
	 * @param x
	 * @param y
	 * @param command
	 * @return
	 */
	private Outcome nearestPart(int x, int y, String command, PMQuadTree<MapPart> metropole) {
		
		Outcome outcome;
		String typeStr = null, outputType = null;
		CityType type = null;
		MapPart nearest;
		
		switch(command) {
			case "nearestIsolatedCity":
				type = CityType.ISOLATED;
				typeStr = "city";
				outputType = "isolatedCity";
				break;
			case "nearestCity":
				type = CityType.NEITHER;
				typeStr = "city";
				outputType = typeStr;
				break;
			case "nearestRoad":
				type = CityType.CONNECTED;
				typeStr = "road";
				outputType = typeStr;
				break;
		}
		
		nearest = metropole.nearest(x, y, type);
		
		if(nearest == null) {
			outcome = new Outcome(Variables.ERROR, command, typeStr + "NotFound");
		} else {
			outcome = new Outcome(Variables.SUCCESS, command);
			
			Element ele = nearest.getXML(writer, outputType);			
			outcome.addOutput(ele);
		}
				
		outcome.addParameter("localX", Integer.toString(x));
		outcome.addParameter("localY", Integer.toString(y));
		
		return outcome;
	}
	
	/**
	 * Return the name and location of the closest city in the spatial 
	 * map to the specified road in the spatial map.
	 * @param start
	 * @param end
	 * @return
	 */
	public Outcome nearestCityToRoad(String start, String end) {
		Outcome outcome;
		
		/*if(!dict.has(start) || !dict.has(end))
			return Variables.UNDEFINED_OUTCOME;*/
		
		// check if road is mapped
		if(!adjList.containsBiEdge(dict.get(start), dict.get(end)))
			outcome = new Outcome(Variables.ERROR, "nearestCityToRoad", "roadIsNotMapped");
		else {
			MapPart nearest = 
					metropoleSpatial.nearestCityToRoad(new Road(dict.get(start), dict.get(end)));
			
			if(nearest == null) {
				outcome = new Outcome(Variables.ERROR, 
						"nearestCityToRoad", "noOtherCitiesMapped");
			} else {
				outcome = new Outcome(Variables.SUCCESS, "nearestCityToRoad");
				outcome.addOutput(nearest.getXML(writer, "city"));
			}			
		}
		
		outcome.addParameter("start", start);
		outcome.addParameter("end", end);
		
		return outcome;
	}

	/**
	 * Replaces the current {@link Document} writer with {@code writer}.
	 * @param writer
	 */
	public void setWriter(Document writer) {
		this.writer = writer;
	}

	/**
	 * Prints the structure of DodekaTrie.
	 * @return
	 */
	public Outcome printDodekaTrie(Document results) {
		
		return dict.printDodekaTrie(results);
	}

	/**
	 * Inserts a road between the two cities (vertices) named by the {@code start} and 
	 * {@code end} attributes in PM quadtree.
	 * @param start
	 * @param end
	 * @return
	 */
	public Outcome mapRoad(String start, String end) {
		
		City startCity, endCity;
		Outcome outcome = new Outcome(Variables.ERROR, "mapRoad");
		outcome.addParameter("start", start);
		outcome.addParameter("end", end);
		
		// edge cases
		if(!dict.has(start))
			return outcome.settypeAttr("startPointDoesNotExist");
		else if(!dict.has(end))
			return outcome.settypeAttr("endPointDoesNotExist");
		else if(start.equals(end))
			return outcome.settypeAttr("startEqualsEnd");
		startCity = dict.get(start);
		endCity = dict.get(end);

		// TODO might not need now
		/*if(startCity.getCityType() == CityType.ISOLATED || 
				endCity.getCityType() == CityType.ISOLATED)
			return outcome.settypeAttr("startOrEndIsIsolated");*/
		
		Point2D ptStart = getRemotes(startCity), ptEnd = getRemotes(endCity);
		
		if(!isSameRemote(ptStart, ptEnd))
			return outcome.settypeAttr("roadNotInOneMetropole");
		
		Rectangle2D rect = new Rectangle2D.Double(0, 0, localWidth, localHeight);
		Road road = new Road(startCity, endCity);
			
		if(!Inclusive2DIntersectionVerifier.intersects(ptStart, rect) 
				|| !Inclusive2DIntersectionVerifier.intersects(road, rect))
			return outcome.settypeAttr("roadOutOfBounds");
		if(adjList.containsBiEdge(startCity, endCity))
			return outcome.settypeAttr("roadAlreadyMapped");

		if(!metropoleMap.containsKey(ptStart)) {
			City metroCity = new City("metropole" + UUID.randomUUID().toString(),
								(int)ptStart.getX(), (int)ptStart.getY(), 0, "X");
			metropoleMap.put(ptStart, new PMQuadTree<>(localWidth, localHeight, pmOrder));
			metropoleSpatial.map(metroCity);
			metroDict.put(metroCity); // TODO do I remove it after all cities in that point are removed??
		}
		
		PMQuadTree<MapPart> metropole = metropoleMap.get(ptStart);
				
		outcome = metropole.mapQEdge(road, startCity, endCity);

		// add edge onto the graph
		if(outcome.isSuccessful())
			adjList.addBiEdge(startCity, endCity, startCity.distance(endCity));
		
		outcome.addOutput(road.getXML(writer, "roadCreated"));
		
		return outcome;
	}
	
	
	public Outcome mapTerminal(String name, int localX, int localY, int remoteX, int remoteY, 
			String cityName, String airportName) {
		
		Outcome outcome = new Outcome(Variables.ERROR, "mapTerminal");
		City terminal = new Terminal(name, localX, localY, remoteX, remoteY, cityName, airportName);
		Point2D terminalRemote = new Point2D.Double(remoteX, remoteY);
		PMQuadTree<MapPart> metropole = metropoleMap.get(terminalRemote);
		
		if(airDict.has(name) || dict.has(name))
			outcome.settypeAttr("duplicateTerminalName");
		else if (dict.hasCoordinate(terminal) || airDict.hasCoordinate(terminal))
			outcome.settypeAttr("duplicateTerminalCoordinates");
		else if(!(metropoleSpatial.cityIntersects(terminal) && metropole.cityIntersects(terminal)))
			outcome.settypeAttr("terminalOutOfBounds");
		else if(!airDict.has(airportName))
			outcome.settypeAttr("airportDoesNotExist");
		else if(!isSameRemote(terminalRemote, airDict.get(airportName).getRemote()))
			outcome.settypeAttr("airportNotInSameMetropole");
		else if(!dict.has(cityName))
			outcome.settypeAttr("connectingCityDoesNotExist");
		else if(!isSameRemote(terminalRemote, dict.get(cityName).getRemote()))
			outcome.settypeAttr("connectingCityNotInSameMetropole");
		else if(dict.get(cityName).getCityType() != CityType.CONNECTED) // TODO might not be the best check
			outcome.settypeAttr("connectingCityNotMapped"); // TODO this command conflicts with mapTerminal
		else {
			City city = dict.get(cityName);
			outcome = metropole.mapQEdge(new Road(city, terminal), city, terminal);
			if(outcome.isSuccessful()) {
				outcome = new Outcome(Variables.SUCCESS, "mapTerminal");
				// add terminal to the airport
				airDict.put(terminal);
				((Airport) airDict.get(airportName)).addTerminal((Terminal) terminal);
				adjList.addBiEdge(city, terminal, city.distance(terminal));
			}
		}
		
		/*outcome = new Outcome(Variables.ERROR, "mapTerminal", "terminalViolatesPMRules");
		outcome = new Outcome(Variables.ERROR, "mapTerminal", "roadIntersectsAnotherRoad");*/
		
		return outcome.addParameter("name", name)
				.addParameter("localX", Integer.toString(localX))
				.addParameter("localY", Integer.toString(localY))
				.addParameter("remoteX", Integer.toString(remoteX))
				.addParameter("remoteY", Integer.toString(remoteY))
				.addParameter("cityName", cityName)
				.addParameter("airportName", airportName);
	}
	
	/**
	 * Creates an airport at the specified local and remote coordinates.
	 * @param localX
	 * @param localY
	 * @param remoteX
	 * @param remoteY
	 * @param terminalName
	 * @param terminalX
	 * @param terminalY
	 * @param terminalCity
	 * @return
	 */
	public Outcome mapAirport(String name, int localX, int localY, int remoteX, int remoteY, 
			String terminalName, int terminalX, int terminalY, String terminalCity) {
		
		Outcome outcome = new Outcome(Variables.ERROR, "mapAirport"), errOutcome = outcome;
		Airport airport = new Airport(name, localX, localY, remoteX, remoteY);
		Point2D airportPoint = new Point2D.Double(remoteX, remoteY);
		PMQuadTree<MapPart> metropole = metropoleMap.get(airportPoint);
		
		outcome.addParameter("name", name)
			.addParameter("localX", Integer.toString(localX))
			.addParameter("localY", Integer.toString(localY))
			.addParameter("remoteX", Integer.toString(remoteX))
			.addParameter("remoteY", Integer.toString(remoteY))
			.addParameter("terminalName", terminalName)
			.addParameter("terminalX", Integer.toString(terminalX))
			.addParameter("terminalY", Integer.toString(terminalY))
			.addParameter("terminalCity", terminalCity);

		if(dict.has(name) || airDict.has(name))
			return outcome.settypeAttr("duplicateAirportName");
		else if (dict.hasCoordinate(airport) || airDict.hasCoordinate(airport))
			return outcome.settypeAttr("duplicateAirportCoordinates");
		else if(!(metropoleSpatial.cityIntersects(airport) && metropole.cityIntersects(airport)))
			return outcome.settypeAttr("airportOutOfBounds");

		// map the terminal with the passed city
		airDict.put(airport);
		outcome = mapTerminal(terminalName, terminalX, terminalY, remoteX, remoteY, terminalCity, name);
		if(!outcome.isSuccessful()) {
			airDict.remove(name);
			airDict.remove(terminalName);
			// TODO remove terminal from airport's terminal list
			return errOutcome.settypeAttr(outcome.gettypeAttr());
		}
		// map the airport
		outcome = metropole.map(airport);
		if(!outcome.isSuccessful()) {
			unmapRoad(terminalName, terminalCity);

			airDict.remove(name);
			airDict.remove(terminalName);
			
			if(outcome.gettypeAttr().equals("roadViolatesPMRules"))
				return errOutcome.settypeAttr("terminalViolatesPMRules");
			return errOutcome.settypeAttr(outcome.gettypeAttr());
		} else if(dict.get(terminalCity).getCityType() != CityType.CONNECTED) { // TODO might not be the best check
			// unmapTerminal
			airDict.remove(name);
			airDict.remove(terminalName);
			metropole.unmap(airport);
			return errOutcome.settypeAttr("connectingCityNotMapped");
		}
		
		/*outcome.settypeAttr("duplicateTerminalName");
		outcome.settypeAttr("duplicateTerminalCoordinates");
		outcome.settypeAttr("terminalOutOfBounds");
		outcome.settypeAttr("connectingCityDoesNotExist");
		outcome.settypeAttr("connectingCityNotInSameMetropole");
		
		outcome.settypeAttr("airportViolatesPMRules");
		outcome.settypeAttr("connectingCityNotMapped");
		outcome.settypeAttr("terminalViolatesPMRules");
		outcome.settypeAttr("roadIntersectsAnotherRoad");*/
		
		return outcome.addParameter("name", name)
			.addParameter("localX", Integer.toString(localX))
			.addParameter("localY", Integer.toString(localY))
			.addParameter("remoteX", Integer.toString(remoteX))
			.addParameter("remoteY", Integer.toString(remoteY))
			.addParameter("terminalName", terminalName)
			.addParameter("terminalX", Integer.toString(terminalX))
			.addParameter("terminalY", Integer.toString(terminalY))
			.addParameter("terminalCity", terminalCity);
	}
	
	/**
	 * Prints the PMQuadtree.
	 * @return
	 */
	public Outcome printPMQuadtree() {
		return printSpatial();
	}
	
	/**
	 * Prints the PMQuadtree of the given metropole.
	 * @return
	 */
	public Outcome printPMQuadtree(int remoteX, int remoteY) {
		
		Outcome outcome = new Outcome(Variables.ERROR, "printPMQuadtree");
		Point2D pt = new Point2D.Double(remoteX, remoteY);
		
		if(!getRect().contains(new Point2D.Double(remoteX, remoteY)))
			outcome.settypeAttr("metropoleOutOfBounds");
		else if(!metropoleMap.containsKey(pt))
			outcome.settypeAttr("metropoleIsEmpty");
		else
			outcome = metropoleMap.get(pt).toOutcome(writer);
		
		return outcome
				.addParameter("remoteX", Integer.toString(remoteX))
				.addParameter("remoteY", Integer.toString(remoteY));
	}

	/**
	 * Lists all the roads present in the spatial map that intersect the circle 
	 * defined by the given radius and point (x, y).
	 * @param x
	 * @param y
	 * @param radius
	 * @param writer2
	 * @param saveMap
	 * @return
	 */
	public Outcome rangeRoads(int x, int y, int radius, Document writer2, boolean saveMap) {
		Outcome outcome;
		
		TreeSet<MapPart> roadNames = metropoleSpatial.rangeRoads(x, y, radius);
		
		if(roadNames.isEmpty()) {
			outcome = new Outcome(Variables.ERROR, "rangeRoads", "noRoadsExistInRange");
		} else {
			outcome = new Outcome(Variables.SUCCESS, "rangeRoads");
			outcome.setOutputTag("roadList");
			
			if (saveMap) {
				canvas.addCircle(x, y, radius, Color.BLUE, false);
			}
			Circle2D cir = new Circle2D.Double(x, y, radius);
			
			for (MapPart part: roadNames) {
				/*if(getRect().contains(((Road) part).getP1()) || 
						getRect().contains(((Road) part).getP2())) {*/
				if((getRect().contains(((Road) part).getP1()) || 
						getRect().contains(((Road) part).getP2())) || (cir.contains(((Road) part).getP1()) || 
						cir.contains(((Road) part).getP2()))) {
					outcome.addOutput(part.getXML(writer, "road"));	
				}
			}
		}
		
		outcome.addParameter("x", Integer.toString(x));
		outcome.addParameter("y", Integer.toString(y));
		outcome.addParameter("radius", Integer.toString(radius));
		
		return outcome;
	}
	
	/**
	 * Unmaps a city to the spatial dictionary
	 * @param name
	 * @return
	 */
	public Outcome unmapCity(String name) {
		
		City city = null;
		// get the right dictionary
		if(dict.has(name))
			city = dict.get(name);
		else
			city = airDict.get(name);
		
		PMQuadTree<MapPart> metropole = metropoleMap.get(city.getRemote());
		
		
		// remove city from adjacency list
		adjList.removeVertex(city);
		
		// remove from map
		return  metropole.unmap(city);
	}
	
	public Outcome unmapRoad(String start, String end) {
		
		Outcome outcome = new Outcome(Variables.ERROR, "unmapRoad");
		outcome.addParameter("start", start);
		outcome.addParameter("end", end);
		
		// edge cases
		if(!dict.has(start) && !airDict.has(start))
			return outcome.settypeAttr("startPointDoesNotExist");
		else if(!dict.has(end) && !airDict.has(end))
			return outcome.settypeAttr("endPointDoesNotExist");
		else if(start.equals(end))
			return outcome.settypeAttr("startEqualsEnd");
		
		// determine which dictionary roads comes from
		// TODO can terminals be connected to each other?, if so -> individual ifs
		City startCity, endCity;
		if(airDict.has(start)) {
			startCity = airDict.get(start);
			endCity = dict.get(end);
		} else if(airDict.has(end)) {
			startCity = dict.get(start);
			endCity = airDict.get(end);
		} else {
			startCity = dict.get(start);
			endCity = dict.get(end);
		}
		
		Point2D ptStart = getRemotes(startCity), ptEnd = getRemotes(endCity);
		Road toRemove = new Road(startCity, endCity);

		if(!isSameRemote(ptStart, ptEnd) || !adjList.containsBiEdge(startCity, endCity))
			return outcome.settypeAttr("roadNotMapped"); // TODO let unmap do the logic in the future?
		PMQuadTree<MapPart> metropole = metropoleMap.get(ptStart);

		metropole.unmap(toRemove);
		
		// update adjacency list accordingly
		adjList.removeBiEdge(startCity, endCity);
		
		// check if road removal caused isolated vertices
		if(adjList.getNeighbors(startCity).isEmpty()) {
			adjList.removeVertex(startCity);
			metropole.unmap(startCity);
		}
		
		if(adjList.getNeighbors(endCity).isEmpty()) {
			adjList.removeVertex(endCity);
			metropole.unmap(endCity);
		}
		
		outcome = new Outcome(Variables.SUCCESS, "unmapRoad");
		outcome.addParameter("start", start);
		outcome.addParameter("end", end);
		outcome.addOutput(toRemove.getXML(writer, "roadDeleted"));
				
		return outcome;
	}
	
	/**
	 * Removes a terminal from the map.<br> If the associated airport has 
	 * no remaining mapped terminals, then the airport is also removed.<br>
	 * Roads connecting the terminals to their cities will also be unmapped.
	 * @param name
	 * @return
	 */
	public Outcome unmapTerminal(String name) {
		Outcome outcome;
		Terminal terminal = (Terminal) airDict.get(name);
		
		if(terminal == null) {
			outcome = new Outcome(Variables.ERROR, "unmapTerminal", "terminalDoesNotExist");
		} else {
			// remove from the map
			outcome = new Outcome(Variables.SUCCESS, "unmapTerminal");
			unmapRoad(name, terminal.getCity());
			String airportName = terminal.getAirport();
			Airport airport = (Airport) airDict.get(airportName);
			
			// remove terminal from the airport's list
			airport.removeTerminal(terminal);
	
			// check if that terminal was the only one connected to the airport
			if(airport.hasNoTerminals()) {
				metropoleMap.get(airport.getRemote()).unmap(airport);
				outcome.addOutput(airport.getXML(writer, "airportUnmapped"));
				airDict.remove(airportName);
			}
			
			airDict.remove(name);
		}

		outcome.addParameter("name", name);
		
		return outcome;
	}
	
	/**
	 * Removes an airport from the map.<br> 
	 * All terminals associated with the airport will also be unmapped.<br>
	 * Roads connecting the terminals to their cities will also be unmapped.
	 * @param name
	 * @return
	 */
	public Outcome unmapAirport(String name) {
		Outcome outcome;
		Airport airport = (Airport) airDict.get(name);
		
		if(airport == null) {
			outcome = new Outcome(Variables.ERROR, "unmapAirport", "airportDoesNotExist");
		} else {
			// remove terminals and the roads that connects to it
			outcome = new Outcome(Variables.SUCCESS, "unmapAirport");
			Set<Terminal> terminals = new LinkedHashSet<>(airport.getTerminals());
			for(Terminal terminal: terminals) {
				outcome.addOutput(terminal.getXML(writer, "terminalUnmapped"));
				unmapTerminal(terminal.getName());
			}
		}
		
		outcome.addParameter("name", name);
		
		return outcome;
	}
	
	/**
	 * Prints the shortest path and direction traveled (from the perspective of 
	 * someone driving down the current road) between the cities named by the 
	 * {@code start} and {@code end} attributes, where the length of the road is the 
	 * Euclidean distance between the cities.
	 * @param start
	 * @param end
	 * @return
	 */
	public Outcome shortestPath(String start, String end, boolean saveMap) {
		Outcome outcome;
		
		outcome = new Outcome(Variables.SUCCESS, "shortestPath");
		City startCity = dict.get(start), endCity = dict.get(end);
		
		Rectangle2D rect = new Rectangle2D.Double(0, 0, 
				metropoleSpatial.getWidth(), metropoleSpatial.getHeight());
		
		if(startCity == null || !adjList.isVertex(startCity) || 
			!Inclusive2DIntersectionVerifier.intersects(startCity, rect)){
			return new Outcome(Variables.ERROR, "shortestPath", "nonExistentStart")
				.addParameter("start", start)
				.addParameter("end", end);
		}
		
		if(endCity == null || !adjList.isVertex(endCity) || 
				!Inclusive2DIntersectionVerifier.intersects(endCity, rect)) {
			return new Outcome(Variables.ERROR, "shortestPath", "nonExistentEnd")
					.addParameter("start", start)
					.addParameter("end", end);
		}
		
		Map<City, City> res = adjList.shortestPath(startCity, endCity);
		
		Map<City, City> resRev = new TreeMap<City, City>();
		
		double length = 0.0;
		int hops = 0;
		Element path = writer.createElement("path");
		
		// reverse the path (won't work if I switch shortestPath arguments)
		if(!startCity.equals(endCity)) {
			City curr = endCity;

			try {
				do {
					City next = res.get(curr);
					resRev.put(next, curr);
					
					if(!Inclusive2DIntersectionVerifier.intersects(curr, getRect()))
						throw new NullPointerException();
					
					curr = next;
				} while(!curr.equals(startCity));
			} catch (NullPointerException e) {
				return new Outcome(Variables.ERROR, "shortestPath", "noPathExists")
					.addParameter("start", start)
					.addParameter("end", end);
			}
		}
		
		res = resRev;
		
		if(!startCity.equals(endCity)) {
			City curr = startCity;
			try {
				do {
					City next = res.get(curr);
					hops++;
					length += curr.distance(res.get(curr));
					
					if(saveMap) {
						canvas.addLine(curr.getX(), curr.getY(), next.getX(),
								next.getY(), Color.BLUE);
						canvas.addPoint(curr.getName(), curr.getX(),
								curr.getY(), Color.BLUE);
					}
					
					Element road = writer.createElement("road");
					road.setAttribute("start", curr.getName());
					road.setAttribute("end", next.getName());
					path.appendChild(road);
					
					// find direction
					if(res.get(next) != null) {
						City A = curr;
						City B = next;
						City C = res.get(next);
						
						Arc2D.Double arc = new Arc2D.Double();
						arc.setArcByTangent(A, B, C, 1);
						double angle = arc.getAngleExtent();
						String direction = null;
						if(angle >= -45 && angle < 45)
							direction = "straight";
						else if (angle < -45 && angle > -180)
							direction = "left";
						else
							direction = "right";
						path.appendChild(writer.createElement(direction));
					}
					
					curr = next;
				} while(!curr.equals(endCity));
			} catch (NullPointerException e) {
				return new Outcome(Variables.ERROR, "shortestPath", "noPathExists")
					.addParameter("start", start)
					.addParameter("end", end);
			}
		}
		
		if(saveMap) {
			canvas.addPoint(endCity.getName(), endCity.getX(), endCity.getY(), Color.RED);
			canvas.addPoint(startCity.getName(),
					startCity.getX(), startCity.getY(), Color.GREEN);
		}

		outcome.addParameter("start", startCity.getName());
		outcome.addParameter("end", endCity.getName());
		outcome.addOutput(path);
		
		// make the number pretty
		DecimalFormat df = new DecimalFormat("0.000");
		path.setAttribute("length", df.format(length));
		
		path.setAttribute("hops", Integer.toString(hops));

		return outcome;
	}
	
	/**
	 * @return make it support older spec
	 */
	private boolean isOlderSpatial() {
		return (metropoleSpatial instanceof PRQuadTree); 
	}
	
	/**
	 * @return a rectangle representing the spatial data structure
	 */
	private Rectangle2D getRect() {
		return new Rectangle2D.Double(0, 0, metropoleSpatial.getWidth(), metropoleSpatial.getHeight()); 
	}

	// returns a point representing the remote X and Y location of the city
	private Point2D getRemotes(City city) {
		double remoteX = city.getRemoteX(), remoteY = city.getRemoteY();
		return new Point2D.Double(remoteX, remoteY);
	}
	
	// sees if two cities go in the same remote location
	private boolean isSameRemote(Point2D start, Point2D end) {
		return Double.compare(start.getX(), end.getX()) == 0 &&
				Double.compare(start.getY(), end.getY()) == 0;
	}
}