package controller.comparators;

import java.util.Comparator;

import model.mappart.cities.City;

public class CityGraphComparator implements Comparator<City> {

	public int compare(City o1, City o2) {
		return o1.getName().compareTo(o2.getName());
	}
}
