package controller.comparators;

import java.util.Comparator;

import model.Variables;
import model.mappart.MapPart;
import model.mappart.Road;
import model.mappart.cities.City;

public class MapPartNameComparator implements Comparator<MapPart> {
	public int compare(MapPart o1, MapPart o2) {
		if(o1 instanceof City && o2 instanceof Road)
			return -1;
		else if(o2 instanceof City && o1 instanceof Road)
			return 1;
		else if(o1 instanceof City && o2 instanceof City)
			return Variables.CITY_NAME_COMP.compare((City)o1, (City)o2);
		else {
			int start = ((Road) o1).getStart().compareTo(((Road) o2).getStart());
			return start == 0 ? ((Road) o1).getEnd().compareTo(((Road) o2).getEnd()) : start;
		}
	}
}