package controller.comparators;

import java.util.Comparator;

import model.mappart.Road;

/**
 * Get the descending asciibetical rule to the start city name and end city name
 */
public class NearestRoadComparator implements Comparator<Road>{

	public int compare(Road road1, Road road2) {
		int start = road1.getStartCity().getName().compareTo(road2.getStartCity().getName());
		return start == 0 ? road1.getEndCity().getName().compareTo(road2.getEndCity().getName()) : start;
	}
}
