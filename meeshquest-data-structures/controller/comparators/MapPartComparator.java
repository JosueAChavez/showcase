package controller.comparators;

import java.util.Comparator;

import model.mappart.MapPart;
import model.mappart.Road;
import model.mappart.cities.City;

public class MapPartComparator implements Comparator<MapPart> {
	public int compare(MapPart o1, MapPart o2) {
		if(o1 instanceof City && o2 instanceof Road)
			return -1;
		else if(o2 instanceof City && o1 instanceof Road)
			return 1;
		else if(o1 instanceof City && o2 instanceof City)
			return ((City) o1).compareTo((City) o2); // shouldn't matter
		else
			return ((Road) o1).compareTo((Road) o2);
	}
}