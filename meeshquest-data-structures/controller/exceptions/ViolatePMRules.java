package controller.exceptions;
/**
 * Thrown if a command violated one of the PM rules
 */
public class ViolatePMRules extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ViolatePMRules() {
	} 
	
	public ViolatePMRules(String message) {
		super(message);
	}
}
