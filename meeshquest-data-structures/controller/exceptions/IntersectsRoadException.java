package controller.exceptions;
/**
 * Thrown if a road intersects an existing road or roads
 */
public class IntersectsRoadException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public IntersectsRoadException() {
	} 
	
	public IntersectsRoadException(String message) {
		super(message);
	}
}