package controller.exceptions;
/**
 * Thrown if a spatial location is already mapped on the spatial data structure
 */
public class AlreadyMapped extends RuntimeException {

	private static final long serialVersionUID = 4539890015430088013L;

	public AlreadyMapped() {
	} 
	
	public AlreadyMapped(String message) {
		super(message);
	}
}