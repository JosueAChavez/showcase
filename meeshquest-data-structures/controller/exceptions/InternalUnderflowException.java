package controller.exceptions;

import model.dictionary.dt.node.BPNode;
/**
 * Thrown if a B+ Internal is underflown
 */
public class InternalUnderflowException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private BPNode<?, ?> node;
	private Object val;

	public InternalUnderflowException(BPNode<?, ?> node, Object val) {
		this.node = node;
		this.val = val;
	} 
	
	public InternalUnderflowException(String message, BPNode<?, ?> node, Object val) {
		super(message);
		this.node = node;
		this.val = val;
	}
	
	public BPNode<?, ?> getNode() {
		return node;
	}
	
	public Object getVal() {
		return val;
	}
}