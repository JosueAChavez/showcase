package controller.exceptions;
/**
 * Thrown if a spatial location is not found on the spatial data structure
 */
public class DoesNotExistException extends RuntimeException {

	private static final long serialVersionUID = 697167382858252268L;
	
	public DoesNotExistException() {
	} 
	
	public DoesNotExistException(String message) {
		super(message);
	}
}