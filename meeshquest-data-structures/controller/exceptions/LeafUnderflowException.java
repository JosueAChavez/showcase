package controller.exceptions;
/**
 * Thrown if a B+ leaf is underflown
 */
public class LeafUnderflowException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private Object val;
	
	public LeafUnderflowException(Object val) {
		this.val = val;
	} 
	
	public LeafUnderflowException(String message, Object val) {
		super(message);
		this.val = val;
	}
	
	public Object getVal() {
		return val;
	}
}
