package model.spatial.pmquadtree.node;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeSet;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import cmsc420.drawing.CanvasPlus;
import cmsc420.geom.Circle2D;
import controller.exceptions.DoesNotExistException;
import model.mappart.MapPart;
import model.mappart.cities.CityType;
import model.spatial.NameRank;

public abstract class PMNode {
	public abstract PMNode add(MapPart g, Point2D.Double center, double width, double height) throws DoesNotExistException;
	public abstract PMNode remove(MapPart g);
	/**
	 * Returns the XML tag representation implemented PMNode
	 */
	public abstract Element getNodeElement(Document doc);
	public abstract void rangeSearch(TreeSet<MapPart> result, Circle2D circle, CityType type);
	public abstract MapPart nearestSearch(Set<MapPart> visited, PriorityQueue<NameRank> pq, Point2D point, CityType type);
	public abstract MapPart nearestCityToRoad(Set<MapPart> visited, PriorityQueue<NameRank> pq, Line2D line);
	public abstract void saveNode(CanvasPlus canvas);
}
