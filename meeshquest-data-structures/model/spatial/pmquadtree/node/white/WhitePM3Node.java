package model.spatial.pmquadtree.node.white;

import model.spatial.pmquadtree.node.black.BlackPM3Node;
import model.spatial.pmquadtree.node.black.BlackPMNode;

public class WhitePM3Node extends WhitePMNode {
	private static WhitePMNode uniq = new WhitePM3Node();
	
	private WhitePM3Node() {
	}
	
	public static WhitePMNode getInstance() {
		return uniq;
	}

	public boolean equals(Object obj) {
		return this == obj;
	}

	public BlackPMNode newBlack() {
		return new BlackPM3Node();
	}
}