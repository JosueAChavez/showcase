package model.spatial.pmquadtree.node.white;

import model.spatial.pmquadtree.node.black.BlackPM1Node;
import model.spatial.pmquadtree.node.black.BlackPMNode;

public class WhitePM1Node extends WhitePMNode{
	private static WhitePMNode uniq = new WhitePM1Node();
	
	private WhitePM1Node() {
	}
	
	public static WhitePMNode getInstance() {
		return uniq;
	}

	public boolean equals(Object obj) {
		return this == obj;
	}

	public BlackPMNode newBlack() {
		return new BlackPM1Node();
	}
}