package model.spatial.pmquadtree.node.white;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeSet;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import cmsc420.drawing.CanvasPlus;
import cmsc420.geom.Circle2D;
import controller.exceptions.DoesNotExistException;
import model.mappart.MapPart;
import model.mappart.cities.CityType;
import model.spatial.NameRank;
import model.spatial.pmquadtree.node.PMNode;
import model.spatial.pmquadtree.node.black.BlackPMNode;

public abstract class WhitePMNode extends PMNode {
	public abstract BlackPMNode newBlack();
	
	public String toString() {
		return "White Node";
	}
	
	public PMNode add(MapPart g, Point2D.Double center, double width, double height) 
			throws DoesNotExistException {
		BlackPMNode blk = newBlack();
		return blk.add(g, center, width, height);
	}

	public PMNode remove(MapPart g) {
		throw new DoesNotExistException(g.toString());
	}
	
	public Element getNodeElement(Document doc) {
		return doc.createElement("white");
	}
	
	public void rangeSearch(TreeSet<MapPart> result, Circle2D circle, CityType type) {
		return; // do nothing
	}
	
	public MapPart nearestSearch(Set<MapPart> visited, 
			PriorityQueue<NameRank> pq, Point2D point, CityType type) {
		return null;
	}

	public void saveNode(CanvasPlus canvas) {
		return; // do nothing 
	}

	public MapPart nearestCityToRoad(Set<MapPart> visited, 
			PriorityQueue<NameRank> pq, Line2D line) {
		return null; // do nothing
	}
}