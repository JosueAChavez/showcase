package model.spatial.pmquadtree.node.gray;

import java.awt.geom.Point2D;

import model.spatial.pmquadtree.node.black.BlackPM1Node;
import model.spatial.pmquadtree.node.black.BlackPMNode;
import model.spatial.pmquadtree.node.white.WhitePM1Node;
import model.spatial.pmquadtree.node.white.WhitePMNode;

public class GrayPM1Node extends GrayPMNode {

	public GrayPM1Node(Point2D.Double center, double height, double width) {
		super(center, height, width);
	}
	
	public WhitePMNode geWhiteInstance() {
		return WhitePM1Node.getInstance();
	}
	
	public BlackPMNode newBlack() {
		return new BlackPM1Node();
	}
}
