package model.spatial.pmquadtree.node.gray;

import java.awt.geom.Point2D;

import model.spatial.pmquadtree.node.black.BlackPM3Node;
import model.spatial.pmquadtree.node.black.BlackPMNode;
import model.spatial.pmquadtree.node.white.WhitePM3Node;
import model.spatial.pmquadtree.node.white.WhitePMNode;

public class GrayPM3Node extends GrayPMNode {

	public GrayPM3Node(Point2D.Double center, double height, double width) {
		super(center, height, width);
	}
	
	public WhitePMNode geWhiteInstance() {
		return WhitePM3Node.getInstance();
	}

	public BlackPMNode newBlack() {
		return new BlackPM3Node();
	}
}
