package model.spatial.pmquadtree.node.gray;

import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeSet;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import cmsc420.drawing.CanvasPlus;
import cmsc420.geom.Circle2D;
import cmsc420.geom.Inclusive2DIntersectionVerifier;
import cmsc420.geom.Shape2DDistanceCalculator;
import controller.exceptions.ViolatePMRules;
import model.mappart.MapPart;
import model.mappart.Road;
import model.mappart.cities.City;
import model.mappart.cities.CityType;
import model.spatial.NameRank;
import model.spatial.pmquadtree.node.PMNode;
import model.spatial.pmquadtree.node.black.BlackPMNode;
import model.spatial.pmquadtree.node.white.WhitePMNode;

public abstract class GrayPMNode extends PMNode {	
	private static final double XF[] = {-4.0, 4.0, -4.0, 4.0};
	private static final double YF[] = {4.0, 4.0, -4.0, -4.0};
	
	protected Rectangle2D[] regions;
	protected PMNode[] children;	
	protected double x, y, height, width;
	
	public abstract WhitePMNode geWhiteInstance();
	public abstract BlackPMNode newBlack();
	
	protected GrayPMNode(Point2D center, double height, double width) {
		children = new PMNode[4];
		regions = new Rectangle2D[4];
		
		// check if x and y doesn't violate PM rules
		if(height <= 1 || width <= 1)
			throw new ViolatePMRules("Trying to create a partition less than 1");
		
		this.x = center.getX();
		this.y = center.getY();
		this.height = height;
		this.width = width;
		
		for (int i = 0; i < regions.length; i++) {
			double cX = x, cY = y, cW = width, cH = height;
			
			cX += (cW/XF[i]);
			cW /= 2;
			cY += (cH/YF[i]);
			cH /= 2;
			
			cX -= cW/2;
			cY -= cH/2;
			
			regions[i] = new Rectangle2D.Double(cX, cY, cW, cH);
			children[i] = geWhiteInstance();
		}
	}
	
	protected boolean haveAllGrayChildren() {
		for(int i  = 0; i < children.length; i ++)
			if(!(children[i] instanceof GrayPMNode))
				return false;
		return true;
	}

	protected boolean haveAllWhiteChildren() {
		for(int i  = 0; i < children.length; i ++)
			if(children[i] != geWhiteInstance())
				return false;
		return true;
	}
	
	/**
	 * @return x value
	 */
	protected double getX() {
		return x;
	}
	/**
	 * @return y value
	 */
	protected double getY() {
		return y;
	}
	/**
	 * @return width value
	 */
	protected double getWidth() {
		return width;
	}
	/**
	 * @return height value
	 */
	protected double getHeight() {
		return height;
	}
	
	/**
	 * Looks for the only Black child in current Gray Node 
	 * and returns it and otherwise null if it's not the only
	 * child or the gray node does not contain any.
	 * @return
	 */
	protected BlackPMNode getOnlyBlackChild() {

		int numChilds = 0;
		BlackPMNode res = null;
				
		for(int i = 0; i < children.length; i++) {
			PMNode curr = children[i];
			if(curr instanceof BlackPMNode) {
				res = (BlackPMNode) curr;
				numChilds++;
			} else if (curr instanceof GrayPMNode) {
				return null;
			}
		}
		
		return numChilds == 1 ? res: null;
	}
	
	public String toString() {
		return "Gray Node X: " + x + " | Y: " + y + " | Width: " + width + " | Height: " + height;
	}
	
	public PMNode getQuadrant(int quadrant) {
		return children[quadrant];
	}

	/**
	 * Returns the XML tag representation of the gray node
	 */
	public Element getNodeElement(Document doc) {
			
		Element gTag = doc.createElement("gray");
		
		gTag.setAttribute("x", Integer.toString(Math.round((float) x)));
		gTag.setAttribute("y", Integer.toString(Math.round((float) y)));
		
		return gTag;
	}
	
	public PMNode add(MapPart g, Point2D.Double center, double width, double height) {
		
		for(int i = 0; i < children.length; i++) {
			if(MapPartIntersects(g, i)) {
				Point2D.Double newCenter = new Point2D.Double(
						Math.floor(regions[i].getX() + regions[i].getWidth()/2), 
						Math.floor(regions[i].getY() + regions[i].getHeight()/2));
				double newWidth = regions[i].getWidth(), newHeight = regions[i].getHeight();
				
				children[i] = children[i].add(g, newCenter, newWidth, newHeight);
			}
		}
		
		return this;
	}
	
	/**
	 * Determines if a City or a Road intersects a region
	 * @param g
	 * @param i
	 * @return
	 */
	private boolean MapPartIntersects(MapPart g, int i) {
		return (g instanceof City && Inclusive2DIntersectionVerifier.intersects(((City) g), regions[i]))
				|| (g instanceof Road && Inclusive2DIntersectionVerifier.intersects(((Road) g), regions[i]));
	}

	public PMNode remove(MapPart g) {
		for(int i = 0; i < children.length; i++) {
			if(MapPartIntersects(g, i)) {
				children[i] = children[i].remove(g);
			}
		}
		
		if(haveAllWhiteChildren())
			return geWhiteInstance();
		
		BlackPMNode blk = getOnlyBlackChild();
		if(blk != null)
			return blk;
		if(!haveAllGrayChildren()) {
			blk = newBlack();
			// add all geometry in this subtree into b;
			for(int i = 0; i < children.length; i++) {
				PMNode curr = children[i];
				if(curr instanceof BlackPMNode)
					blk.merge((BlackPMNode) curr);
				else if(curr instanceof GrayPMNode) // means GN is needed 
					return this;
			}
			
			if(blk.isValid())
				return blk;
		}
		
		return this;
	}
	
	public void rangeSearch(TreeSet<MapPart> result, Circle2D circle, CityType type) {
		double dist, currX, currY;
		Rectangle2D rect;
		
		currX = x - (width/2); // move towards bottom left
		currY = y - (height/2);
		
		rect = new Rectangle2D.Double(currX, currY, width, height);
		dist = Shape2DDistanceCalculator.distance(circle.getCenter(), rect);
		
		if(dist <= circle.getRadius()) { // look for ranges in this area	
			for(int i = 0; i < children.length; i++) {
				children[i].rangeSearch(result, circle, type);
			}
		}
	}
	
	public MapPart nearestSearch(Set<MapPart> visited, PriorityQueue<NameRank> pq, 
			Point2D point, CityType type) {
		
		for (int i = 0; i < children.length; i++) {
			PMNode child = children[i];
			
			if(child instanceof BlackPMNode) {
				// insert MapPart with shortest distance and greatest name
				MapPart representative = ((BlackPMNode) child).shortestDistance(type, point);
				if(representative != null) {
					if(!visited.contains(representative)) {
						double dist = representative.distancePoint(point);
						pq.add(new NameRank(child, dist, representative));
						visited.add(representative);
					}
				}
			} else if (child instanceof GrayPMNode) {
				nearestCalculation(pq, child, point);
			}
		}
		
		return null;
	}
	
	public void saveNode(CanvasPlus canvas) {
		canvas.addLine(x - width/2, y, x + width/2, y, Color.GRAY);
		canvas.addLine(x, y - height/2, x, y + height/2, Color.GRAY);
	}
	
	public MapPart nearestCityToRoad(Set<MapPart> visited, 
			PriorityQueue<NameRank> pq, Line2D line) {
		
		for (int i = 0; i < children.length; i++) {
			PMNode child = children[i];
			// TODO see if BlackPMNode case can be done in nearestCalculation
			if(child instanceof BlackPMNode) {
				// insert the only city
				City representative = ((BlackPMNode) child).getCity();
				if(representative != null) {
					if(!visited.contains(representative)) {
						double dist = line.ptLineDist(representative);
						pq.add(new NameRank(child, dist, representative));
						visited.add(representative);
					}
				}
			} else if (child instanceof GrayPMNode) {
				nearestCalculation(pq, child, line);
			}
		}
		
		return null;
	}
	
	private void nearestCalculation(PriorityQueue<NameRank> pq,
			PMNode child, Object shape) {
		Rectangle2D rect;
		double dist;
		double childX, childY, childWidth, childHeight;
		GrayPMNode childg = ((GrayPMNode) child);
		
		childWidth = childg.getWidth();
		childHeight = childg.getHeight();
		childX = childg.getX() - (childWidth/2);
		childY = childg.getY() - (childHeight/2);
		
		rect = new Rectangle2D.Double(childX, childY, childWidth, childHeight);
		
		if(shape instanceof Line2D)
			dist = Shape2DDistanceCalculator.distance((Line2D) shape, rect);
		else
			dist = Shape2DDistanceCalculator.distance((Point2D) shape, rect);

		pq.add(new NameRank(child, dist, null));

	}
}