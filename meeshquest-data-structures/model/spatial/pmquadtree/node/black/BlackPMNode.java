package model.spatial.pmquadtree.node.black;

import java.awt.Color;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeSet;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import cmsc420.drawing.CanvasPlus;
import cmsc420.geom.Circle2D;
import cmsc420.geom.Inclusive2DIntersectionVerifier;
import controller.comparators.MapPartComparator;
import controller.comparators.NearestRoadComparator;
import controller.exceptions.AlreadyMapped;
import controller.exceptions.IntersectsRoadException;
import controller.exceptions.ViolatePMRules;
import model.mappart.MapPart;
import model.mappart.Road;
import model.mappart.cities.City;
import model.mappart.cities.CityType;
import model.spatial.NameRank;
import model.spatial.pmquadtree.node.PMNode;
import model.spatial.pmquadtree.node.gray.GrayPMNode;
import model.spatial.pmquadtree.node.white.WhitePMNode;

public abstract class BlackPMNode extends PMNode {
	
	protected Set<MapPart> list;
	public abstract boolean isValid();	
	public abstract GrayPMNode newGray(Point2D.Double center, double width, double height);
	public abstract WhitePMNode getWhiteInstance();

	protected BlackPMNode () {
		list = new TreeSet<MapPart>(new MapPartComparator()); 
		
	}
	
	protected boolean validNumCities() {
		int res = 0;
		
		for(MapPart part: list)
			if(part instanceof City)
				res++;
		
		return res <= 1;
	}
	
	public Element getNodeElement(Document doc) {
		
		Element bTag = doc.createElement("black");
		
		bTag.setAttribute("cardinality", Integer.toString(list.size()));
		
		for(MapPart i: list) {			
			Element curr = i.getXML(doc, null);
			bTag.appendChild(curr);
		}
		
		return bTag;
	}
	
	public String toString() {
		return "Black Node: " + list.toString() + "\n";
	}
	
	public PMNode add(MapPart g, Point2D.Double center, double width, double height) {
		intersectCheck(g);

		if(list.contains(g))
			throw new AlreadyMapped(g.toString());
		else
			list.add(g);
		
		if(isValid())
			return this;
		return partition(center, width, height);
	}

	/**
	 * Merges the MapParts of toMerge to current Black Node 
	 * @param curr
	 */
	public void merge(BlackPMNode toMerge) {
		for(MapPart part: toMerge.list)
			list.add(part);
	}
	
	public PMNode remove(MapPart g) {
		list.remove(g);
		if(list.size() == 0)
			return getWhiteInstance();
		else
			return this;
	}

	public PMNode partition(Point2D.Double center, double width, double height) {
		GrayPMNode g = newGray(center, width, height);
		for (MapPart i: list)	
			g.add(i, center, width, height);
		
		return g;
	}
	
	public void rangeSearch(TreeSet<MapPart> result, Circle2D circle, CityType type) {

		Double dist;
		boolean cond = false;
		
		for(MapPart part: list) {
			if(type == CityType.ISOLATED && part instanceof City) {
				dist = ((City) part).distancePoint(circle.getCenter());
				cond = (dist <= circle.getRadius());
			} else if(type == CityType.CONNECTED && part instanceof Road)
				cond = ((Road) part).ptSegDist(circle.getCenter()) <= circle.getRadius(); 
			else
				cond = false;
			
			if(cond)
				result.add(part);
		}
	}
	
	public MapPart nearestSearch(Set<MapPart> visited, 
			PriorityQueue<NameRank> pq, Point2D point, CityType type) {
		MapPart result = null;
		double shortest = Double.MAX_VALUE;
		NearestRoadComparator comp = new NearestRoadComparator();
		
		for(MapPart part: list) {
			if(part instanceof City) {
				City city = (City) part;
				// return the city if it matches type
				boolean validCity = ((type == CityType.NEITHER && 
						city.getCityType() == CityType.CONNECTED) || 
						(type == CityType.ISOLATED && 
						city.getCityType() == CityType.ISOLATED));
				if(validCity)
					return part;
			} else if(part instanceof Road && type == CityType.CONNECTED) {
				double dist = ((Road) part).ptLineDist(point);
				if(dist < shortest) { 
					shortest = dist;
					result = part;
				// handle case when distances are same
				} else if (Double.compare(dist, shortest) == 0) {
					if(comp.compare((Road) part, (Road) result) > 0)
						result = part;
				}
			}
		}
		return result;
	}
	/**
	 * Finds the shortest distance from point to all the of MapPart parts
	 * @param type
	 * @param point
	 * @return
	 */
	public MapPart shortestDistance(CityType type, Point2D point) {
		
		MapPart result = null;
		double shortest = Double.MAX_VALUE;
		
		for(MapPart part: list) {
			if(part instanceof City && (type == CityType.NEITHER 
				|| type == CityType.ISOLATED)) {
				return part;
			} else if(part instanceof Road && type == CityType.CONNECTED) {
				double dist = ((Road) part).ptLineDist(point);
				if(dist < shortest) { 
					shortest = dist;
					result = part;
				// handle case when distances are same
				} else if (Double.compare(dist, shortest) == 0) {
					NearestRoadComparator comp = new NearestRoadComparator();
					if(comp.compare((Road) part, (Road) result) > 0)
						result = part;
				}
			}
		}
		return result;	
	}
	
	public void saveNode(CanvasPlus canvas) {
		
		for(MapPart part: list) {
			if(part instanceof City) {
				String cityName = ((City) part).getName();
				double cityX = ((City) part).getX(), cityY = ((City) part).getY();
				canvas.addPoint(cityName, cityX, cityY, Color.BLACK);
			} else {
				double x1 = ((Road) part).getX1(), x2 = ((Road) part).getX2(),
						y1 = ((Road) part).getY1(), y2 = ((Road) part).getY2();
				canvas.addLine(x1, y1, x2, y2, Color.BLACK);
			}
		}
	}
	
	public MapPart nearestCityToRoad(Set<MapPart> visited, 
			PriorityQueue<NameRank> pq, Line2D line) {
		return getCity();
	}

	/**
	 * Gets the city if the PMBlackNode has one
	 * @return city or null if it does not have one
	 */
	public City getCity() {
		for(MapPart part: list)
			if(part instanceof City)
				return (City) part;
		
		return null;
	}
	
	// check in the case when a part intersects an existing road
	private void intersectCheck(MapPart g) {
		if(g instanceof Road) {
			for(MapPart part: list) {
				Road gRoad = (Road) g;
				if(part instanceof Road && ((Road) part).intersectsLine(((Road) g))
						&& !(((Road) part).inRoad(gRoad.getStartCity())
						|| ((Road) part).inRoad(gRoad.getEndCity()))) {
					throw new IntersectsRoadException(g + " intersects " + part);
				}
			}
		} else if(g instanceof City) {
			for(MapPart part: list) {
				if ((part instanceof Road
						&& Inclusive2DIntersectionVerifier.intersects((City) g, (Road) part))
						&& !((Road) part).inRoad((City) g)) {
					throw new ViolatePMRules(g + " is intersecting with " + part);
				}
			}
		}
	}
}