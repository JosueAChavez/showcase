package model.spatial.pmquadtree.node.black;

import java.awt.geom.Point2D.Double;

import model.mappart.MapPart;
import model.mappart.Road;
import model.mappart.cities.City;
import model.spatial.pmquadtree.node.gray.GrayPM1Node;
import model.spatial.pmquadtree.node.gray.GrayPMNode;
import model.spatial.pmquadtree.node.white.WhitePM1Node;
import model.spatial.pmquadtree.node.white.WhitePMNode;

public class BlackPM1Node extends BlackPMNode {
	public BlackPM1Node() {
		super();
	}
	
	public boolean isValid() {
		if (!validNumCities())
			return false;
		// If a quadtree leaf node's region contains a vertex, it can contain no q-edge
		// that does not include that vertex.
		
		// get vertex to check with
		City cityToCheck = null;
		boolean hasCity = false;
		for(MapPart part: list) {
			if(part instanceof City) {
				cityToCheck = (City) part;
				hasCity = true;
				break;
			}
		}
		
		if(hasCity) {
			for(MapPart part: list) {
				if(part instanceof Road && !(((Road) part).inRoad(cityToCheck)))
					return false;	
			}
		}
		
		// If a quadtree leaf node's region contains no vertices, it can contain, at most,
		// one q-edge.
		if(!hasCity && list.size() != 1)
			return false;
		
		return true;
	}

	public GrayPMNode newGray(Double.Double center, double width, double height) {
		return new GrayPM1Node(center, width, height);
	}
	
	public WhitePMNode getWhiteInstance() {
		return WhitePM1Node.getInstance();
	}
}
