package model.spatial.pmquadtree.node.black;

import java.awt.geom.Point2D;

import model.spatial.pmquadtree.node.gray.GrayPM3Node;
import model.spatial.pmquadtree.node.gray.GrayPMNode;
import model.spatial.pmquadtree.node.white.WhitePM3Node;
import model.spatial.pmquadtree.node.white.WhitePMNode;

public class BlackPM3Node extends BlackPMNode {
	public BlackPM3Node() {
		super();
	}
	
	public boolean isValid() {
		return validNumCities();
	}

	public GrayPMNode newGray(Point2D.Double center, double width, double height) {
		return new GrayPM3Node(center, height, width);
	}
	
	public WhitePMNode getWhiteInstance() {
		return WhitePM3Node.getInstance();
	}
}