package model.spatial.pmquadtree.validator;

public abstract class Validator {
	public abstract boolean isValid();
}
