package model.spatial.pmquadtree;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.HashSet;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Stack;
import java.util.TreeSet;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import cmsc420.drawing.CanvasPlus;
import cmsc420.geom.Circle2D;
import cmsc420.geom.Inclusive2DIntersectionVerifier;
import controller.exceptions.AlreadyMapped;
import controller.exceptions.DoesNotExistException;
import controller.exceptions.IntersectsRoadException;
import controller.exceptions.ViolatePMRules;
import model.Outcome;
import model.Variables;
import model.mappart.MapPart;
import model.mappart.Road;
import model.mappart.cities.Airport;
import model.mappart.cities.City;
import model.mappart.cities.CityType;
import model.mappart.cities.Terminal;
import model.spatial.NameRank;
import model.spatial.Spatial;
import model.spatial.pmquadtree.node.PMNode;
import model.spatial.pmquadtree.node.black.BlackPM1Node;
import model.spatial.pmquadtree.node.black.BlackPM3Node;
import model.spatial.pmquadtree.node.black.BlackPMNode;
import model.spatial.pmquadtree.node.gray.GrayPMNode;
import model.spatial.pmquadtree.node.white.WhitePMNode;

public class PMQuadTree <K extends MapPart> implements Iterable<PMNode>, Spatial<MapPart> {

	private int width, height, order;
	private PMNode root;
	
	public PMQuadTree(int width, int height, int order) {
		this.width = width;
		this.height = height;
		this.order = order;
		root = null;		
	}
	
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public Outcome map(MapPart part) {

		String caller = getCaller(part), output = "map" + caller;
		
		if(root == null) // The tree at root is initially empty
			root = (order == 3) ? new BlackPM3Node() : new BlackPM1Node();
		
		try {
			root = root.add(part, new Point2D.Double(width/2, height/2), width, height);
		} catch(AlreadyMapped | IntersectsRoadException | ViolatePMRules e) {
			
			if(e instanceof AlreadyMapped) {
				if(part instanceof Road)
					return new Outcome(Variables.ERROR, output, "roadAlreadyMapped");
				else
					return new Outcome(Variables.SUCCESS, output);
			}
			
			// convert to lowercase for errors
			caller = caller.substring(0, 1).toLowerCase() + caller.substring(1); 

			try {
				unmap(part);
			} catch (DoesNotExistException f) {} // do nothing 
			
			if(e instanceof IntersectsRoadException) 
				return new Outcome(Variables.ERROR, output, caller + "IntersectsAnotherRoad");
			else {
				if(caller.equals("city"))
					caller = "road";
				return new Outcome(Variables.ERROR, output, caller + "ViolatesPMRules");
			}
		}
			
		return new Outcome(Variables.SUCCESS, output);
	}
	
	public Outcome unmap(MapPart part) {
		String caller = "unmap" + getCaller(part);
		
		if(root == null)
			throw new DoesNotExistException("root is empty, can't umap " + part);
		
		root = root.remove(part);
		
		if(root instanceof WhitePMNode)
			root = null;
		
		if(part instanceof City)
			((City) part).changeState(CityType.NEITHER);
		
		return new Outcome(Variables.SUCCESS, caller);
	}
	
	/**
	 * Given a qedge (aka road), map it and it's cities to the PMQuadTree 
	 * @param road
	 * @return
	 */
	public Outcome mapQEdge(Road road, City startCity, City endCity) {
		
		Outcome outcome = new Outcome(Variables.ERROR, "mapRoad"), errOutcome = outcome;
		outcome.addParameter("start", startCity.getName());
		outcome.addParameter("end", endCity.getName());

		// map the road
		outcome = map(road);
		if(!outcome.isSuccessful())
			return errOutcome.settypeAttr(outcome.gettypeAttr());
		
		Outcome tempOut = map(startCity);
		if(!tempOut.isSuccessful()) {
			unmap(road);
			return errOutcome.settypeAttr(tempOut.gettypeAttr());
		}
		
		tempOut = map(endCity);
		if(!tempOut.isSuccessful()) {
			unmap(road);
			unmap(startCity);
			return errOutcome.settypeAttr(tempOut.gettypeAttr());
		}
		
		// change states and map if they haven't already been mapped
		startCity.changeState(CityType.CONNECTED);
		endCity.changeState(CityType.CONNECTED);

		outcome.addParameter("start", startCity.getName());
		outcome.addParameter("end", endCity.getName());

		return outcome;
	}

	public TreeSet<MapPart> rangeCities(int x, int y, int radius) {
		return rangeHelper(x, y, radius, CityType.ISOLATED);
	}
	
	public TreeSet<MapPart> rangeRoads(int x, int y, int radius) {
		return rangeHelper(x, y, radius, CityType.CONNECTED);
	}

	public Outcome toOutcome(Document writer) {
		
		// super(writer, "printPMQuadtree");
		
		Outcome outcome = new Outcome(Variables.SUCCESS, "printPMQuadtree");
		Stack<Element> elements = new Stack<Element>();
		Element currentElement;
		Element nodeElement;
		Element rootOutput;
		
		// check if map is empty
		if (root == null)
			return new Outcome(Variables.ERROR, "printPMQuadtree", "metropoleIsEmpty");
		
		nodeElement = writer.createElement("dummy");
		rootOutput = nodeElement;   
		elements.push(nodeElement);
		outcome.setOutputTag("quadtree");
		
		for(PMNode currentNode: this) {
			currentElement = elements.pop();
			nodeElement = currentNode.getNodeElement(writer);
			currentElement.appendChild(nodeElement);
			
			if(currentNode instanceof GrayPMNode) {
				for (int i = 3; i >= 0; i--) {
					elements.push(nodeElement);
				}
			}
		}
		
		// had to mimic output tag
		outcome.addOutput((Element) rootOutput.getFirstChild(), order);
		return outcome;
	}

	@Override
	public Iterator<PMNode> iterator() {
		
		Stack<PMNode> stack = new Stack<PMNode>();
		if (root != null) {
			stack.push(root);
		}
		
		Iterator<PMNode> it = new Iterator<PMNode>() {
		
		    public boolean hasNext() {
		        return !stack.isEmpty();
		    }
		
		    public PMNode next() {
		    	PMNode node = stack.pop();
		    	
		    	if(node instanceof GrayPMNode) {
		    		GrayPMNode gnode = (GrayPMNode) node;
					for (int i = 3; i >= 0; i--) {
						stack.push(gnode.getQuadrant(i));
					}
		    	}
		    	
		    	return node;
		    }
		
		    public void remove() {
		        throw new UnsupportedOperationException();
		    }
		};
		return it;
	}
	
	public Outcome saveMap(String name, CanvasPlus canvas) {		
		for(PMNode node: this)
			node.saveNode(canvas);
		
		return new Outcome(Variables.SUCCESS, "saveMap");
	}
	
	public Spatial<MapPart> reset() {
		root = null; // TODO might not need
		return new PMQuadTree<MapPart>(width, height, order);
	}
	
	public boolean isEmpty() {
		return root == null;
	}
	
	public boolean cityIntersects(City city) {
		Rectangle2D rect = new Rectangle2D.Double(0, 0, width, height);
		return Inclusive2DIntersectionVerifier.intersects(city, rect);
	}
	
	private TreeSet<MapPart> rangeHelper(int x, int y, int radius, CityType type) {
		
		TreeSet<MapPart> result = new TreeSet<MapPart>();

		if(!isEmpty()) {
			Circle2D circle = new Circle2D.Double(x, y, radius);
			root.rangeSearch(result, circle, type);
		}
				
		return result;
	}
	
	public MapPart nearest(int x, int y, CityType type) {
		PriorityQueue<NameRank> pq = new PriorityQueue<NameRank>(); 
		Point2D point = new Point2D.Double(x, y);
		PMNode node;
		MapPart result = null;
		HashSet<MapPart> visited = new HashSet<MapPart>();
			
		// handle base cases
		if(!isEmpty()) {
			if(root instanceof BlackPMNode) {
				// ensure that the city matches the query type
				MapPart part = ((BlackPMNode) root).shortestDistance(type, point);
				System.err.println(root);
				if(((City) part).getCityType() == type)
					return part;
			} else {
				pq.add(new NameRank(root, 0, null));
			}
		}
		
		while(!pq.isEmpty()) {
			node = pq.remove().getNode();
			
			result = node.nearestSearch(visited, pq, point, type);
			if(result != null)
				return result;
		}
		return result;
	}

	public MapPart nearestCityToRoad(Road road) {	
		PriorityQueue<NameRank> pq = new PriorityQueue<NameRank>();
		HashSet<MapPart> visited = new HashSet<MapPart>();
		MapPart result = null;
		PMNode node;
		
		// handle base cases
		if(!isEmpty()) {
			if(root instanceof BlackPMNode)
				return ((BlackPMNode) root).getCity();
			 else
				pq.add(new NameRank(root, 0, null));
		}
		
		while(!pq.isEmpty()) {
			node = pq.remove().getNode();
			
			result = node.nearestCityToRoad(visited, pq, road);
			if(result != null) {
				if(!(road.getStartCity().equals(result) || 
					road.getEndCity().equals(result))) {
					return result;
				} else {
					result = null;
				}
			}
		}
		return result;
	}


	// Convert MapPart type into a string
	private String getCaller(MapPart part) {
		String caller = null;

		if(part instanceof Airport)
			caller = "Airport";
		else if(part instanceof Terminal)
			caller = "Terminal";
		else if(part instanceof City)
			caller = "City";
		else if(part instanceof Road)
			caller = "Road";
		
		return caller;
	}
}