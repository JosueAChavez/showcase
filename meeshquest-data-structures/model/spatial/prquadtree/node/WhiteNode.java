package model.spatial.prquadtree.node;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * A singleton of the White Node used in a
 * PRQuadTree (or trie).
 * @author Josue A. Chavez
 *
 */
public final class WhiteNode implements Node {
	
	private static WhiteNode uniq = new WhiteNode();
	
	private WhiteNode() {
	}
	
	public static WhiteNode getInstance() {
		return uniq;
	}

	// TODO THINK OF A BETTER WAY OF DOING THIS PLS
	public double getX() {
		return -1;
	}

	public double getY() {
		return -1;
	}
	
	public String toString() {
		return "White Node";
	}
	
	/**
	 * Returns the XML tag representation of the white node
	 */
	public Element getNodeElement(Document doc) {
			
		Element wTag = doc.createElement("white");
		
		return wTag;
	}
	
	public boolean equals(Object obj) {
		return this == obj;
	}
	
	public String getName() {
		return "White Node";
	}
}