package model.spatial.prquadtree.node;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public interface Node {
	public double getX();
	public double getY();
	public String toString();
	public Element getNodeElement(Document doc);
	public boolean equals(Object obj);
	public String getName();
}
