/**
 * The definition of Gray Node. Used in a PRQuadTree.
 * @author Josue A. Chavez
 *
 */
package model.spatial.prquadtree.node;

import java.awt.geom.Point2D;
import java.util.Iterator;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

@SuppressWarnings("serial")
public class GrayNode extends Point2D.Float implements Node, Iterable<Node>{
	
	private Node quad0, quad1, quad2, quad3;
	private int width, height;
	
	public GrayNode(float x, float y, int width, int height) {
		super.x = x;
		super.y = y;
		
		this.height = height;
		this.width = width;
		
		quad0 = WhiteNode.getInstance();
		quad1 = WhiteNode.getInstance();
		quad2 = WhiteNode.getInstance();
		quad3 = WhiteNode.getInstance();
	}
	
	public double getX() {
		return super.getX();
	}
	
	public double getY() {
		return super.getY();
	}
	
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	/**
	 * Sets the node on a specific quadrant depending on quadrant number 
	 * @param node
	 * @param quadrant
	 */
	public boolean setQuadrant(Node node, int quadrant) {
		switch(quadrant) {
			case 0:
				quad0 = node;
				break;
			case 1:
				quad1 = node;
				break;
			case 2:
				quad2 = node;
				break;
			case 3:
				quad3 = node;
				break;
			default:
				return false;
		}
		return true;
	}
	
	/**
	 * Returns a Node whose quadrant number it belongs to
	 * @param node
	 * @param quadrant
	 * @return
	 */
	public Node getQuadrant(int quadrant) {
		switch(quadrant) {
			case 0:
				return quad0;
			case 1:
				return quad1;
			case 2:
				return quad2;
			case 3:
				return quad3;
		}
		
		return null;
	}
	
	/**
	 * Returns a newly replaced Node whose quadrant 
	 * number it belongs to, assuming quadrant is
	 * correct 
	 * @param node
	 * @param quadrant
	 */
	public void removeQuadrant(int quadrant) {
		setQuadrant(WhiteNode.getInstance(), quadrant);
	}
	
	/**
	 * Looks for the only Black child in current Gray Node 
	 * and returns it and otherwise null if it's not the only
	 * child or the gray node does not contain any.
	 * @return
	 */
	public Node getOnlyBlackChild() {

		int numChilds = 0;
		Node res = null;
				
		for(int i = 0; i < 4; i++) {
			Node curr = getQuadrant(i);
			if(curr instanceof BlackNode) {
				res = curr;
				numChilds++;
			} else if (curr instanceof GrayNode) {
				return null;
			}
		}
		
		return numChilds == 1 ? res: null;
	}
	
	public String toString() {
		return "Gray Node X: " + x + " | Y: " + y + " | Width: " + width + " | Height: " + height;
	}

	/**
	 * Returns the XML tag representation of the gray node
	 */
	public Element getNodeElement(Document doc) {
			
		Element gTag = doc.createElement("gray");
		
		gTag.setAttribute("x", Integer.toString(Math.round(this.x)));
		gTag.setAttribute("y", Integer.toString(Math.round(this.y)));
		
		return gTag;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof GrayNode))
			return false;
		GrayNode other = (GrayNode) obj;
		return getX() == other.getX() && getY() == other.getY();
	}

	/**
	 * return true if the current gray node exactly one gray node
	 * @return
	 */
	public boolean hasOneGrayChild() {
		int numChilds = 0;
				
		for(int i = 0; i < 4; i++) {
			Node curr = getQuadrant(i);
			if(curr instanceof GrayNode) {
				numChilds++;
			} else if (curr instanceof BlackNode) {
				return false;
			}
		}
		
		return numChilds == 1;
	}

	public String getName() {
		return "Gray Node";
	}
	
	public Iterator<Node> iterator() {
				
		Iterator<Node> it = new Iterator<Node>() {
			int currQuad = 0;

		    public boolean hasNext() {
		        return currQuad < 4;
		    }
		
		    public Node next() {
		    	return getQuadrant(currQuad++);
		    }
		
		    public void remove() {
		        throw new UnsupportedOperationException();
		    }
		};
		return it;
	}	

}
