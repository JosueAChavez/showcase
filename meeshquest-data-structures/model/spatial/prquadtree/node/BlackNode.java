/**
 * The definition of Black Node. Used in a PRQuadTree.
 * @author Josue A. Chavez
 *
 */
package model.spatial.prquadtree.node;

import java.awt.geom.Point2D;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

@SuppressWarnings("serial")
public class BlackNode extends Point2D.Float implements Node {

	private String name;
	
	public BlackNode(String name, double x, double y) {
		
		this.name = name;
		super.x = new java.lang.Float(x);
		super.y = new java.lang.Float(y);
	}
	
	public double getX() {
		return super.getX();
	}
	
	public double getY() {
		return super.getY();
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return "Black Node X: " + x + " | Y: " + y + " | name: " + name;
	}
	
	/**
	 * Returns the XML tag representation of the black node
	 */
	public Element getNodeElement(Document doc) {
			
		Element bTag = doc.createElement("black");
		
		bTag.setAttribute("name", name);
		bTag.setAttribute("x", Integer.toString(Math.round(x)));
		bTag.setAttribute("y", Integer.toString(Math.round(y)));
		
		return bTag;
	}
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof BlackNode))
			return false;
		BlackNode other = (BlackNode) obj;
		return getX() == other.getX() && getY() == other.getY() /*&& name.equals(other.getName())*/;
	}
}
