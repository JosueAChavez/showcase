package model.spatial.prquadtree;


import java.awt.Color;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Collections;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.Stack;
import java.util.TreeSet;
import java.util.PriorityQueue;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import cmsc420.drawing.CanvasPlus;
import cmsc420.geom.Inclusive2DIntersectionVerifier;
import cmsc420.geom.Shape2DDistanceCalculator;
import model.Outcome;
import model.Variables;
import model.dictionary.Dictionary;
import model.mappart.MapPart;
import model.mappart.Road;
import model.mappart.cities.City;
import model.mappart.cities.CityType;
import model.spatial.Spatial;
import model.spatial.prquadtree.node.*;

public class PRQuadTree <K extends City> implements Iterable<Node>, Spatial<City> {
	
	private int width, height;
	private Node root;
	
	public PRQuadTree (int width, int height) {
		this.width = width;
		this.height = height;
		root = null;
	}
	
	public int getWidth() {
		return width;
	}


	public int getHeight() {
		return height;
	}

	/**
	 * Maps a given City object.
	 * @param city
	 * @return
	 */
	public Outcome mapCity(City city) {
		
		if(city == null) {
			return new Outcome(Variables.ERROR, "mapCity", "nameNotInDictionary");	
		}
		
		BlackNode item = new BlackNode(city.getName(), city.getX(), city.getY());
		
		if (isOutOfBounds(city)) {
			return new Outcome(Variables.ERROR, "mapCity", "cityOutOfBounds");
		} else if (insert(item)) {
			return new Outcome(Variables.SUCCESS, "mapCity");
		} else {
			return new Outcome(Variables.ERROR, "mapCity", "cityAlreadyMapped");
		}
	}

	/**
	 * Unmaps a given City object.
	 * @param city
	 * @return
	 */
	public Outcome unmapCity(City city) {
		if(city == null) {
			return new Outcome(Variables.ERROR, "unmapCity", "nameNotInDictionary");
		} 
		
		BlackNode item = new BlackNode(city.getName(), city.getX(), city.getY());

		 if (delete(item)) {
			 return new Outcome(Variables.SUCCESS, "unmapCity");
		 } else {
		  	return new Outcome(Variables.ERROR, "unmapCity", "cityNotMapped");
		 }
	}
	
	public boolean isEmpty() {
		return root == null;
	}

	private boolean isOutOfBounds(City city) {
		return city.getX() >= width || city.getY() >= height; 
	}	

	/**
	 * 
	 * @param P - Node to potentially insert
	 * @param x - X location of point
	 * @param y - Y location of point
	 * @param lx - width of tree
	 * @param ly - height of tree
	 */
	private boolean insert(BlackNode P) {
		
		float x = width/2, y = height/2;
		int lx = width, ly = height;
		
		Node nodeT, nodeU;
		int quadQ, quadUQ;
		
		final int XF[] = {-1, 1, -1, 1};
		final int YF[] = {1, 1, -1, -1};
		
		if(root == null) { // The tree at R is initially empty
			root = P;
			return true;
		} else if (!(root instanceof GrayNode)) {		
			if(P.equals(root)) {
				return false;
			} else { // Special handling when the tree initially contains one node
				nodeU = root;
				root = new GrayNode(x, y, lx, ly);
				quadQ = determineQuadrant(nodeU, x, y);				
				((GrayNode) root).setQuadrant(nodeU, quadQ);
			}
		}
		nodeT = root;
		quadQ = determineQuadrant(P, x, y);
				
		while(((GrayNode) nodeT).getQuadrant(quadQ) instanceof GrayNode) {
			nodeT = ((GrayNode) nodeT).getQuadrant(quadQ);
			
			x = x + (XF[quadQ] * lx/4);
			lx = lx/2;
			y = y + (YF[quadQ] * ly/4);
			ly = ly/2;
			quadQ = determineQuadrant(P, x, y);
		}
		
		if (((GrayNode) nodeT).getQuadrant(quadQ) instanceof WhiteNode) {
			((GrayNode) nodeT).setQuadrant(P, quadQ); 
		} else if (P.equals(((GrayNode) nodeT).getQuadrant(quadQ))) {
			return false;
		} else {
			
			nodeU = ((GrayNode) nodeT).getQuadrant(quadQ);
			
			do {
				x = x + (XF[quadQ] * lx/4);
				lx = lx/2;
				y = y + (YF[quadQ] * ly/4);
				ly = ly/2;
				
				((GrayNode) nodeT).setQuadrant(new GrayNode(x, y, lx, ly), quadQ);
				nodeT = ((GrayNode) nodeT).getQuadrant(quadQ);
				
				quadQ = determineQuadrant(P, x, y);
				quadUQ = determineQuadrant(nodeU, x, y);
			} while (quadQ == quadUQ);
			((GrayNode) nodeT).setQuadrant(P, quadQ);
			((GrayNode) nodeT).setQuadrant(nodeU, quadUQ);
		}
		
		return true;
	}
	
	private boolean delete(BlackNode node) {
		
		Stack<Node> stack = new Stack<Node>();
		Stack<GrayNode> parents = new Stack<GrayNode>();
		Stack<Integer> directions = new Stack<Integer>();
		Node curr;
		
		if(node.equals(root)) {
			root = null;
    		return true;
		}
		if (root != null) {
			stack.push(root);
		}
		
		// find the node to remove from tree
		while(!stack.isEmpty()) {
	    	curr = stack.pop();
	    	
	    	if (curr instanceof GrayNode) {
	    		GrayNode parent = (GrayNode) curr;
				int childQuad = determineQuadrant(node, parent.getX(), parent.getY());
	    		Node childNode = parent.getQuadrant(childQuad);
	    		
				if (childNode.equals(node)) {
					parent.removeQuadrant(childQuad);
					
					Node orphan = parent.getOnlyBlackChild();
					
					// backtrack through visited nodes
					if (orphan != null) {
						GrayNode fosterParent = null;
						int orhpanQuad = 0;

						do {
							try {
								fosterParent = parents.pop();
								orhpanQuad = directions.pop();
							} catch (EmptyStackException e) {
								root = orphan;
								break;
							}
						} while (fosterParent.hasOneGrayChild());
						
						if(!root.equals(orphan)) { // case when root = orphan 
							fosterParent.setQuadrant(orphan, orhpanQuad);
						}
					}
					
					return true;

				} else {
					stack.push(childNode);
					parents.push(parent);
					directions.push(childQuad);
				}
	    	}
		}
		return false;
	}
	
	/**
	 * Uses the radius and the x and y coordinates to 
	 * determine what cities are within that range.
	 * @param x
	 * @param y
	 * @param radius
	 * @return
	 */
	public TreeSet<String> rangeCities(int x, int y, int radius, Dictionary dict) {
		
		TreeSet<String> result = new TreeSet<String>(Collections.reverseOrder());
		Node node = new BlackNode("", x, y), curr;
		Stack<Node> stack = new Stack<Node>();
		double dist, currX, currY;
		int width, height;
		
		if(root == null) {
			return result;
		} else if (root.equals(node)) {
			result.add(((BlackNode) root).getName());
		} else {
			stack.push(root);
		}
		
		Rectangle2D rect;
		Point2D pt = new Point2D.Double(x, y);
		
		
		while(!stack.isEmpty()) {
			curr = stack.pop();
			if(curr instanceof BlackNode) {
				dist = dict.get(curr.getName()).distance(pt);
				
				if(dist <= radius) {
					result.add(curr.getName());
				}
				
			} else if (curr instanceof GrayNode) {
				width = ((GrayNode) curr).getWidth();
				height = ((GrayNode) curr).getHeight();
				currX = curr.getX() - (width/2);
				currY = curr.getY() - (height/2);
				
				rect = new Rectangle2D.Double(currX, currY, width, height);
				dist = Shape2DDistanceCalculator.distance(pt, rect);
						
				for(Node child : (GrayNode) curr) {
					if(dist <= radius) {
						stack.add(child);
					}
				}
			}
		}
				
		return result;
	}

	/**
	 * Returns the name of the closest city
	 * @param x
	 * @param y
	 * @return
	 */
	public String nearestCity(int x, int y, Dictionary dict) {
		
		/*PriorityQueue<NameRank> pq = new PriorityQueue<NameRank>(); 
		Rectangle2D rect = new Rectangle2D.Double(0,0 , x, y);*/
		Point2D pt = new Point2D.Double(x, y);
		double dist;
		/*Node node;
		NameRank element = null;*/
		double bestDist = Double.MAX_VALUE/*, currX, currY*/;
		Node bestNode = root;

		for(Node ele : this) {
			if(ele instanceof BlackNode) {
				dist = ((BlackNode) ele).distance(pt);
				if(dist <= bestDist) {
					if(dist == bestDist) { // same dist, diff name
						if(ele.getName().compareTo(bestNode.getName()) > 0) {
							bestNode = ele;
						}
					} else {
						bestNode = ele;
						bestDist = dist;
					}
				}
			}
		}
		
		if(isEmpty()) {
			return null;
		}
		
		return bestNode.getName();
		
/*		if(!isEmpty()) {
			pq.add(new NameRank(root, 0));
		}
		
		
		while(!pq.isEmpty()) {
			element = pq.remove();
			node = element.node;
			
			if (node instanceof BlackNode) { // Base case
				return node.getName();
			} else if (node instanceof GrayNode) {
				for (Node child: node) {
					
					if(child instanceof BlackNode) {
						dist = ((BlackNode) child).distance(pt);						
						pq.add(new NameRank(child, dist));
					} else if (child instanceof GrayNode) {
						width = ((GrayNode) child).getWidth();
						height = ((GrayNode) child).getHeight();
						currX = child.getX() - (width/2);
						currY = child.getY() - (height/2);
						
						rect = new Rectangle2D.Double(currX, currY, width, height);
						dist = Shape2DDistanceCalculator.distance(pt, rect);
						pq.add(new NameRank(child, dist));
					}
				}
			}
		}
		return null;*/
	}
	/**
	 * @param node
	 * @param x
	 * @param y
	 * @return the quadrant Node P belongs to 
	 * 0 - NW
	 * 1 - NE
	 * 2 - SW
	 * 3 - SE
	 */
	private int determineQuadrant(Node node, double x, double y) {
		if(node.getX() < x) {
			if (node.getY() < y) {
				return 2; //"SW";
			} else {
				return 0; //"NW";
			}
		} else {
			if (node.getY() < y) {
				return 3; //"SE";
			} else {
				return 1; //"NE";
			}
		}
	}

	/**
	 * Prints the node tree in a XML format
	 * @param writer
	 * @return
	 */
	public Outcome toOutcome(Document writer) {
		
		Outcome outcome = new Outcome(Variables.SUCCESS, "printPRQuadtree");
		Stack<Element> elements = new Stack<Element>();
		Element currentElement;
		Element nodeElement;
		Element rootOutput;
		
		// check if map is empty
		if (root == null)
			return new Outcome(Variables.ERROR, "printPRQuadtree", "mapIsEmpty");
		
		nodeElement = root.getNodeElement(writer);
		rootOutput = nodeElement;   
		elements.push(nodeElement);
		outcome.setOutputTag("quadtree");
		
		for(Node currentNode: this) {
			currentElement = elements.pop();
			nodeElement = currentNode.getNodeElement(writer);
			currentElement.appendChild(nodeElement);
			
			if(currentNode instanceof GrayNode) {				
				for (int i = 3; i >= 0; i--) {
					elements.push(nodeElement);
				}
			}
			
		}
		
		// get the only child as it is the real output, otherwise will have duplicate
		outcome.addOutput((Element) rootOutput.getFirstChild());
		return outcome;
	}
	
	/**
	 * Generates an image that displays the current PRQuadTree structure
	 * @param name
	 * @param canvas
	 * @return
	 */
	public Outcome saveMap(String name, CanvasPlus canvas) {
		Outcome outcome = new Outcome(Variables.SUCCESS, "saveMap");
		
		double x, y;
		String cityName;
		
		for(Node node: this) {
			x = node.getX();
			y = node.getY();
			
			// add points or lines depending on node type
			if(node instanceof BlackNode) {
				cityName = ((BlackNode) node).getName();
				canvas.addPoint(cityName, x, y, Color.BLACK);
			} else if (node instanceof GrayNode) {
				int width = ((GrayNode) node).getWidth()/2,
					height = ((GrayNode) node).getHeight()/2;
				
				canvas.addLine(x - width, y, x + width, y, Color.BLACK);
				canvas.addLine(x, y - height, x, y + height, Color.BLACK);
			}
		}
		
		return outcome;
	}
	
	public Spatial<City> reset() {
		return new PRQuadTree<K>(width, height);
	}

	@Override
	public Outcome map(City city) {
			return mapCity(city);
	}

	@Override
	public Outcome unmap(City city) {
			return unmapCity(city);
	}

	public Iterator<Node> iterator() {
		
		Stack<Node> stack = new Stack<Node>();
		if (root != null) {
			stack.push(root);
		}
		
		Iterator<Node> it = new Iterator<Node>() {
		
		    public boolean hasNext() {
		        return !stack.isEmpty();
		    }
		
		    public Node next() {
		    	Node node = stack.pop();
		    	
		    	if(node instanceof GrayNode) {
		    		GrayNode gnode = (GrayNode) node;
					for (int i = 3; i >= 0; i--) {
						stack.push(gnode.getQuadrant(i));
					}
		    	}
		    	
		    	return node;
		    }
		
		    public void remove() {
		        throw new UnsupportedOperationException();
		    }
		};
		return it;
	}	
	
	/**
	 * An inner class used for the {@link PriorityQueue} in nearestCity 
	 * @author Josue
	 *
	 */
	protected class NameRank implements Comparable<NameRank> {
		
		private Node node;
		private double dist;
		
		public NameRank(Node node, double dist) {
			this.node = node;
			this.dist = dist;
		}

		public int compareTo(NameRank other) {
			
			int distComp = Double.compare(dist, other.dist);
			
			return distComp == 0 ? other.node.getName().compareTo(node.getName()) : distComp;
		}
	}

	public TreeSet<MapPart> rangeCities(int x, int y, int radius) {
		throw new UnsupportedOperationException();
	}

	public TreeSet<MapPart> rangeRoads(int x, int y, int radius) {
		throw new UnsupportedOperationException(); 
	}

	public boolean cityIntersects(City city) {
		Rectangle2D rect = new Rectangle2D.Double(0, 0, width, height);
		return Inclusive2DIntersectionVerifier.intersects(city, rect);
	}

	public MapPart nearest(int x, int y, CityType type) {
		throw new UnsupportedOperationException(); 
	}

	public MapPart nearestCityToRoad(Road road) {
		throw new UnsupportedOperationException();	
	}
}