package model.spatial;

import java.util.PriorityQueue;

import controller.comparators.NearestRoadComparator;
import model.mappart.MapPart;
import model.mappart.Road;
import model.mappart.cities.City;
import model.spatial.pmquadtree.node.PMNode;
/**
 * An inner class used for the {@link PriorityQueue} in nearestCity 
 * @author Josue
 *
 */
public class NameRank implements Comparable<NameRank> {
		
	private PMNode node;
	private double dist;
	private MapPart rep;
	
	public NameRank(PMNode node, double dist, MapPart rep) {
		this.node = node;
		this.dist = dist;
		this.rep = rep;
	}
	
	public PMNode getNode() {
		return node;
	}

	public int compareTo(NameRank other) {
		
		int distComp = Double.compare(dist, other.dist);
		
		if(distComp == 0 && rep != null && other.rep != null) {
			if(rep instanceof City) {
				return -1*((City) rep).getName().compareTo(((City) other.rep).getName());
			} else {
				// do reverse comparison as GrayPM
				NearestRoadComparator comp = new NearestRoadComparator();
				return comp.compare((Road) other.rep, (Road) rep);
			}
		}
		
		return distComp;
	}
	
	public String toString() {
		String repStr= "Non-Black Node";
		if(rep != null) 
			repStr = rep.toString();
		
		return repStr + ": " + dist;
	}
}