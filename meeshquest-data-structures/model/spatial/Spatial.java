package model.spatial;

import java.util.TreeSet;

import org.w3c.dom.Document;

import cmsc420.drawing.CanvasPlus;
import model.Outcome;
import model.mappart.MapPart;
import model.mappart.Road;
import model.mappart.cities.City;
import model.mappart.cities.CityType;

public interface Spatial <K> {
	
	// Old spatial
	/*public String nearestCity(int x, int y, Dictionary dict);*/
	
	public int getWidth();

	public int getHeight();

	public Outcome map(K part);

	public MapPart nearest(int x, int y, CityType type);

	public TreeSet<MapPart> rangeCities(int x, int y, int radius);
	public TreeSet<MapPart> rangeRoads(int x, int y, int radius);

	public Outcome toOutcome(Document writer);

	public Outcome unmap(K part);
	
	public Outcome saveMap(String name, CanvasPlus canvas);
	
	public Spatial<K> reset();
	
	public boolean cityIntersects(City city);

	public MapPart nearestCityToRoad(Road road);
}
