package model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Outcome {

	private static final String SUCCESS = "success";
	
	private String outcomeTag, commandAttr, outputTag, typeAttr;
	private LinkedHashMap<String, String> parametersMap;
	private ArrayList<Element> outputs;
	private int id, pmOrder;
	
	
	private Outcome(String outcomeTag, String command, String output, String typeAttr, int id) {
		this.outcomeTag = outcomeTag;
		this.commandAttr = command;
		this.parametersMap = new LinkedHashMap<String, String>();
		this.outputTag = output;
		this.outputs =  new ArrayList<Element>();
		this.typeAttr = typeAttr;
		this.id = id;
		this.pmOrder = -1;
	}
	
	/**
	 * For when there is a success
	 * @param outcomeTag
	 * @param command
	 */
	public Outcome(String outcomeTag, String command) {
		this(outcomeTag, command, "", "", -1);
	}
	
	/**
	 * For the case when there is an error
	 * @param error
	 * @param command
	 * @param typeAttr
	 */
	public Outcome(String outcomeTag, String command, String typeAttr) {
		this(outcomeTag, command, "", typeAttr, -1);
	}

	/**
	 * 
	 * @param results Document that outcome is written to 
	 * @return outcome
	 */
	public Element generateElement(Document results) {
		
		Element result = results.createElement(outcomeTag);
		
		Element command = results.createElement("command");
		command.setAttribute("name", commandAttr);
		if(id > 0) {
			command.setAttribute("id", Integer.toString(id));
		}
		result.appendChild(command);
		
		Element parameters = results.createElement("parameters");
		for(String parameter : parametersMap.keySet()) {
			Element ele = results.createElement(parameter);
			ele.setAttribute("value", parametersMap.get(parameter));
			parameters.appendChild(ele);
		}
		result.appendChild(parameters);
		
		Element output = results.createElement("output");
		if(!outputs.isEmpty()) {
			Element outputSub;
			
			if(!outputTag.equals("")) {
				outputSub = results.createElement(outputTag);
				output.appendChild(outputSub);
			} else {
				outputSub = output;
			}
			for(Element ele : outputs) {
				outputSub.appendChild(ele);
			}
			if(pmOrder > 0) {
				outputSub.setAttribute("order", Integer.toString(pmOrder));
			}
		}
		
		if(outcomeTag.equals(SUCCESS)) {
			result.appendChild(output);
		} else {
			result.setAttribute("type", typeAttr);
		}

    	return result;
	}

	/**
	 * Adds a parameter to the current Outcome object
	 * @param parameterName
	 * @param parameterValue
	 * @return
	 */
	public Outcome addParameter(String parameterName, String parameterValue) {
		parametersMap.put(parameterName, parameterValue);
		return this;
	}
	
	/**
	 * Adds an output to the current Outcome object
	 * @param ele
	 * @return
	 */
	public Outcome addOutput(Element ele) {
		outputs.add(ele);
		return this;
	}
	
	/**
	 * Set the name of the output tag
	 * @param outputTag
	 * @return
	 */
	public Outcome setOutputTag(String outputTag) {
		this.outputTag = outputTag;
		return this;
	}
	
	/**
	 * Set the ID of the output. Used for part 2
	 * @param outputTag
	 * @return
	 */
	public Outcome setID(int id) {
		this.id = id;
		return this;
	}

	/**
	 * Determines if the outcome was successful
	 * @return
	 */
	public boolean isSuccessful() {
		return outcomeTag.equals(Variables.SUCCESS);
	}

	/**
	 * Adds an output to the current Outcome object but 
	 * includes the order for PMQuad 
	 * @param firstChild
	 * @param pmOrder
	 * @return
	 */
	public Outcome addOutput(Element firstChild, int pmOrder) {
		addOutput(firstChild);
		this.pmOrder = pmOrder;
		
		return this;
	}
	/**
	 * Sets the typeAttr field.
	 * @param typeAttr
	 * @return
	 */
	public Outcome settypeAttr(String typeAttr) {
		this.typeAttr = typeAttr;
		return this;
	}

	/**
	 * Gets the error attribute of the Outcome, if any 
	 * @return
	 */
	public String gettypeAttr() {
		return typeAttr;
	}
}