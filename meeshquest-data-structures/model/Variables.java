package model;

import java.util.Comparator;

import model.mappart.cities.City;

public class Variables {
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
	public static final String FATAL_ERROR = "fatalError";
	public static final String UNDEFINED_ERROR = "undefinedError";
	public static final Outcome FATAL_OUTCOME = new Outcome(FATAL_ERROR, "", "");
	public static final Outcome UNDEFINED_OUTCOME = new Outcome(UNDEFINED_ERROR, "", "");
	
	private static final Comparator<City> Y_COMP =
			(City c1, City c2) -> Double.compare(c1.y, c2.y);
	private static final Comparator<City> X_COMP =
			(City c1, City c2) -> Double.compare(c1.x, c2.x);
	private static final Comparator<City> REMOTE_Y_COMP =
			(City c1, City c2) -> Double.compare(c1.getRemoteY(), c2.getRemoteY());
	private static final Comparator<City> REMOTE_X_COMP =
			(City c1, City c2) -> Double.compare(c1.getRemoteX(), c2.getRemoteX());
	public static final Comparator<City> COORDINATE_COMP =
			(City c1, City c2) -> REMOTE_Y_COMP.thenComparing(REMOTE_X_COMP)
									.thenComparing(Y_COMP)
									.thenComparing(X_COMP)
									.compare(c1, c2);
	public static final Comparator<City> CITY_NAME_COMP = 
			(City c1, City c2) ->  c2.getName().compareTo(c1.getName());
	
	
	public static void tester(boolean equality, String str) {
		if(equality)
			System.err.println(str);
	}
}
