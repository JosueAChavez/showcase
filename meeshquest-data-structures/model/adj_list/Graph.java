package model.adj_list;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
/**
 * Graph.java
 * @author Josue A. Chavez
 * Directory ID: jachavez
 * University ID: 114538727
 * Section: 0201
 * This class represents a Graph object. A graph contains vertex and weighted 
 * edges. This representation of a Graph is directed, meaning that each edge
 * points to a vertex in a single direction. The edge are weighted based off 
 * primitive Double values. Vertex and Edge Objects are used to represent 
 * vertex and edges, respectively.
 */
public class Graph<V> {

	private TreeMap<V, TreeMap<V, Double>> vertices;
	private Comparator<V> comparator;

	/**
	 * @param comparator
	 *            initializes a Graph object. This is done by instantiating a
	 *            new Graph object.
	 */
	public Graph(Comparator<V> comparator) {
		if (comparator == null)
			throw new NullPointerException();
		this.comparator = comparator;
		vertices = new TreeMap<V, TreeMap<V, Double>>(comparator);
	}

	/**
	 * @param otherGraph
	 *            A constructor that performs a shallow copy of the passed Graph
	 *            object. The copy created through this constructor should make
	 *            the passed Graph object independent of the instantiated Graph
	 *            object.
	 */
	public Graph(Graph<V> otherGraph) {
		vertices = new TreeMap<V, TreeMap<V, Double>>();
		comparator = otherGraph.comparator;
		// get all the source vertices
		for (V source : otherGraph.vertices.keySet()) {
			addVertex(source);
			// get the destinations inside the vertices
			for (V dest : otherGraph.vertices.get(source).keySet())
				addEdgeBetweenVertices(source, dest,
						otherGraph.getEdgeCost(source, dest));
		}
	}

	/**
	 * @param vertex
	 * @return tries to add a new vertex that does not exist on the current
	 *         rooster of vertices. returns true indicating that the adding
	 *         operation was successful and false if the vertex already exist.
	 */
	public boolean addVertex(V vertex) {
		// checks if the vertex is already on the list
		if (!(isVertex(vertex))) {
			vertices.put(vertex, new TreeMap<V, Double>());
			return true;
		}
		return false;
	}

	/**
	 * @param vertex
	 * @return returns a boolean which represent if the passed vertex Object
	 *         exists on the graph.
	 */
	public boolean isVertex(V vertex) {
		nullCheck(vertex);
		/*return vertices.containsKey(vertex);*/
		for (V aVertex : vertices.keySet())
			if (aVertex.equals(vertex))
				return true;

		return false;
	}

	/**
	 * @return Takes all of the vertices and places them all in an object that
	 *         implements the Collection interface. In the case that there is no
	 *         vertices in the current Graph Object, an empty collection is
	 *         returned.
	 */
	public Collection<V> getVertices() {
		Collection<V> col = new ArrayList<V>();

		for (V aVertex : vertices.keySet())
			col.add(aVertex);
		return col;
	}

	/**
	 * @param vertex
	 * @return attempts to remove the passed vertex in the current Graph Object.
	 *         In the case that such vertex does not exist, no operations will
	 *         be performed and the method will return false. Otherwise, the
	 *         removal operation is done and true is return indicating that the
	 *         operation was sucessful.
	 */
	public boolean removeVertex(V vertex) {
		if (isVertex(vertex)) {
			vertices.remove(vertex);

			// remove it's neighbors as well
			for (V possibleNeighbor : vertices.keySet())
				removeEdgeBetweenVertices(possibleNeighbor, vertex);
			// change change it to where if remove is null, then return false
			return true;
		}
		return false;

	}

	/**
	 * @param source
	 * @param dest
	 * @param cost
	 * @return creates a edge between the vertex source and vertex dest with the
	 *         weight cost. If source or dest do not exist on the current Graph
	 *         Object, they will be added. The method will return false in the
	 *         event that the weight is negative. Otherwise, true is returned.
	 */
	public boolean addEdgeBetweenVertices(V source, V dest, double cost) {
		nullCheck(dest);
		nullCheck(source);

		// check that weight is valid
		if (cost < 0)
			return false;
		else {
			// check to see if source or dest exist
			if (!isVertex(source))
				addVertex(source);
			if (!isVertex(dest))
				addVertex(dest);

			vertices.get(source).put(dest, cost);

			return true;
		}
	}

	/**
	 * @param source
	 * @param dest
	 * @return returns the weight of a edge that comes starts from the vertex
	 *         source to the vertex dest. In the case that either source or dest
	 *         are not located in the current Graph object, -1 is returned. -1
	 *         is also returned when there is no edge between the vertices
	 *         source and dest.
	 */
	public double getEdgeCost(V source, V dest) {
		Double result = null;
		// access each vertex step by step
		TreeMap<V, Double> destExist = vertices.get(source);

		if (destExist != null)
			result = destExist.get(dest);

		return result == null ? -1 : result;
	}

	/**
	 * @param source
	 * @param dest
	 * @return Removes an edge that is between the vertices source and dest. in
	 *         the case where either source or dest do not exist, no actions are
	 *         performed and false is returned. False is also returned when
	 *         there is no edge between the source dest. True is returned when
	 *         none of the cases that return false is false, which means that
	 *         they are true.
	 */
	public boolean removeEdgeBetweenVertices(V source, V dest) {
		if (getEdgeCost(source, dest) < 0)
			return false;
		vertices.get(source).remove(dest);
		return true;

	}

	/**
	 * @param vertex
	 * @return gets all of the neighbors in a given vertex and stores and
	 *         returns them using the Collection Interface. In the case that
	 *         there is no vertices in the current Graph Object, an empty
	 *         collection is returned.
	 */
	public Collection<V> getNeighbors(V vertex) {
		
		try {
			// make a collection that could have neighbors or none (null)
			return vertices.get(vertex).keySet();
		} catch(NullPointerException e) {
			return new ArrayList<V>();
		}
		/*
		
		Collection<V> result = null;
		// make a collection that could have neighbors or none (null)
		TreeMap<V, Double> destExist = vertices.get(vertex);
		if (destExist != null)
			result = destExist.keySet();

		// if its null, return an "empty" list, otherwise return the neighbors
		return result == null ? new ArrayList<V>() : result;*/
	}

	/**
	 * @param vertex
	 * @return gets all of the predecessors in a given vertex and stores and
	 *         returns them using the Collection Interface. In the case that
	 *         there is no vertices in the current Graph Object, an empty
	 *         collection is returned.
	 */
	public Collection<V> getPredecessors(V vertex) {
		// find out whose pointing to me
		// go through each neighbor and see if they contain the vertex
		ArrayList<V> result = new ArrayList<V>();
		for (V possiblePredecessor : vertices.keySet()) {
			if (vertices.get(possiblePredecessor).get(vertex) != null)
				result.add(possiblePredecessor);
		}
		return result;
	}

	/**
	 * @param vertex1
	 * @param vertex2
	 * @return merges two vertices. Will not merge in the following cases:<br>
	 *         - either vertices are not in the graph <br>
	 *         - there is no edge between the vertices <br>
	 *         - both vertices passed are the same vertex<br>
	 *         When merging occurs, the following cases are considered:<br>
	 *         - vertex with the smallest data is the vertex that takes over<br>
	 *         - the edge with the largest weight takes over if there are two
	 *         edges that it's predecessors<br>
	 *         - the edge with the largest weight takes over if there are two
	 *         edges that it's neighbors<br>
	 *         - edges pointing to the passed vertexes are discarded<br>
	 *         - the largest self edge will be selected if the vertices both
	 *         have self point edges <br>
	 */
	public boolean contractEdgeBetweenVertices(V vertex1, V vertex2) {

		// check if vertices are on the graph, also does null check
		if (isVertex(vertex1) && isVertex(vertex2)) {
			// determine which vertex will be the main vertex
			V smallestData = comparator.compare(vertex1, vertex2)
					> 0 ? vertex2 : vertex1;
			if (getEdgeCost(vertex1, vertex2) > -1 ||
					getEdgeCost(vertex2, vertex1) > -1) {
				
				// get list of predecessors and neighbors with the largest edges
				TreeMap<V, Double> newNeighbors = new TreeMap<V, Double>(),
						newPredessors = new TreeMap<V, Double>();
				
				// see if both vertices point to themselves
				Double pointsToSelf1 = getEdgeCost(vertex1, vertex1),
						pointsToSelf2 = getEdgeCost(vertex2, vertex2);

				// get vertex 1 neighbors and predecessors
				insertNeighbors(vertex1, vertex2, newNeighbors);
				insertPredecessors(vertex1, vertex2, newPredessors);

				// get vertex 2 neighbors and predecessors
				insertNeighbors(vertex2, vertex1, newNeighbors);
				insertPredecessors(vertex2, vertex1, newPredessors);
				

				// determine which self pointing edge should stay, if any
				if(pointsToSelf1 != -1 || pointsToSelf2 != -1){
					newNeighbors.put(smallestData, pointsToSelf1 >
					pointsToSelf2? pointsToSelf1: pointsToSelf2);
					newPredessors.put(smallestData, pointsToSelf1 > 
					pointsToSelf2? pointsToSelf1: pointsToSelf2);
				}
				
				// delete the vertex, which also removes the edges
				removeVertex(vertex1);
				removeVertex(vertex2);
				
				// add in the new vertex and their neighbors and predecessors
				addVertex(smallestData);
				
				for(V addNeighbor: newNeighbors.keySet())
					addEdgeBetweenVertices(smallestData, addNeighbor, 
							newNeighbors.get(addNeighbor));
				for(V addPred: newPredessors.keySet())
					addEdgeBetweenVertices(addPred, smallestData, 
							newPredessors.get(addPred));

				return true;
			}
		}

		return false;
	}

	/**
	 * @param v1
	 * @param v2
	 * @return Checks to see if the vertices are bidirectional, returns a
	 *			 boolean indicating if there is an edge between v1 and v2.
	 */
	public boolean containsBiEdge(V v1, V v2) {
		if(v1 == null || v2 == null)
			return false;
		return getEdgeCost(v1, v2) > 0 && getEdgeCost(v2, v1) > 0;  
	}
	
	/**
	 * @param v1
	 * @param v2
	 * @param weight
	 * @return Adds a bidirectional edge. Returns true if successful and 
	 * 			false in the end that there is already an edge
	 */
	public boolean addBiEdge(V v1, V v2, Double weight) {
		if(containsBiEdge(v1, v2)) {
			return false;
		} else {
			addEdgeBetweenVertices(v1, v2, weight);
			addEdgeBetweenVertices(v2, v1, weight);
			return true;
		}
	}
	
	/**
	 * @param v1
	 * @param v2
	 * @return Removes a bidirectional edge. Returns true if successful and 
	 * 			false in the end that there is already an edge
	 */
	public boolean removeBiEdge(V v1, V v2) {
		if(!containsBiEdge(v1, v2)) {
			return false;
		} else {
			removeEdgeBetweenVertices(v1, v2);
			removeEdgeBetweenVertices(v2, v1);
			return true;
		}
	}
	
	public Map<V, V> shortestPath(V start, V end) {
		Set<V> processed = new HashSet<V>();
		Map <V, V> predecessors = new HashMap<V, V>();
		Map<V, Double> minCosts = new HashMap<V, Double>();
		
		minCosts.put(start, 0.0);
		
		// go until you reached end
		while(!processed.contains(end)) {
			// find the vertex Min that's not in Processed 
			// that has the smallest value of MinCosts[Min]
			V min = null;
			Double smallestCost = Double.MAX_VALUE;
			for (V vertex : minCosts.keySet()) {
				Double cost = minCosts.get(vertex);
				if(!processed.contains(vertex)) {
					int compDist = cost.compareTo(smallestCost);
					if(compDist < 0 || (compDist == 0 &&
							comparator.compare(min, vertex) > 0)) {
						smallestCost = cost;
						min = vertex;
					}
				}
			}
			// means has list neighbors
			if(min == null)
				return null;

			processed.add(min);
			// for each neighbor N of Min that's not in Processed
			for(V neigh : getNeighbors(min)) {
				if(!processed.contains(neigh)) {
					
					if(!minCosts.containsKey(neigh))
						minCosts.put(neigh, Double.MAX_VALUE);
					
					Double len = getEdgeCost(min, neigh);
					if(len.compareTo(0.0) < 0)
						len = Double.MAX_VALUE;
					else
						len += minCosts.get(min);
					
					int compDist = minCosts.get(neigh).compareTo(len);					
					if((compDist > 0)) {
						minCosts.put(neigh, len);
						predecessors.put(neigh, min);	
					}
				}
			}
		}
				
		return predecessors;
	}
	
	// helper methods
	public String toString() {
		// return vertices.toString();
		StringBuffer result = new StringBuffer();
		for (V aVertex : vertices.keySet()) {
			result.append("\n--Vertex: " + aVertex + "--\nNeighbors: \n");
			for (V neighbor : getNeighbors(aVertex))
				result.append("- " + neighbor + ", " + 
			getEdgeCost(aVertex, neighbor) + "\n");
			result.append("\nPredecessors: \n");
			for (V pred : getPredecessors(aVertex))
				result.append("* " + pred + ", " +
			getEdgeCost(pred, aVertex) + "\n");
		}
		return result.toString();
	}

	private void nullCheck(V vertex) {
		if (vertex == null)
			throw new NullPointerException();
	}
	
	private void insertNeighbors(V vertex1, V vertex2, TreeMap<V, Double> map){
		for (V secondNeighbor : getNeighbors(vertex1)){
			if (!(comparator.compare(secondNeighbor, vertex2) == 0) &&
					!(comparator.compare(secondNeighbor, vertex1) == 0)) {
				Double addend = getEdgeCost(vertex1, secondNeighbor);
				Double augend = map.get(secondNeighbor);
				// check if there is an edge pointing and compare if so
				if (augend != null){
					if(addend.compareTo(augend) > 0) 
						map.put(secondNeighbor, addend);
				} else {
					map.put(secondNeighbor,
							getEdgeCost(vertex1, secondNeighbor));
				}
			}
		}
	}
	private void insertPredecessors(V vertex1, V vertex2, TreeMap<V, 
			Double> map){
		for (V predessor : getPredecessors(vertex1)){
			if (!(comparator.compare(predessor, vertex2) == 0) &&
					!(comparator.compare(predessor, vertex1) == 0)) {
				Double addend = getEdgeCost(predessor, vertex1);
				Double augend = map.get(predessor);
				// check if there is an edge pointing and compare if so
				if (augend != null){
					if(addend.compareTo(augend) > 0) 
						map.put(predessor, addend);
				} else {
					map.put(predessor, getEdgeCost(predessor, vertex1));
				}
			}
		}
	}
}