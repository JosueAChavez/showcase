package model.adj_list;
/**
 * Vertex.java
 * @author Josue A. Chavez
 * Directory ID: jachavez
 * University ID: 114538727
 * Section: 0201
 * This class represents a Vertex object. A vertex contains a list of it 
 * neighbors with an edge weighted value (an integer). 
 */


//public class Vertex<V> implements Comparable<Vertex>{
public class Vertex<V> {
	private V value;
//	private HashMap<V, Integer> neighbors, sucessors;

	public Vertex(V value) {
		this.value = value;
//		neighbors = new HashMap<V, Integer>();
//		sucessors = new HashMap<V, Integer>();
	}
//
//	public void addEdge(V neighbor, int weight){
//		neighbors.put(neighbor, weight);
//	}
	
//	public Collection<V> getNeighbors(){
//		return neighbors.keySet();
//	}
//	
//	public Collection<V> getSucessors(){
//		return sucessors.keySet();
//	}
	
	public V getValue(){
		return value;
	}
	
	public String toString(){
//		return "value: " + value + " \nneighbors: " + neighbors + " \nsucessors" + sucessors;
		return "value: " + value;
	}

	// public int compareTo(Vertex arg0) {
	// return 0;
	// }

}
