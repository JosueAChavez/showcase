package model.dictionary;

import java.util.Comparator;
import java.util.TreeMap;

import model.dictionary.dt.BPTree;

public class GuardedDodekaTrie<K, V> extends TreeMap<K, V> {

	private static final long serialVersionUID = 1L;
	private BPTree<K, V> dt;
	
	public GuardedDodekaTrie(int leafOrder, Comparator<K> c){
		super(c);
		dt = new BPTree<K,V>(leafOrder, c);
	}

	public void clear() {
		dt.clear();
		super.clear();
	}

	public boolean isEmpty() {
		dt.isEmpty();
		return super.isEmpty();
	}

	public boolean containsKey(Object key) {
		dt.containsKey(key);
		return super.containsKey(key);
	}

	public V put(K key, V value) {
		dt.put(key, value);
		return super.put(key, value);
	}

	public V get(Object key) {
		dt.get(key);
		return super.get(key);
	}

	public V remove(Object key) {
		dt.remove(key);
		return super.remove(key);
		
	}

	public BPTree<K, V> getDT() {
		return dt;
	}
}