package model.dictionary;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import controller.comparators.StringComparator;
import model.Outcome;
import model.Variables;
import model.dictionary.dt.Tuple;
import model.dictionary.dt.node.BPInternal;
import model.dictionary.dt.node.BPLeaf;
import model.dictionary.dt.node.BPNode;
import model.mappart.cities.City;

public class Dictionary {	
	private GuardedDodekaTrie<String, City> stringToCity;
	private GuardedDodekaTrie<City, String> cityToString;
	private int leafOrder;

	public Dictionary() {
		this(12);
	}
	
	public Dictionary(int leafOrder) {
		stringToCity = 
				new GuardedDodekaTrie<String, City>(leafOrder, new StringComparator());
		cityToString = new GuardedDodekaTrie<City, String>(leafOrder, Variables.COORDINATE_COMP);
		this.leafOrder = leafOrder;
	}
	
	public Outcome clear() {
		this.stringToCity.clear();
		this.cityToString.clear();
		
		return new Outcome(Variables.SUCCESS, "clearAll");
	}
	
	public Outcome getCities(String attribute, Document results) {
		List<City> cities = new ArrayList<City>(stringToCity.values());
		Comparator<City> comparator = null;
		Outcome outcome;

    	
    	if(stringToCity.isEmpty()) {
    		outcome = new Outcome(Variables.ERROR, "listCities", "noCitiesToList");
    		outcome.addParameter("sortBy", attribute);
    		return outcome;
    	}
		
		// sort cities depending on what attribute is
		switch (attribute) {
			case "name":
				comparator = Variables.CITY_NAME_COMP;
				break;
			case "coordinate":
				comparator = Variables.COORDINATE_COMP;
				break;
		}
		
		outcome = new Outcome(Variables.SUCCESS, "listCities");
		
		Collections.sort(cities, comparator);
		for(City city: cities) {
			outcome = outcome.addOutput(city.getXML(results, "city"));
		}
		
		outcome.setOutputTag("cityList");
		outcome.addParameter("sortBy", attribute);
		return outcome;
	}
	
	public Outcome put(City city) {
    	Outcome result = null;
    	String name = city.getName();
    	
    	// add parameters
    	Map<String, String> parameters = city.getParameters();
    	    	
    	if(cityToString.containsKey(city)) {
    		result = new Outcome(Variables.ERROR, "createCity", "duplicateCityCoordinates");
    	} else if(stringToCity.containsKey(name)) {
    		result = new Outcome(Variables.ERROR, "createCity", "duplicateCityName");
    	} else {
    		stringToCity.put(name, city);
    		cityToString.put(city, name);
    		
    		result = new Outcome(Variables.SUCCESS, "createCity");
    	}
    	
    	for (String parameter : parameters.keySet()) {
    		result.addParameter(parameter, parameters.get(parameter));
    	}
    	
    	return result;
	}
	
	public Outcome remove(String name) {
    	Outcome result = null;
    	
    	if(!stringToCity.containsKey(name)) {
    		result = new Outcome(Variables.ERROR, "deleteCity", "cityDoesNotExist");
    	} else {
    		City temp = stringToCity.get(name);
    		stringToCity.remove(name);
    		cityToString.remove(temp);
    		
    		result = new Outcome(Variables.SUCCESS, "deleteCity");
    	}
    	
    	// add parameter
    	result.addParameter("name", name);
    	
    	return result;
	}
	
	public City get(String name) {
		return stringToCity.get(name);
	}
	
	/**
	 * See if a city's name is in the dictionary 
	 * @param name
	 * @return
	 */
	public boolean has(String name) {
		return stringToCity.containsKey(name);
	}
	
	/**
	 * Check if a city's metropole AND local coordinate already exist in the dictionary 
	 * @param city
	 * @return
	 */
	public boolean hasCoordinate(City city) {
		return cityToString.containsKey(city);
	}

	// TODO not sure if this is the right place
	public Outcome printDodekaTrie(Document doc) {
		
		if(stringToCity.isEmpty())
			return new Outcome(Variables.ERROR, "printDodekaTrie", "emptyTree");
		
		Element result = doc.createElement("DodekaTrie");
		result.setAttribute("leafOrder", Integer.toString(leafOrder));
		result.setAttribute("cardinality", 
				Integer.toString(stringToCity.getDT().size()));
		BPNode<String, City> root = stringToCity.getDT().getRoot();
		
		if(root instanceof BPLeaf)
			result.appendChild(printDTLeaf((BPLeaf<String, City>) root, doc));
		else
			result.appendChild(printDTInternal((BPInternal<String, City>) root, doc));
		
		return new Outcome(Variables.SUCCESS, "printDodekaTrie").addOutput(result); 
	}
	
	/**
	 * Prints what a DT leaf contains in XML
	 * @param leafNode
	 * @param doc
	 * @return
	 */
	private Element printDTLeaf(BPLeaf<String, City> leafNode, Document doc) {
		
		Element leaf = doc.createElement("leaf");
		
		for(Tuple<String, City> tuple :  leafNode.getkeyVals()) {
			Element entry = doc.createElement("entry");
			City city = tuple.getValue();
			entry.setAttribute("key", tuple.getKey());
			
			entry.setAttribute("value", 
					"(" + (int) city.x + "," + (int) city.y + ")");
			leaf.appendChild(entry);
		}
		
		return leaf;
	}
	
	private Element printDTInternal(BPInternal<String, City> internalNode, Document doc) {
		Element guide = doc.createElement("guide");
		
		for(Tuple<String, BPNode<String, City>> tuple: internalNode.getKeyGuides()) {
			BPNode<String, City> node = tuple.getValue();  
			if(node instanceof BPInternal)
				guide.appendChild(printDTInternal((BPInternal<String, City>) node, doc));
			else if (node instanceof BPLeaf)
				guide.appendChild(printDTLeaf((BPLeaf<String, City>) node, doc));
			
			Element key = doc.createElement("key");
			key.setAttribute("value", tuple.getKey());
			guide.appendChild(key);
		}
		
		// handle far right case
		BPNode<String, City> node = internalNode.getRight();
		if(node instanceof BPInternal)
			guide.appendChild(printDTInternal((BPInternal<String, City>) node, doc));
		else if (node instanceof BPLeaf)
			guide.appendChild(printDTLeaf((BPLeaf<String, City>) node, doc));
		
		return guide;
	}
}