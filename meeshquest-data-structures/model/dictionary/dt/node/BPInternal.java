package model.dictionary.dt.node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import controller.exceptions.InternalUnderflowException;
import controller.exceptions.LeafUnderflowException;
import model.dictionary.dt.Tuple;

public class BPInternal<K, V> implements BPNode<K, V> {
	
	private List<Tuple<K, BPNode<K, V>>> keyGuides;
	private BPNode<K, V> right;
	private int numAry;
	private Comparator<Tuple<K, ?>> tupleComp;

	private BPInternal(int numAry, Comparator<Tuple<K, ?>> tupleComp) {
		this.numAry = numAry;
		this.tupleComp = tupleComp;
		keyGuides = new ArrayList<Tuple<K, BPNode<K, V>>>();
	}
	
	/**
	 * Used for splitting leaves
	 * @param numAry
	 * @param comp
	 * @param tupleComp
	 * @param toLeft - the full BPLeaf that is going to be split
	 */
	public BPInternal(int numAry, Comparator<Tuple<K, ?>> tupleComp, 
			BPLeaf<K, V> toLeft) {
		
		this(numAry, tupleComp);

		List<Tuple<K, V>> keyVals = toLeft.getkeyVals();
		int len = keyVals.size();
		
		// get the middle element for promotion
		K middle = keyVals.get(len/2).getKey();

		// divide list into two
		BPLeaf<K, V> right = new BPLeaf<>(len - 1, tupleComp);
				   
		right.setNext(toLeft.getNext());
		
		for(int i = len/2; i < len; i++)
			right.add(keyVals.get(i).getKey(), keyVals.get(i).getValue());
		
		// use leaf that needed to be split as left 
		for(int i = len-1; i >= len/2; i--)
			keyVals.remove(keyVals.get(i));
			
		// need to do this to ensure that the prev of toLeft points to same left
		toLeft.setNext(right); // set left's next element
		right.setPrev(toLeft); // make right prev point to left
		
		// see if right's next is a Leaf and point it's prev to right if so
		if(right.getNext() != null)
			right.getNext().setPrev(right);
				
		insert(new Tuple<K, BPNode<K,V>>(middle, toLeft));
		this.right = right;
	}

	/**
	 * Used for splitting internal nodes
	 * @param numAry
	 * @param toSplit
	 * @param comp
	 * @param tupleComp
	 */
	private BPInternal(int numAry, List<Tuple<K, BPNode<K, V>>> toSplit, BPNode<K, V>
		toSplitRight, Comparator<Tuple<K, ?>> tupleComp) {
		this(numAry, tupleComp);
		
		List<Tuple<K, BPNode<K, V>>> list = toSplit;
		int len = list.size();

		// get the middle element for promotion
		K middle = list.get(len/2).getKey();
		
		// divide list into two		
		BPInternal<K,V> left = new BPInternal<>(numAry, tupleComp);   
		BPInternal<K,V> right = new BPInternal<>(numAry, tupleComp);
		
		for(int i = 0; i < numAry/2; i++) 
			left.insert(toSplit.get(i));

		left.right = toSplit.get(len/2).getValue(); // set middle's value to right
		
		for(int i = numAry/2 + 1; i < numAry; i++)
			right.insert(toSplit.get(i));			
		
		right.right = toSplitRight;
		
		insert(new Tuple<K, BPNode<K,V>>(middle, left));
		this.right = right;
	}
	
	/**
	 * Merges two BPInternal nodes.
	 * @param temp
	 * @return
	 */
	public BPNode<K, V> add(BPInternal<K, V> temp) {
		BPNode<K, V> newRight = temp.right;
		
		// merge data in temp to current node
		Tuple<K,BPNode<K, V>> key = merge(temp);
		int loc = Collections.binarySearch(keyGuides, key, tupleComp) + 1;
		setPointer(loc, newRight);
		
		
		// Do check to see if splitting needs to happen at internal level
		if(keyGuides.size() == numAry) {
			return new BPInternal<>(numAry, keyGuides, right, tupleComp);
		}
		
		return this;
	}

	public BPNode<K, V> pointers(int currec) {
		if(currec == keyGuides.size())
			return right;
		
		return keyGuides.get(currec).getValue();
	}
	
	/**
	 * sets a pointer given a value
	 * @param currec
	 * @param value
	 */
	private void setPointer(int currec, BPNode<K, V> value) {
		if(currec == keyGuides.size())
			right = value;
		else
			keyGuides.get(currec).setValue(value);
	}

	public int binarySearch(K key) {
		return Collections.binarySearch(keyGuides, new 
				Tuple<K, BPNode<K, V>>(key, null), tupleComp);
	}

	public List<Tuple<K, BPNode<K, V>>> getKeyGuides() {
		return keyGuides;
	}
	
	public String toString() {
		String res = "| ";
		for(Tuple<K, ?> keys: keyGuides)
			res += keys.getKey() + " | ";
		
		return res;
	}

	public BPNode<K, V> getRight() {
		return right;
	}
	
	/** merge data in toMerge to current Internal Node */
	private Tuple<K,BPNode<K, V>> merge(BPInternal<K, V> toMerge) {
		Tuple<K,BPNode<K, V>> res = null;
		for(Tuple<K,BPNode<K, V>> tuple: toMerge.keyGuides) {
			insert(tuple);
			res = tuple;
		}
		return res;
	}

	public int size() {
		return keyGuides.size();
	}
	
	/**
	 * Updates keyGuides around tuple. Assumes that tuple is already in keyGuides.
	 * @param tuple
	 * @param reKey
	 */
	private void updateKeys(Tuple<K, BPNode<K,V>> tuple) {		
		
		int loc = Collections.binarySearch(keyGuides, tuple, tupleComp), toUpdate;
		boolean updatePrev = loc != 0;
		K newKey;

		// update left
		if(updatePrev) {
			toUpdate = loc - 1;
			newKey = pointers(loc).getFirstKey();
			keyGuides.set(toUpdate, new Tuple<K, BPNode<K,V>>(newKey, pointers(toUpdate)));
		}
		
		// update current
		toUpdate = loc;
		newKey = pointers(loc + 1).getFirstKey();
		keyGuides.set(toUpdate, new Tuple<K, BPNode<K,V>>(newKey, pointers(toUpdate)));
	}
	
	public void insert(Tuple<K, BPNode<K,V>> tuple) {		
		int possLoc = Collections.binarySearch(keyGuides, tuple, tupleComp);
		
		if(possLoc < 0) {			
			keyGuides.add(tuple);
			keyGuides.sort(tupleComp);
		} else 
			keyGuides.set(possLoc, tuple);
	}

	public V remove(K key) {
		int loc = binarySearch(key);
		V res = null;
		boolean getFirst = false;
		
		if(loc < 0)
			loc = (loc + 1) * -1;
		else {
			getFirst = (keyCompare(key, keyGuides.get(loc).getKey()) == 0);
			loc++; // go the right
		}

		BPNode<K, V> node = null;
		boolean redistrib = false,
				grabLeft = true,
				rightMost = loc == keyGuides.size();
		// determine which where key is at
		if(rightMost) 					// right
			node = right;
		else							// normal or end
			node = keyGuides.get(loc).getValue();
		try {
			res = node.remove(key);
		} catch(LeafUnderflowException e) {
						
			res = (V) e.getVal();
			BPLeaf<K, V> leaf = (BPLeaf<K, V>) node;
			// attempt to transfer tuple from adjacent(s) sibling 
			if (loc != 0) {	
				redistrib = leaf.grabSiblingNode(leaf.getPrev(), false);
				grabLeft = redistrib;
			}
			if(!redistrib && !rightMost) {
				redistrib = leaf.grabSiblingNode(leaf.getNext(), true);
			}
			
			if(!redistrib) { // means that sibling(s) can't spare -> merge
				
				BPLeaf<K, V> dest, toDelete;
				
				if(rightMost) {					// right - put prev's data into right
					toDelete = leaf.getPrev();
					dest = leaf;
										
					if(toDelete.getPrev() != null)
						toDelete.getPrev().setNext(leaf);
					leaf.setPrev(toDelete.getPrev());

					keyGuides.remove(loc - 1);
				} else {						// put current data into next
					toDelete = leaf;
					dest = leaf.getNext();

					if(leaf.getPrev() != null)
						leaf.getPrev().setNext(leaf.getNext());
					leaf.getNext().setPrev(leaf.getPrev());
					keyGuides.remove(loc);
				}
				if(loc > 0) // not beginning of list
					loc--;

				// transfer data into destination leaf
				for(Tuple<K, V> tuple: toDelete.getkeyVals())
					dest.add(tuple.getKey(), tuple.getValue());
				toDelete.getkeyVals().clear();
		
				// lose a internal node if it only has one child (right)
				if(keyGuides.isEmpty())
					throw new InternalUnderflowException("leaf underflow", right, e.getVal());
			}

			int toUpdate = -1;
			K newKey = null;
			// update key guide 
			if(rightMost) { // right
				newKey = right.getFirstKey(); // TODO use pointers(loc)
				toUpdate = loc - 1; // go to the end of list
			} else {
				if(grabLeft && loc > 0) {
					newKey = pointers(loc).getFirstKey();
					toUpdate = loc - 1;
				} else {
					newKey = pointers(loc + 1).getFirstKey();
					toUpdate = loc;
				}
			}
			
			if(newKey != null) {
				/*System.err.println("updating " + keyGuides.get(toUpdate).getKey() + " to " + newKey + "\n");*/
				keyGuides.set(toUpdate, new Tuple<K, BPNode<K,V>>(newKey, pointers(toUpdate)));
			}
		} catch (InternalUnderflowException e) {
			res = (V) e.getVal();
			BPNode<K,V> orphan = (BPNode<K, V>) e.getNode();
			
			// let's make life easier
			List<BPInternal<K, V>> guides = new ArrayList<>();
			for(Tuple<K, BPNode<K, V>> tup : keyGuides)
				guides.add((BPInternal<K, V>) tup.getValue());
			
			guides.add((BPInternal<K, V>) right);
			
			// merge onto another BPleaf
			Tuple<K, BPNode<K,V>> newTuple;
			if(rightMost) {
				
					
				newTuple = new Tuple<K, BPNode<K,V>>(orphan.getFirstKey(), guides.get(loc - 1).right);
				guides.get(loc - 1).right = orphan;
				guides.get(loc - 1).insert(newTuple);
				
				if(orphan instanceof BPLeaf)
					guides.get(loc - 1).updateKeys(newTuple);
				right = guides.get(loc - 1);
				keyGuides.remove(loc - 1); // remove end of list
			} else {
				newTuple = new Tuple<K, BPNode<K,V>>(keyGuides.get(loc).getKey(), orphan); 
				guides.get(loc + 1).insert(newTuple);
				if(orphan instanceof BPLeaf)
					guides.get(loc + 1).updateKeys(newTuple);
				keyGuides.remove(loc);
			}
			
			if(keyGuides.isEmpty())
				throw new InternalUnderflowException("internal underflow", right, e.getVal());
		}
		
		// need to change key guide if the key removed were the same
		loc = binarySearch(key);
		
		if(loc < 0)
			loc = (loc + 1) * -1;

		// update the guide 
		K newKey;
		
		if(getFirst && loc != keyGuides.size()) {
			newKey = pointers(loc + 1).getFirstKey();
			keyGuides.set(loc, new Tuple<K, BPNode<K,V>>(newKey, pointers(loc)));
		}
						
		return res;
	}
	
	public K getFirstKey() {
		return keyGuides.get(0).getValue().getFirstKey();
	}
	
	private int keyCompare(K key1, K key2) {
		return tupleComp.compare(new Tuple<K, BPNode<K, V>>(key1, null), 
				new Tuple<K, BPNode<K, V>>(key2, null));
	}
}