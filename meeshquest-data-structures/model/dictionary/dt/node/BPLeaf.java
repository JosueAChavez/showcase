package model.dictionary.dt.node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import controller.exceptions.LeafUnderflowException;
import model.Variables;
import model.dictionary.dt.Tuple;

public class BPLeaf<K, V> implements BPNode<K, V> {
		
	private List<Tuple<K, V>> keyVals;
	private int leafOrder;
	private Comparator<Tuple<K, ?>> tupleComp;
	private BPLeaf<K, V> next, prev;

	public BPLeaf(int leafOrder, Comparator<Tuple<K, ?>> tupleComp) {
		keyVals = new ArrayList<Tuple<K, V>>();
		this.tupleComp = tupleComp;
		this.leafOrder = leafOrder;
		next = null;
	}
	
	public BPNode<K, V> add(K k, V e) {		
		
		Tuple<K, V> tuple = new Tuple<>(k, e);
		
		int possLoc = Collections.binarySearch(keyVals, tuple, tupleComp);
		// check if its a repeat and change value
		if(possLoc > -1) {
			keyVals.get(possLoc).setValue(e);
		} else {
			insert(tuple);

			// perform splitting make the new parent
			if(keyVals.size() > leafOrder)
				return new BPInternal<>(12, tupleComp, this);	
		}
		return this;
	}
	
	public void insert(Tuple<K, V> tuple) {		
		int possLoc = Collections.binarySearch(keyVals, tuple, tupleComp);
		
		if(possLoc < 0) {			
			keyVals.add(tuple);
			keyVals.sort(tupleComp);
		} else 
			keyVals.set(possLoc, tuple);
	}
	
	public void remove(Tuple<K, V> tuple) {
		keyVals.remove(tuple); // not sure if have to sort again
		keyVals.sort(tupleComp);
	}

	public V remove(K key) {
		V res = null;
		int possLoc = binarySearch(key);

		if(possLoc > -1) {
			res = keyVals.get(possLoc).getValue();
			remove(keyVals.get(possLoc));
		}
		
		int half = (int) Math.ceil((double)leafOrder/2); 
		
		// underflow check
		if(keyVals.size() < half)	
			throw new LeafUnderflowException(res);
		
		/*if(res == null)
			throw new NullPointerException();*/
		
		return res;
	}
	
	/**
	 * sibling = prev -> getFirst = false <br>
	 * sibling = next -> getFirst = true <br>
	 * @param sibling
	 * @param getFirst
	 * @return
	 */
	public boolean grabSiblingNode(BPLeaf<K, V> sibling, boolean getFirst) {

		int half = (int) Math.ceil((double) leafOrder/2); 
		
		if(sibling != null && (sibling.size() - 1) >= half) {
			
			if(getFirst)
				insert(sibling.removeFirst());
			else
				insert(sibling.removeLast());
			
			return true;
		}
		
		return false;
	}

	public V recs(int currec) {
		return keyVals.get(currec).getValue();
	}

	public int binarySearch(K key) {
		return Collections.binarySearch(keyVals, new 
				Tuple<K, V>(key, null), tupleComp);
	}

	public List<Tuple<K, V>> getkeyVals() {
		return keyVals;
	}
	
	public String toString() {
		String res = "";
		for(Tuple<K, V> tuple: keyVals)
			res += "(" + tuple.getKey().toString() + ")";
		
		return res;
	}

	public void setNext(BPLeaf<K, V> next) {
		this.next = next;
	}
	
	public BPLeaf<K, V> getNext() {
		return next;
	}
	
	public void setPrev(BPLeaf<K, V> prev) {
		this.prev = prev;
	}
	
	public BPLeaf<K, V> getPrev() {
		return prev;
	}
	
	public K pointers(int currec) {
		return keyVals.get(currec).getKey();
	}

	public int size() {
		return keyVals.size();
	}
	

	
	/** Returns the first key of keyVals */
	public K getFirstKey() {
		Variables.tester(keyVals.isEmpty(), "");
		return keyVals.get(0).getKey();
	}
	
	/** Removes the first element of keyVals */
	private Tuple<K, V> removeFirst() {
		return keyVals.remove(0);
	}
	
	/** Removes the last element of keyVals */
	private Tuple<K, V> removeLast() {
		return keyVals.remove(keyVals.size() - 1);
	}
	
	
}