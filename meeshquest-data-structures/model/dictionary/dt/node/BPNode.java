package model.dictionary.dt.node;

public interface BPNode<K, V> {
	
	/**
	 * Returns the location of where the key is
	 * @param key
	 * @return index of where key is, negative index 
	 * 			if the location isn't in Node
	 */
	public int binarySearch(K key);
	public int size();
	public V remove(K key);
	public K getFirstKey();
}