package model.dictionary.dt;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.SortedMap;
import java.util.Stack;

import controller.exceptions.InternalUnderflowException;
import controller.exceptions.LeafUnderflowException;
import model.dictionary.dt.node.BPInternal;
import model.dictionary.dt.node.BPLeaf;
import model.dictionary.dt.node.BPNode;

public class BPTree<K, V> extends AbstractMap<K, V> implements SortedMap<K, V> {

	private Comparator<K> c;
	private int leafOrder, cardinality, modCount;
	private BPNode<K, V> root;
	private boolean naturalOrdering;

	public BPTree(int leafOrder) {
		// see if it implements comparable
		this(leafOrder, new Comparator<K>() {
			@SuppressWarnings("unchecked")
			public int compare(K o1, K o2) {
				return ((Comparable<? super K>)o1).compareTo((K)o2);
			}
		});
		naturalOrdering = false;
	}
	
	public BPTree(int leafOrder, Comparator<K> c) {
		if(leafOrder < 1) 
			throw new IllegalArgumentException("Leaf order must be greater than 0");

		this.c = c;
		this.leafOrder = leafOrder;
		root = new BPLeaf<K, V>(leafOrder, new TupleComparator());
		cardinality = 0;
		modCount = 0;
		naturalOrdering = true;
	}
	
	// insert AL
	public BPNode<K,V> insert(K k, V e) {
		root = inserthelp(root, k, e);
		return root;
	}
	
	private BPNode<K,V> inserthelp(BPNode<K,V> rt, K k, V e) {
		if (rt instanceof BPLeaf) // At leaf node: insert here			
			return ((BPLeaf<K,V>)rt).add(k, e);
		
		// Add to internal node
		int currec = rt.binarySearch(k);
		if(currec < 0)
			currec = (currec + 1) * -1;
		else
			currec++; // go the right
		
		BPNode<K,V> temp = inserthelp(((BPInternal<K,V>) rt).pointers(currec), k, e);
		// check for change and merge
		if (temp != ((BPInternal<K,V>)rt).pointers(currec))
			return ((BPInternal<K,V>)rt).add((BPInternal<K,V>)temp);
		else
			return rt;
	}

	private class TupleComparator implements Comparator<Tuple<K, ?>> {
		public int compare(Tuple<K, ?> o1, Tuple<K, ?> o2) {
			return c.compare((K) o1.getKey(), (K) o2.getKey());
		}
	}
	
	private void nullCheck(Object key) {
		if(key == null)
			throw new NullPointerException("Hey! No nulls allowed");
	}
	
	private void castCheck(Object key) {
		c.compare((K) key, (K) key);
	}
	
	// goes to far left of tree	
	private BPLeaf<K, V> getFirstLeafNode(BPNode<K, V> curr) {

		while(!(curr instanceof BPLeaf))
			curr = ((BPInternal<K,V>) curr).getKeyGuides().get(0).getValue();

		return ((BPLeaf<K, V>) curr);
	}
	
	// goes to far right of tree
	private BPLeaf<K, V> getLastLeafNode(BPNode<K, V> curr) {

		while(!(curr instanceof BPLeaf))
			curr = ((BPInternal<K,V>) curr).getRight();

		return ((BPLeaf<K, V>) curr);
	}
	
	// get the location of where key would be 
	private int nextLoc(BPInternal<K,V> node, K key) {
		int currec = node.binarySearch(key);

		if(currec < 0)
			return ((currec + 1) * -1);
		else // matches so direct it to the right
			return (currec + 1);
	}
	
	public BPNode<K, V> getRoot() {
		return root; // TODO i do not like this at all
	}
	
	// Contract requirements
	public void clear() {
		root = new BPLeaf<K, V>(leafOrder, new TupleComparator());
		cardinality = 0;
	}

	public boolean containsKey(Object key) {
		castCheck(key);
		nullCheck(key);
		return get(key) != null;
	}

	@SuppressWarnings("unchecked")
	public V get(Object key) {
		nullCheck(key);
		
		BPNode<K,?> rt = root;
		K k = (K) key;
		Stack<BPNode<K,?>> stack = new Stack<>();
		stack.push(root);
		
		while(!stack.isEmpty()) {
			rt = stack.pop();
			int currec = rt.binarySearch(k);
			if (rt instanceof BPLeaf) {
				if (currec > -1 && c.compare(
						((BPLeaf<K, V>)rt).pointers(currec), k) == 0)
					return ((BPLeaf<K,V>)rt).recs(currec);
				else 
					return null;
			} else {
				if(currec < 0)
					currec = (currec + 1) * -1;
				else // matches so direct it to the right
					currec += 1;
				
				stack.push(((BPInternal<K,V>)rt).pointers(currec));
			}
		}
		return null;
	}

	public boolean isEmpty() {
		return size() == 0;
	}

	public V put(K key, V value) {
		castCheck(key);
		nullCheck(key);
		nullCheck(value);
		
		V old = get(key);
		if(old == null) // count if it doesn't exist
			cardinality++; 
		
		insert(key, value);
		
		modCount++;
		return old;
	}
	
	public int size() {
		return cardinality;
	}

	public Comparator<? super K> comparator() {
		if(naturalOrdering)
			return c;
		
		return null;
	}

	public Set<Entry<K, V>> entrySet() {
		return new EntrySet();
	}

	public K firstKey() {
		if(isEmpty())
			throw new NoSuchElementException();
		
		BPLeaf<K, V> firstLeaf = getFirstLeafNode(root);
		return firstLeaf.getkeyVals().get(0).getKey();
	}
	
	public K lastKey() {
		if(isEmpty())
			throw new NoSuchElementException();
		
		BPLeaf<K, V> lastLeaf = getLastLeafNode(root);
		List<Tuple<K, V>> kvs = lastLeaf.getkeyVals();
		return kvs.get(kvs.size() - 1).getKey();
	}
	
	public SortedMap<K, V> subMap(K fromKey, K toKey) {		
		return new SubMap(fromKey, toKey);
	}
	
	public SortedMap<K, V> headMap(K arg0) {
		throw new UnsupportedOperationException();
	}

	public SortedMap<K, V> tailMap(K fromKey) {
		throw new UnsupportedOperationException();
	}
	
	@SuppressWarnings("unchecked")
	public V remove(Object key) {
		if(containsKey(key)) {
			modCount--;
			cardinality--;
		}
		
		try {
			return root.remove((K) key);
		} catch (LeafUnderflowException e) { 	 // okay to have underflow when at root level
			/*System.err.println("A " + e.getVal());*/
			return (V) e.getVal();
		} catch (InternalUnderflowException e) { // removal caused Internal to be a Leaf
			root = (BPNode<K, V>) e.getNode();
			/*System.err.println("A " + ((BPLeaf<K, V>) root).getkeyVals());*/
			return (V) e.getVal();
		}
	}
	
	public Set<K> keySet() {
		throw new UnsupportedOperationException();
	}
	
	public Collection<V> values() {
		throw new UnsupportedOperationException();
	}
	
	protected class SubMap extends AbstractMap<K, V> implements SortedMap<K, V> {

		private K fromKey, toKey;

		public SubMap(K fromKey, K toKey) {
			int comparison = c.compare(fromKey, toKey);
			
			nullCheck(fromKey);
			nullCheck(toKey);
			
			if(comparison > 0)
				throw new IllegalArgumentException("fromKey is greater than toKey");

			this.fromKey = fromKey;
			this.toKey = toKey;
		}
		  
		// find the BPinternal that matches the range
		private BPNode<K, V> getLoc() {
			
			BPNode<K, V> subRoot = root;
			
			while(!(subRoot instanceof BPLeaf) &&
					nextLoc(((BPInternal<K, V>) subRoot),fromKey) 
					== nextLoc(((BPInternal<K, V>) subRoot), toKey)) {
				subRoot = ((BPInternal<K, V>) subRoot).pointers
						(nextLoc(((BPInternal<K, V>) subRoot), toKey));
			}
			
			return subRoot;
		}
		
		// throw exception when a given to and don't conform range
		private void rangeCheck(K from, K to) {
			if(c.compare(from, fromKey) < 0)
				throw new IllegalArgumentException(from + " is below of range (" + fromKey + ").");
			else if(c.compare(to, toKey) >= 0)
				throw new IllegalArgumentException(to + " is above of range (" + toKey + ").");
		}

		public void clear() {
			BPTree.this.clear();
		}

		public boolean containsKey(Object key) {
			rangeCheck((K) key, (K) key);
			return BPTree.this.containsKey(key);
		}

		public V get(Object key) {
			rangeCheck((K) key, (K) key);
			return BPTree.this.get(key);
		}

		public boolean isEmpty() {
			return size() == 0;
		}

		public V put(K key, V value) {
			rangeCheck(key, key);
			return BPTree.this.put(key, value);
		}

		public void putAll(Map<? extends K, ? extends V> m) {
			// make sure all keys fit in the range
			for(K key: m.keySet())
				rangeCheck(key, key);	
			
			BPTree.this.putAll(m);
		}

		public V remove(Object key) {
			/*rangeCheck((K) key, (K) key);*/
			return BPTree.this.remove((K)key);
		}

		@SuppressWarnings("unused")
		public int size() {
			int size = 0;
			for(Map.Entry<K,V> unused: entrySet())
				size++;
			
			return size;
		}

		public Comparator<? super K> comparator() {
			return BPTree.this.comparator();
		}

		public K firstKey() {
			if(isEmpty())
				throw new NoSuchElementException();
			
			BPLeaf<K, V> firstLeaf = getFirstLeafNode(getLoc());
			return firstLeaf.getkeyVals().get(0).getKey();
		}
		
		public K lastKey() {
			if(isEmpty())
				throw new NoSuchElementException();
			
			BPLeaf<K, V> lastLeaf = getLastLeafNode(getLoc());
			List<Tuple<K, V>> kvs = lastLeaf.getkeyVals();
			return kvs.get(kvs.size() - 1).getKey();
		}
		
		public SortedMap<K, V> subMap(K fromKey, K toKey) {
			return new SubMap(fromKey, toKey);
		}
		
		public SortedMap<K, V> headMap(K arg0) {
			throw new UnsupportedOperationException();
		}

		public SortedMap<K, V> tailMap(K fromKey) {
			throw new UnsupportedOperationException();
		}
		
		public Set<K> keySet() {
			throw new UnsupportedOperationException();
		}
		
		public Collection<V> values() {
			throw new UnsupportedOperationException();
		}

		public Set<Entry<K, V>> entrySet() {
			return new SubMap.EntrySet();
		}
		
	    protected class EntrySet extends AbstractSet<Map.Entry<K,V>>  {
	    	public boolean remove(Object o) {
	    		if (!(o instanceof Map.Entry))
	    			return false;
	    		Map.Entry<K,V> entry = (Map.Entry<K,V>) o;
	    		V value = entry.getValue();
	    		Entry<K,V> p = new Tuple<K,V>(entry.getKey(), BPTree.this.get(entry.getKey()));
	    		if (p != null && valEquals(p.getValue(), value)) {
	    			BPTree.this.remove(entry.getKey());
	    			return true;
	    		}
	    		return false;
	    	}
	    	private final boolean valEquals(Object o1, Object o2) {
	    		return (o1==null ? o2==null : o1.equals(o2));
	    	}
	        
	        public boolean contains(Object o) {
	            Map.Entry<K,V> me = (Map.Entry<K,V>)o;
	            return BPTree.this.containsKey(me.getKey()) &&
	                (me.getValue() == null ?
	                    BPTree.this.get(me.getKey()) == null :
	                    me.getValue().equals(BPTree.this.get(me.getKey())));
	        }

			public Iterator<Entry<K,V>> iterator() {
				// start with a value greater or equal to fromKey
				BPLeaf<K, V> hurr = getFirstLeafNode(getLoc()), prev = hurr;
				int idx = 0;
				
				if(hurr.size() > 0) {
					// skip numBranches times to find appropriate location
					while(c.compare(fromKey, hurr.getkeyVals().get(idx).getKey()) >= 0) {
						prev = hurr;
						if(hurr.getNext() != null)
							hurr = (BPLeaf<K, V>) hurr.getNext();
						else
							break;
					}
					// look into where to begin within leaf node 
					while(prev.size() != idx &&
						c.compare(fromKey, prev.getkeyVals().get(idx).getKey()) > 0)
						idx++;
				}
				
				final BPLeaf<K, V> startLeaf = prev;
				final int iteratorModCount = modCount;
				final int start = idx;
				
				// iterate through each leaf node
				Iterator<Entry<K,V>> it = new Iterator<Entry<K,V>>() {
					
					int currIndex = start, numRemoved = 0;
					BPLeaf<K, V> currLeaf = startLeaf;
					boolean removed = true;
					Tuple<K,V> currEntry;

				    public boolean hasNext() {
				    	
				    	// ensure that next value is not outside of toKey
				    	if(currIndex < currLeaf.size()) {
				    		return (c.compare(toKey, currLeaf.getkeyVals().get(currIndex).getKey()) > 0);
				    	} else if(currIndex == currLeaf.size() && currLeaf.getNext() != null) {
				    		// temporarily change results
				    		int tempIndex = currIndex;
				    		BPLeaf<K, V> tempLeaf = currLeaf;
				    		 
				    		getNext();
				    		boolean res = (c.compare(toKey, currLeaf.getkeyVals().get(currIndex).getKey()) > 0);
				    		
				    		currIndex = tempIndex;
				    		currLeaf = tempLeaf;
				    		
				    		return res;
				    	}
				    	
				    	return false;
				    }
				
				    public Entry<K,V> next() {
				    	if((iteratorModCount - numRemoved) != modCount)
				    		throw new ConcurrentModificationException();
				    	else if(!hasNext())
				    		throw new NoSuchElementException();
				    	
				    	if(currIndex == currLeaf.size() && currLeaf.getNext() != null)
				    		getNext();

				    	removed = false;
				    	currEntry = currLeaf.getkeyVals().get(currIndex);

			    		currIndex++;
			    		return currEntry;			    		
				    }
				
				    public void remove() {
				    	if(!removed && currEntry != null && BPTree.this.get(currEntry.getKey()) != null) {
				    		currEntry.remove();
				    		BPTree.this.remove(currEntry.getKey());
				    		fastForeward();
				    		removed = true;
				    		numRemoved++; // offset iteratorModCount accordingly
				    	} else
				    		throw new IllegalStateException();
				    }
				    
				    private void fastForeward() {
				    	
				    	currIndex = currLeaf.binarySearch(currEntry.getKey());
				    	int insertionPoint = (currIndex + 1) * -1;
				    	while(currLeaf.size() == insertionPoint) {
				    		if(currLeaf.getNext() == null)
				    			break;
			    			currLeaf = currLeaf.getNext();
			    			currIndex = currLeaf.binarySearch(currEntry.getKey());
			    			insertionPoint = (currIndex + 1) * -1;
				    	}
				    	currIndex = insertionPoint;
				    }
				    
				    private void getNext() {
				    	currIndex = 0;
			    		currLeaf = currLeaf.getNext();
				    }
				};
				
				return it;
			}

			public int size() {
				return SubMap.this.size();
			}

			public void clear() {
				SubMap.this.clear();
			}
	    } // end of Entry Set
	} // end of SubMap
	
    protected class EntrySet extends AbstractSet<Map.Entry<K,V>>  {
    	public boolean remove(Object o) {
    		if (!(o instanceof Map.Entry))
    			return false;
    		Map.Entry<K,V> entry = (Map.Entry<K,V>) o;
    		V value = entry.getValue();
    		Entry<K,V> p = new Tuple<K,V>(entry.getKey(), BPTree.this.get(entry.getKey()));
    		if (p != null && valEquals(p.getValue(), value)) {
    			BPTree.this.remove(entry.getKey());
    			return true;
    		}
    		return false;
    	}
    	private final boolean valEquals(Object o1, Object o2) {
    		return (o1==null ? o2==null : o1.equals(o2));
    	}
		public boolean contains(Object o) {
            Map.Entry<K,V> me = (Map.Entry<K,V>)o;
            return BPTree.this.containsKey(me.getKey()) &&
                (me.getValue() == null ?
                    BPTree.this.get(me.getKey()) == null :
                    me.getValue().equals(BPTree.this.get(me.getKey())));
        }

		public Iterator<Entry<K,V>> iterator() {
			
			final BPLeaf<K, V> curr = getFirstLeafNode(root);
			final int iteratorModCount = modCount;
			
			// iterate through each leaf node
			Iterator<Entry<K,V>> it = new Iterator<Entry<K,V>>() {
				
				int currIndex = 0, numRemoved = 0;
				BPLeaf<K, V> currLeaf = curr;
				boolean removed = true;
				Tuple<K,V> currEntry = null;

			    public boolean hasNext() {
			    	if(currIndex < currLeaf.size()) {
			    		return true;
			    	} else if(currIndex == currLeaf.size()
			    			&& currLeaf.getNext() != null) {
			    		return true;
			    	}
			    	
			    	return false;
			    }
			
			    public Entry<K,V> next() {
			    	if((iteratorModCount - numRemoved) != modCount)
			    		throw new ConcurrentModificationException();
			    	else if(!hasNext())
			    		throw new NoSuchElementException();

			    	if(currIndex == currLeaf.size() && currLeaf.getNext() != null)
			    		getNext();
			    	
			    	removed = false;
		    		currEntry = currLeaf.getkeyVals().get(currIndex);		    		
		    		currIndex++;
		    		return currEntry;
			    }
			
			    public void remove() {
			    	if(!removed && currEntry != null) {
			    		currEntry.remove();
			    		BPTree.this.remove(currEntry.getKey());
			    		fastForeward();
			    		removed = true;
			    		numRemoved++; // offset iteratorModCount accordingly
			    	} else
			    		throw new IllegalStateException();
			    }
			    
			    private void fastForeward() {
			    	
			    	currIndex = currLeaf.binarySearch(currEntry.getKey());
			    	int insertionPoint = (currIndex + 1) * -1;
			    	while(currLeaf.size() == insertionPoint) {
			    		if(currLeaf.getNext() == null)
			    			break;
		    			currLeaf = currLeaf.getNext();
		    			currIndex = currLeaf.binarySearch(currEntry.getKey());
		    			insertionPoint = (currIndex + 1) * -1;
			    	}
			    	currIndex = insertionPoint;
			    }
			    
			    private void getNext() {
			    	currIndex = 0;
		    		currLeaf = currLeaf.getNext();
			    }
			};
			return it;

		}
		
		public int size() {
			return BPTree.this.size();
		}

		public void clear() {
			BPTree.this.clear();
		}
    }
}