package model.dictionary.dt;

import java.util.Map;

public class Tuple<K, V> implements Map.Entry<K, V> {
	
	private K key;
	private V val;
	private boolean removed;
	
	public Tuple(K key, V val) {
		this.key = key;
		this.val = val;
		removed = false;
	}
	
	public K getKey() {
		return key;
	}
	
	public V getValue() {
		return val;
	}
	
	public boolean equals(Object o) {
		if (!(o instanceof Map.Entry))
			return false;
		Map.Entry<?,?> e = (Map.Entry<?,?>)o;
		return valEquals(key,e.getKey()) && valEquals(val,e.getValue());
	}
	
	static final boolean valEquals(Object o1, Object o2) {
		return (o1==null ? o2==null : o1.equals(o2));
	}

	public String toString() {
		return key + "=" + val;
	}
	
	public void remove() {
		removed = true;
	}

	@SuppressWarnings("unchecked")
	public Object setValue(Object val) {
		
		V newVal = (V) val; 
		if(val == null)
			throw new NullPointerException("This type doesn't support null values.");
		if(removed)
			throw new IllegalStateException("Can't modify after it's been removed.");
		
		V temp = this.val;
		
		this.val = newVal;
		return temp;
	}
	
	public int hashCode() {
		int keyHash = (key==null ? 0 : key.hashCode());
		int valueHash = (val==null ? 0 : val.hashCode());
		return keyHash ^ valueHash;
	}
}