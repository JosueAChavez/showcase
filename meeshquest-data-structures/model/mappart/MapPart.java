package model.mappart;

import java.awt.geom.Point2D;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public interface MapPart {
	public Element getXML(Document doc, String tagName);

	public double distancePoint(Point2D point);
}
