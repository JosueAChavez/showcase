package model.mappart.cities;

public enum CityType {
	ISOLATED, CONNECTED, NEITHER;
}
