package model.mappart.cities;

import java.util.Set;
import java.util.TreeSet;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import model.Variables;

public class Airport extends City {

	private static final long serialVersionUID = 1L;
	TreeSet<Terminal> terminals = new TreeSet<>(Variables.CITY_NAME_COMP);
	
	public Airport(String name, float localX, float localY, double remoteX, double remoteY) {
		super(name, localX, localY, 0, "", CityType.ISOLATED, remoteX, remoteY);
	}
	
	public void addTerminal(Terminal terminal) {
		terminals.add(terminal);
	}

	public boolean removeTerminal(Terminal terminal) {
		return terminals.remove(terminal);
	}
	
	public boolean hasNoTerminals() {
		return terminals.isEmpty();
	}
	
	public Element getXML(Document doc, String tagName) {
		
		Element airport;

		if (tagName == null)
			airport = doc.createElement("airport");
		else
			airport = doc.createElement(tagName);

		airport.setAttribute("localX", Integer.toString(Math.round(x)));
		airport.setAttribute("localY", Integer.toString(Math.round(y)));

		airport.setAttribute("name", name);
		
		airport.setAttribute("remoteX", Integer.toString((int) Math.round(remoteX)));
		airport.setAttribute("remoteY", Integer.toString((int) Math.round(remoteY)));

		return airport;
	}

	public Set<Terminal> getTerminals() {
		return terminals;
	}
}
