package model.mappart.cities;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Terminal extends City {

	private static final long serialVersionUID = 1L;
	private String cityName, airportName;

	public Terminal(String name, float localX, float localY, double remoteX, double remoteY,
			String cityName, String airportName) {
		super(name, localX, localY, 0, "", CityType.NEITHER, remoteX, remoteY);
		this.cityName = cityName;
		this.airportName = airportName;
	}

	public Element getXML(Document doc, String tagName) {
		
		Element terminal;

		if (tagName == null)
			terminal = doc.createElement("terminal");
		else
			terminal = doc.createElement(tagName);

		terminal.setAttribute("airportName", airportName);
		terminal.setAttribute("cityName", cityName);
		terminal.setAttribute("localX", Integer.toString(Math.round(x)));
		terminal.setAttribute("localY", Integer.toString(Math.round(y)));

		terminal.setAttribute("name", name);
		
		terminal.setAttribute("remoteX", Integer.toString((int) Math.round(remoteX)));
		terminal.setAttribute("remoteY", Integer.toString((int) Math.round(remoteY)));

		return terminal;
	}
	
	/**
	 * Get the airport the terminal belongs to
	 * @return
	 */
	public String getAirport() {
		return airportName;
	}
	
	/**
	 * Get the city that the terminal is connected to
	 * @return
	 */
	public String getCity() {
		return cityName;
	}
}