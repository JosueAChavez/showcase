package model.mappart.cities;

import java.awt.geom.*;
import java.util.LinkedHashMap;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import model.mappart.MapPart;

public class City extends Point2D.Float implements Comparable<City>, MapPart {

	private static final long serialVersionUID = 1L;
	protected String name;
	private String color;
	private int radius;
	private CityType type;
	private boolean isRemote;
	protected double remoteX, remoteY;
	
	public City (String name, int x, int y, int radius, String color) {		
		this(name, x, y, radius, color, CityType.NEITHER);
	}
	
	public City(String name, float x, float y, int radius, String color, CityType type) {
		this.name = name;
		super.x = x;
		super.y = y;
		this.radius = radius;
		this.color = color;
		this.type = type;
		remoteX = 0.0;
		remoteY = 0.0;
		isRemote = false;
	}
	
	public City(String name, float x, float y, int radius, String color,
			double remoteX, double remoteY) {
		this(name, x, y, radius, color, CityType.NEITHER, remoteX, remoteY);
	}
	
	public City(String name, float x, float y, int radius, String color, CityType type,
			double remoteX, double remoteY) {
		this.name = name;
		super.x = x;
		super.y = y;
		this.radius = radius;
		this.color = color;
		this.type = type;
		this.remoteX = remoteX;
		this.remoteY = remoteY;
		isRemote = true;
	}

	public String getName() {
		return name;
	}
	
	public Element getXML(Document doc, String tagName) {
		
		Element city;
		
		if(tagName == null && type != CityType.NEITHER) {
			if(type == CityType.ISOLATED)
				tagName = "isolatedCity";
			else
				tagName = "city";
		}
			
		city = doc.createElement(tagName);
		
		city.setAttribute("color", this.color);
		city.setAttribute("radius", Integer.toString(this.radius));
		city.setAttribute("name", this.name);
		
    	if(isRemote) {
    		city.setAttribute("localX", Integer.toString(Math.round(this.x)));
    		city.setAttribute("localY", Integer.toString(Math.round(this.y)));
    		city.setAttribute("remoteX", Integer.toString((int) Math.round(remoteX)));
    		city.setAttribute("remoteY", Integer.toString((int) Math.round(remoteY)));
    	} else {
    		city.setAttribute("x", Integer.toString(Math.round(this.x)));
    		city.setAttribute("y", Integer.toString(Math.round(this.y)));
    	}
		
		return city;
	}

	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + radius;
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		City other = (City) obj;
		return this.x == other.x && this.y == other.y && this.remoteX 
				== other.remoteX && this.remoteY == other.remoteY; 
	}

	public int compareTo(City city) {
		long yr = Math.round(this.remoteY - city.remoteY);  	
    	
    	if(yr == 0) {
    		long xr = Math.round(this.remoteX - city.remoteX);
    		if(xr == 0) {
    			int yl = Math.round(this.y - city.y);
		    	if(yl == 0)
		    		return Math.round(this.x - city.x);
		    	return yl;
    		}
    		return (int) xr;
    	}
    	return (int) yr;
	}

	public Map<String, String> getParameters() {
    	// add parameters
		LinkedHashMap<String, String> parameters = new LinkedHashMap<String, String>();
    	parameters.put("name", name);

    	if(isRemote) {
	    	parameters.put("localX", Integer.toString(Math.round(x)));
	    	parameters.put("localY", Integer.toString(Math.round(y)));
	    	parameters.put("remoteX", Integer.toString((int) Math.round(remoteX)));
	    	parameters.put("remoteY", Integer.toString((int) Math.round(remoteY)));
    	} else {
    		parameters.put("x", Integer.toString(Math.round(x)));
	    	parameters.put("y", Integer.toString(Math.round(y)));
    	}
    	
    	parameters.put("radius", Integer.toString(radius));
    	parameters.put("color", color);
    	
    	return parameters;
	}

	public void changeState(CityType type) {
		this.type = type;
	}

	public CityType getCityType() {
		return type;
	}
	
	public double getRemoteX() {
		return remoteX;
	}

	public double getRemoteY() {
		return remoteY;
	}

	public String toString() {
		return name; 
	}

	public double distancePoint(Point2D point) {
		return distance(point);
	}

	public Point2D getRemote() {
		return new Point2D.Double(remoteX, remoteY);
	}
	
	public Point2D getLocal() {
		return new Point2D.Double(x, y);
	}
}
