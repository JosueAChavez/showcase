package model.mappart;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import model.mappart.cities.City;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

public class Road extends Line2D.Double implements MapPart, Comparable<Road> {

	private static final long serialVersionUID = 1L;
	private City start, end;
	private String ogStartName, ogEndName;

	public Road(City start, City end) {

		ogStartName = start.getName();
		ogEndName = end.getName();
		
		// decide the end based on which has the higher name
		if(start.getName().compareTo(end.getName()) > 0) {
			this.start = end;
			this.end = start;
			start = end;
			end = this.end;
		} else {
			this.start = start;
			this.end = end;	
		}
		
		super.x1 = start.getX();
		super.y1 = start.getY();
		super.x2 = end.getX();
		super.y2 = end.getY();
	}
	

	public Element getXML(Document doc, String tagName) {  
		
		String startName = start.getName(), endName = end.getName();
		
		if(tagName == null) {
			tagName = "road";
		} else if (tagName.equals("roadCreated") || tagName.equals("roadDeleted")) {
			startName = ogStartName;
			endName = ogEndName;
		}
		
		Element road = doc.createElement(tagName);
		
		road.setAttribute("end", endName);
		road.setAttribute("start", startName);
		
		return road;
	}
	
	public double distancePoint(Point2D point) {
		return super.ptLineDist(point); 
	}
	
	public String getStart() {
		return ogStartName;
	}
	
	public String getEnd() {
		return ogEndName;
	}
	
	public City getStartCity() {
		return start;
	}
	
	public City getEndCity() {
		return end;
	}
	
	/**
	 * Checks to see if a city on either ends of the road
	 * @param city
	 * @return true if the city either the start or end, 
	 * false otherwise.
	 */
	
	public boolean inRoad(City city) {
		return end.equals(city) || start.equals(city);
	}
		
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((end == null) ? 0 : end.hashCode());
		result = prime * result + ((start == null) ? 0 : start.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Road))
			return false;
		Road other = (Road) obj;
		if (end == null) {
			if (other.end != null)
				return false;
		} else if (!inRoad(other.end))
			return false;
		if (start == null) {
			if (other.start != null)
				return false;
		} else if (!inRoad(other.start))
			return false;
		return true;
	}

	public int compareTo(Road other) {
		int startComp = other.start.getName().compareTo(start.getName());
		return startComp == 0 ? other.end.getName().compareTo(end.getName()): startComp;
	}
	
	public String toString() {
		return start + " <=> " + end;
	}
}
