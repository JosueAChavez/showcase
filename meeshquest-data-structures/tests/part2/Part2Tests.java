package tests.part2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static tests.TestTools.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.*;

import model.dictionary.dt.BPTree;
import model.dictionary.dt.Tuple;
import model.mappart.cities.City;
import controller.comparators.CityGraphComparator;


public class Part2Tests {
	
	private final static String in = ".input.xml", out = ".output.xml"; 
	
	private final String[] names = {"Suplex_City", "Death_Valley", "Japan", "Pensocola",
			"Washington", "Jabroni_Boulevard", "Parts_Unknown", "Smackdown_Hotel",
			"Mexico_City", "Hollywood", "Kenny_Omega", "Razor_Ramon"};
	
	private final String[] alpha = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", 
			"N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
	
	public Part2Tests() {
		PATH = "\\tests\\part2\\";
	}
	
	@Test public void quickTest() {
		String id = "createCity/";
		runTest("temp", id + "input.xml", id + "output.xml", false);
	}
	
	@Test public void nonFatal() {
		String id = "nonFatal", u = id + "\\", p = u + id;
		runTest(id+"1", p+"1"+in, p+"1"+out, false);
		runTest(id+"2", p+"2"+in, p+"2"+out, false);
	}
	
	@Test public void publicTests() {
		String u = "public\\";
		runTest("pub1", u + "part2.public.dodekatrie.input.xml", u + "part2.public.dodekatrie.output.xml", false);
		runTest("pub2", u + "part2.public.listCities.input.xml", u + "part2.public.listCities.output.xml", false);
		runTest("pub3", u + "part2.public.nearestCity.input.xml", u + "part2.public.nearestCity.output.xml", false);
		runTest("pub4", u + "part2.public.range.input.xml", u + "part2.public.range.output.xml", false);
		runTest("pub5", u + "part2.public.shortestPath.input.xml", u + "part2.public.shortestPath.output.xml", false);
	}
	
	@Test public void mapRoadTests() {
		String id = "mapRoad", u = id + "\\", p = u + id;
		runTest(id+"1", p+"1"+in, p+"1"+out, false);
		runTest(id+"2", p+"2"+in, p+"2"+out, false);
		runTest(id+"3", p+"3"+in, p+"3"+out, false);
		runTest(id+"4", p+"4"+in, p+"4"+out, false);
	}
	
	@Test public void createCityTests() {
		String id = "createCity", u = id + "\\", p = u + id;
		runTest(id+"1", p+"1"+in, p+"1"+out, false);
		runTest(id+"2", p+"2"+in, p+"2"+out, false);
	}
	
	@Test public void rangeCitiesTests() {
		String id = "rangeCities", u = id + "\\", p = u + id;
		runTest(id+"1", p+"1"+in, p+"1"+out, false);
	}
	
	@Test public void nearestTests() {
		String id = "nearest", u = id + "\\", p = u + id;
		runTest(id+"1", p+"1"+in, p+"1"+out, false);
	}
	
	@Test public void rangeRoadsTests() {
		String id = "rangeRoads", u = id + "\\", p = u + id;
		runTest(id+"1", p+"1"+in, p+"1"+out, false);
		runTest(id+"2", p+"2"+in, p+"2"+out, false);
	}

	@Test public void nearestIsolatedCityTests() {
		String id = "nearestIsolatedCity", u = id + "\\", p = u + id;
		runTest(id+"0", p+"0"+in, p+"0"+out, true);
		runTest(id+"1", p+"1"+in, p+"1"+out, false);
		runTest(id+"2", p+"2"+in, p+"2"+out, false);
		runTest(id+"3", p+"3"+in, p+"3"+out, false);
		/*runTest(id+"4", p+"4"+in, p+"4"+out, true);*/
		runTest(id+"5", p+"5"+in, p+"5"+out, true);
		runTest(id+"6", p+"6"+in, p+"6"+out, false);
		runTest(id+"7", p+"7"+in, p+"7"+out, false);
		runTest(id+"8", p+"8"+in, p+"8"+out, true);
		runTest(id+"9", p+"9"+in, p+"9"+out, true);
		runTest(id+"10", p+"10"+in, p+"10"+out, true);
		runTest(id+"11", p+"11"+in, p+"11"+out, true);
		runTest(id+"12", p+"12"+in, p+"12"+out, true);
		runTest(id+"14", p+"14"+in, p+"14"+out, false);
		runTest(id+"13", p+"13"+in, p+"13"+out, true);
	}
	
	@Test public void nearestCityTests() {
		String id = "nearestCity", u = id + "\\", p = u + id;
		runTest(id+"1", p+"1"+in, p+"1"+out, false);
		runTest(id+"2", p+"2"+in, p+"2"+out, false);
		runTest(id+"3", p+"3"+in, p+"3"+out, false);
		/*runTest(id+"4", p+"4"+in, p+"4"+out, true);*/
		runTest(id+"5", p+"5"+in, p+"5"+out, true);
		runTest(id+"6", p+"6"+in, p+"6"+out, false);
		runTest(id+"7", p+"7"+in, p+"7"+out, false);
		runTest(id+"8", p+"8"+in, p+"8"+out, true);
		runTest(id+"9", p+"9"+in, p+"9"+out, true);
		runTest(id+"10", p+"10"+in, p+"10"+out, true);
	}
	
	@Test public void nearestRoadTests() {
		String id = "nearestRoad", u = id + "\\", p = u + id;
		runTest(id+"1", p+"1"+in, p+"1"+out, false);
		runTest(id+"2", p+"2"+in, p+"2"+out, false);
	}
	
	@Test public void nearestCityToRoadTests() {
		String id = "nearestCityToRoad", u = id + "\\", p = u + id;
		runTest(id+"1", p+"1"+in, p+"1"+out, false);
		runTest(id+"2", p+"2"+in, p+"2"+out, false);
		runTest(id+"3", p+"3"+in, p+"3"+out, false);
		runTest(id+"4", p+"4"+in, p+"4"+out, false);
	}
	
	@Test public void shortestPathTests() {
		String id = "shortestPath", u = id + "\\", p = u + id;
		runTest(id+"1", p+"1"+in, p+"1"+out, false);
		runTest(id+"2", p+"2"+in, p+"2"+out, false);
		runTest(id+"3", p+"3"+in, p+"3"+out, false);
		runTest(id+"4", p+"4"+in, p+"4"+out, false);
		runTest(id+"5", p+"5"+in, p+"5"+out, false);
		/*runTest(id+"6", p+"6"+in, p+"6"+out, false);
		runTest(id+"7", p+"7"+in, p+"7"+out, false);*/
		runTest(id+"8", p+"8"+in, p+"8"+out, false);
	}
	
	@Test public void dodekaTrieTests() {
		String id = "dodekaTrie", u = id + "\\", p = u + id;
		runTest(id+"1", p+"1"+in, p+"1"+out, false);
		runTest(id+"2", p+"2"+in, p+"2"+out, false);
		runTest(id+"3", p+"3"+in, p+"3"+out, false);
		runTest(id+"4", p+"4"+in, p+"4"+out, false);
		runTest(id+"5", p+"5"+in, p+"5"+out, false);
		runTest(id+"6", p+"6"+in, p+"6"+out, false);
	}
	
	// DoedekaTrie
	@Test public void dtTest() {
		SortedMap<City, String> dt = new BPTree<City, String>(2, new CityGraphComparator());
		SortedMap<City, String> tm = new TreeMap<City, String>(new CityGraphComparator());

		ArrayList<City> cities = new ArrayList<>();
		char[] alpha = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 
				'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
		for (int i = 0; i < 52; i++) {
			cities.add(new City(alpha[i % 26] + "" + i+1, 5, i+1, 3, "Electric Blue"));
			/*System.out.println(
					" <createCity id=\"" + i + "\" name=\"" + alpha[i % 26] + "" + i + 
					"\" x=\"5\" y=\"" + i + "\" radius=\"" + 3 +"\" color=\"red\"/>");*/
		}
		assertTrue(dt.toString().equals(tm.toString()));

		assertTrue(tm.equals(dt));
		assertTrue(dt.equals(tm));
		assertTrue(dt.hashCode() == tm.hashCode());

		
		// finds
		for(City city: cities) {
			assertNull(dt.get(city));
			assertNull(tm.get(city));
			
			assertTrue(tm.equals(dt));
			assertTrue(dt.equals(tm));
			assertTrue(dt.toString().equals(tm.toString()));
			assertTrue(dt.hashCode() == tm.hashCode());
		}
		
		// inserts
		for(City city: cities) {
			assertNull(dt.get(city));
			assertNull(tm.get(city));
			dt.put(city, city.getName());
			tm.put(city, city.getName());
			assertNotNull(dt.get(city));
			assertNotNull(tm.get(city));
			
			assertTrue(tm.equals(dt));
			assertTrue(dt.equals(tm));
			assertTrue(dt.toString().equals(tm.toString()));
			assertTrue(dt.hashCode() == tm.hashCode());
			assertTrue(dt.firstKey().equals(tm.firstKey()));
			assertTrue(dt.lastKey().equals(tm.lastKey()));
		}
		
		// finds again
		for(City city: cities) {
			assertTrue(dt.get(city).equals(city.getName()));
			assertTrue(tm.equals(dt));
			assertTrue(dt.equals(tm));
			assertTrue(dt.toString().equals(tm.toString()));
			assertTrue(dt.hashCode() == tm.hashCode());
		}
		
		/*System.err.println(dt);*/
	}
	
	@Test public void entrySetTest() {
		SortedMap<String, String> m = new BPTree<String, String>(4);
		m.put("Auto","Fail");
		Set<Map.Entry<String, String> > s = m.entrySet();
		m.put("F","---");
		assertTrue(s.contains(new Tuple<String, String>("F","---")));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void subMapTestBad1() {
		SortedMap<Integer, String> dt = makeDT();
		
		// toKey is smaller than fromKey
		dt.subMap(43, 13);
	}

	@SuppressWarnings("unused")
	@Test(expected = ConcurrentModificationException.class)
	public void TestBad2() {
		SortedMap<Integer, String> dt = makeDT();
		
		int i = 0;
		for(Map.Entry<Integer, String> me: dt.entrySet()) {
			i++;
			if (i == 3)
				dt.put(i, "aloha");
		}
	}
	
	@Test public void subMapTest() {
		
		int start = 15, end = 500;
		Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> t = randomTwins(10, 25);
		SortedMap<Integer, String> dt = t.getKey();
		SortedMap<Integer, String> dt3 = t.getKey();
		SortedMap<Integer, String> tm = t.getValue();
		
		assertEquals(dt, tm);
		
		SortedMap<Integer, String> dt1 = new BPTree<>(4);
		SortedMap<Integer, String> dt2 = new BPTree<>(4);
		
		dt1.put(1, "hi");
		dt1.put(2, "bye");
		
		dt2.put(1, "hi");
		dt2.put(2, "bye");

		assertTrue(dt1.subMap(1, 2).entrySet().equals(dt2.subMap(1, 2).entrySet()));
		
		SortedMap<Integer, String> dtSub =  dt.subMap(start, end);
		SortedMap<Integer, String> dtSub3 =  dt3.subMap(start, end);
		SortedMap<Integer, String> tmSub =  tm.subMap(start, end);
		
		assertTrue(dtSub3.entrySet().equals(dtSub.entrySet()));
		assertTrue(dtSub.entrySet().equals(dtSub3.entrySet()));

		assertEquals(dtSub.toString(), tmSub.toString());
		assertEquals(dtSub.entrySet().toString(), tmSub.entrySet().toString());
		assertTrue(dtSub.entrySet().equals(tmSub.entrySet()));
		
		assertTrue(dtSub.entrySet().equals(tmSub.entrySet()));
		assertTrue(tmSub.entrySet().equals(dtSub.entrySet()));
				
		/*
		t = randomTwins(10, 5000);
		
		dt.putAll(t.getKey());
		tm.putAll(t.getValue());
		
		assertEquals(dt, tm);
		assertEquals(dtSub, tmSub);
		
		dtSub =  dt.subMap(start, end);
		tmSub =  tm.subMap(start, end);

		assertEquals(dtSub.entrySet(), tmSub.entrySet());*/
	}
	
	
	@Test public void subMapTest1() {
		
		int start = 10, end = 10;
		Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> t = makeTwins(1);
		SortedMap<Integer, String> dt = t.getKey();
		SortedMap<Integer, String> tm = t.getValue();
	
		SortedMap<Integer, String> dtSub = dt.subMap(start, end);
		SortedMap<Integer, String> tmSub = tm.subMap(start, end);
		assertEquals(dtSub.toString(), tmSub.toString());
		
		// normal
		for(start = 0; start < dt.size(); start++){
			for(end = start; end < dt.size(); end++) {
				dtSub = dt.subMap(start, end);
				tmSub = tm.subMap(start, end);
				/*System.out.println(start + "  " + end);
				System.out.println(tmSub);*/
				assertEquals(dtSub, tmSub);
			}
		}
	}
	
	@Test public void subMapTest2() {
		
		int start = 1, end = 16;
		Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> t = makeTwins(2);
		SortedMap<Integer, String> dt = t.getKey();
		SortedMap<Integer, String> tm = t.getValue();
	
		SortedMap<Integer, String> dtSub = dt.subMap(start, end);
		SortedMap<Integer, String> tmSub = tm.subMap(start, end);
		
		int oldDTSize = dt.size();
		dtSub.put(3, "Cali");
		tmSub.put(3, "Cali");
		
		assertEquals(dtSub.get(3),dt.get(3));
		assertEquals(dt.size(), oldDTSize + 1);
		
		assertEquals(dtSub.size(), tmSub.size());
		
		/*for(start = 0; start < dt.size(); start++){
			for(end = start; end < dt.size(); end++) {
				dtSub = dt.subMap(start, end);
				tmSub = tm.subMap(start, end);
				System.out.println(start + "  " + end);
				System.out.println(tmSub);
				assertEquals(dtSub.size(), tmSub.size());
			}
		}*/
	}
	
	// two sub maps of same range
	@Test public void subMapTest3() {
		
		int start = 1, end = 16;
		Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> t = makeTwins(2);
		SortedMap<Integer, String> dt = t.getKey();
		SortedMap<Integer, String> tm = t.getValue();
	
		SortedMap<Integer, String> dtSub1 = dt.subMap(start, end);
		SortedMap<Integer, String> dtSub2 = dt.subMap(start, end);
		SortedMap<Integer, String> tmSub = tm.subMap(start, end);
		
		assertEquals(dtSub1, dtSub2);
		
		dtSub1.put(3, "Cali");
		tmSub.put(3, "Cali");
		
		assertEquals(dtSub1, dtSub2);
		assertEquals(dtSub1.toString(), dtSub2.toString());
		
		dt.put(100, "Germantown");
		
		assertEquals(dtSub1.get(3), dt.get(3));
		assertEquals(dtSub1.get(3), dtSub2.get(3));

		assertEquals(dtSub2.size(), tmSub.size());
		assertEquals(dtSub1.size(), dtSub2.size());
	}
	
	// test toString
	@Test public void randomTest() {
		
		int randOrder = (int) (Math.random() * 5) + 1, 
			randLen = (int) (Math.random() * 500) + 1;
			
		SortedMap<Integer, String> tm = new TreeMap<Integer, String>();
		SortedMap<Integer, String> dt = new BPTree<Integer, String>(randOrder);
		ArrayList<String> cities = new ArrayList<>();

		for (int i = 0; i < randLen; i++) {
			cities.add(alpha[i % 26] + "" + i+1);
		}
		
		int i = 0;
		for(String city: cities) {
			dt.put(i++, city);
			tm.put(i-1, city);
			assertEquals("1", dt.toString(), tm.toString());
		}
		
		int size = dt.size();
		for(String city: cities) {
			int rand = (int) (Math.random() * size);
			dt.put(rand, city);
			tm.put(rand, city);
		}
		
		assertEquals("1", dt.toString(), tm.toString());
		
		assertEquals("1", dt.toString(), tm.toString());
	}
	
	@Test public void emtpySubMapTest() {
		
		SortedMap<Integer, String> dt = new BPTree<>(1);
		SortedMap<Integer, String> tm = new TreeMap<>();

		
		SortedMap<Integer, String> dtSub = dt.subMap(1, 10);
		SortedMap<Integer, String> tmSub = tm.subMap(1, 10);
		
		
		assertEquals(tm, dt);
		assertEquals(tmSub, dtSub);
		
		/*dtSub.put(5, "A5");
		tmSub.put(5, "A5");
		
		assertEquals(dtSub.firstKey(), dt.firstKey());
		assertEquals(dtSub.firstKey(), tmSub.firstKey());
		assertEquals(tm.firstKey(), dt.firstKey());*/
	}
	
	@Test public void entrySetComparatorTest() {
		// without comparators
		SortedMap<NoCompNoEquals, String> dt1 = new BPTree<>(5, new NoCompNoEqualsComparator());
		SortedMap<CompNoEquals, String> dt2 = new BPTree<>(5, new CompNoEqualsComparator());
		SortedMap<NoCompEquals, String> dt3 = new BPTree<>(5, new NoCompEqualsComparator());
		SortedMap<CompEquals, String> dt4 = new BPTree<>(5, new CompEqualsComparator());
		
		SortedMap<NoCompNoEquals, String> tm1 = new TreeMap<>(new NoCompNoEqualsComparator());
		SortedMap<CompNoEquals, String> tm2 = new TreeMap<>(new CompNoEqualsComparator());
		SortedMap<NoCompEquals, String> tm3 = new TreeMap<>(new NoCompEqualsComparator());
		SortedMap<CompEquals, String> tm4 = new TreeMap<>(new CompEqualsComparator());
		
		/*SortedMap<NoCompNoEquals, String> dt5 = new BPTree<>(5);*/
		SortedMap<CompNoEquals, String> dt6 = new BPTree<>(5);
		/*SortedMap<NoCompEquals, String> dt7 = new BPTree<>(5);*/
		SortedMap<CompEquals, String> dt8 = new BPTree<>(5);
		
		/*SortedMap<NoCompNoEquals, String> tm5 = new TreeMap<>();*/
		SortedMap<CompNoEquals, String> tm6 = new TreeMap<>();
		/*SortedMap<NoCompEquals, String> tm7 = new TreeMap<>();*/
		SortedMap<CompEquals, String> tm8 = new TreeMap<>();

		
		// bulk inserts
		for(int i = 0; i < names.length; i++) {
			NoCompNoEquals o1 = new NoCompNoEquals();
			CompNoEquals o2 = new CompNoEquals();
			NoCompEquals o3 = new NoCompEquals();
			CompEquals o4 = new CompEquals();
			
			tm1.put(o1, names[i]);
			tm2.put(o2, names[i]);
			tm3.put(o3, names[i]);
			tm4.put(o4, names[i]);
			
			dt1.put(o1, names[i]);
			dt2.put(o2, names[i]);
			dt3.put(o3, names[i]);
			dt4.put(o4, names[i]);
			
			assertEquals(dt1, tm1);
			assertEquals(dt2, tm2);
			assertEquals(dt3, tm3);
			assertEquals(dt4, tm4);
			assertEquals(dt1.toString(), tm1.toString());
			assertEquals(dt2.toString(), tm2.toString());
			assertEquals(dt3.toString(), tm3.toString());
			assertEquals(dt4.toString(), tm4.toString());
			
			tm6.put(o2, names[i]);
			tm8.put(o4, names[i]);
			
			dt6.put(o2, names[i]);
			dt8.put(o4, names[i]);
			
			assertEquals(dt6, tm6);
			assertEquals(dt8, tm8);
			
			assertEquals(dt6.toString(), tm6.toString());
			assertEquals(dt8.toString(), tm8.toString());
		}
			// comparators vs comparables
			assertNotEquals(dt2.toString(), dt6.toString());
			assertNotEquals(dt4.toString(), dt8.toString());

			assertEquals(dt2, dt6);
			assertEquals(dt4, dt8);
		// bulk change values
		for(int i = 0; i < alpha.length; i++) {
			NoCompNoEquals o1 = new NoCompNoEquals(i);
			CompNoEquals o2 = new CompNoEquals(i);
			NoCompEquals o3 = new NoCompEquals(i);
			CompEquals o4 = new CompEquals(i);
			
			tm1.put(o1, alpha[i]);
			tm2.put(o2, alpha[i]);
			tm3.put(o3, alpha[i]);
			tm4.put(o4, alpha[i]);
			
			dt1.put(o1, alpha[i]);
			dt2.put(o2, alpha[i]);
			dt3.put(o3, alpha[i]);
			dt4.put(o4, alpha[i]);
			
			assertEquals(dt1, tm1);
			assertEquals(dt2, tm2);
			assertEquals(dt3, tm3);
			assertEquals(dt4, tm4);
			assertEquals(dt1.toString(), tm1.toString());
			assertEquals(dt2.toString(), tm2.toString());
			assertEquals(dt3.toString(), tm3.toString());
			assertEquals(dt4.toString(), tm4.toString());
		}
		
	}

	
	@Test public void subMapTest4() {
		Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> t = makeTwins(2);

		
		SortedMap<Integer, String> dt = t.getKey();
		SortedMap<Integer, String> tm = t.getValue();
		
		Set<Map.Entry<Integer, String>> dtes = dt.entrySet();
		Set<Map.Entry<Integer, String>> tmes = tm.entrySet();
		
		assertEquals(dtes.toString(), tmes.toString());
		assertEquals(dtes, tmes);
		
		t = randomTwins((int) (Math.random() * 50 + 1), (int) (Math.random() * 2000 + 1));
		
		dt.putAll(t.getValue());
		tm.putAll(t.getKey());
		
		assertEquals(dtes.toString(), tmes.toString());
		assertEquals(dtes, tmes);
	}
	@Test public void TheTest() {

		// without comparators
		SortedMap<CompNoEquals, String> dt2 = new BPTree<>(5, new CompNoEqualsComparator());
		SortedMap<CompEquals, String> dt4 = new BPTree<>(5, new CompEqualsComparator());
		
		SortedMap<CompNoEquals, String> tm2 = new TreeMap<>(new CompNoEqualsComparator());
		SortedMap<CompEquals, String> tm4 = new TreeMap<>(new CompEqualsComparator());
		
		SortedMap<CompNoEquals, String> dt6 = new BPTree<>(5);
		SortedMap<CompEquals, String> dt8 = new BPTree<>(5);
		
		SortedMap<CompNoEquals, String> tm6 = new TreeMap<>();
		SortedMap<CompEquals, String> tm8 = new TreeMap<>();

		
		// bulk inserts
		for(int i = 0; i < names.length; i++) {
			CompNoEquals o2 = new CompNoEquals();
			CompEquals o4 = new CompEquals();
			
			tm2.put(o2, names[i]);
			tm4.put(o4, names[i]);
			
			dt2.put(o2, names[i]);
			dt4.put(o4, names[i]);
			
			assertEquals(dt2, tm2);
			assertEquals(dt4, tm4);
			assertEquals(dt2.toString(), tm2.toString());
			assertEquals(dt4.toString(), tm4.toString());
			
			tm6.put(o2, names[i]);
			tm8.put(o4, names[i]);
			
			dt6.put(o2, names[i]);
			dt8.put(o4, names[i]);
			
			assertEquals(dt6, tm6);
			assertEquals(dt8, tm8);
			
			assertEquals(dt6.toString(), tm6.toString());
			assertEquals(dt8.toString(), tm8.toString());
		}	
		
		/*System.out.println(dt2.toString()+ "\n" + dt6.toString());*/
		// comparators vs comparables
		assertNotEquals(dt2.toString(), dt6.toString());
		assertNotEquals(dt4.toString(), dt8.toString());

		assertEquals(dt2, tm6);
		assertEquals(tm6, dt2);
		
		assertEquals(dt2, dt6);
		assertEquals(dt4, dt8);
		
		// bulk change values
		for(int i = 0; i < alpha.length; i++) {
			CompNoEquals o2 = new CompNoEquals(i);
			CompEquals o4 = new CompEquals(i);
			
			tm2.put(o2, alpha[i]);
			tm4.put(o4, alpha[i]);
			
			dt2.put(o2, alpha[i]);
			dt4.put(o4, alpha[i]);
			
			assertEquals(dt2, tm2);
			assertEquals(dt4, tm4);
			assertEquals(dt2.toString(), tm2.toString());
			assertEquals(dt4.toString(), tm4.toString());
			
			tm6.put(o2, alpha[i]);
			tm8.put(o4, alpha[i]);
			
			dt6.put(o2, alpha[i]);
			dt8.put(o4, alpha[i]);

			
			assertEquals(dt6, tm6);
			assertEquals(dt8, tm8);
			
			assertEquals(dt6.toString(), tm6.toString());
			assertEquals(dt8.toString(), tm8.toString());
		}
		
		assertNotEquals(dt2.toString(), dt6.toString());
		assertNotEquals(dt4.toString(), dt8.toString());

		assertEquals(dt2, tm6);
		assertEquals(tm6, dt2);
		
		assertEquals(dt2, dt6);
		assertEquals(dt4, dt8);
	}
	
	private Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> 
		makeTwins(int skipFactor) {
		SortedMap<Integer, String> dt = new BPTree<>(3);
		SortedMap<Integer, String> tm = new TreeMap<>();
		
		for(int i = 0; i < names.length; i++) {
			dt.put(i * skipFactor, names[i]);
			tm.put(i * skipFactor, names[i]);
		}
		
		return new Tuple<SortedMap<Integer,String>, SortedMap<Integer,String>>(dt, tm);

	}
	
	
	private Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> 
		randomTwins(int randOrderRange, int randLenRange) {
		
		int randOrder = (int) (Math.random() * randOrderRange) + 1, 
			randLen = (int) (Math.random() * randLenRange) + 1;
			
		SortedMap<Integer, String> tm = new TreeMap<Integer, String>();
		SortedMap<Integer, String> dt = new BPTree<Integer, String>(randOrder);
		ArrayList<String> cities = new ArrayList<>();

		for (int i = 0; i < randLen; i++) {
			cities.add(alpha[i % 26] + "" + i+1);
		}
		
		int i = 0;
		for(String city: cities) {
			dt.put(i++, city);
			tm.put(i-1, city);
		}
		
		int size = dt.size();
		for(String city: cities) {
			int rand = (int) (Math.random() * size);
			dt.put(rand, city);
			tm.put(rand, city);
		}
		
		return new Tuple<SortedMap<Integer,String>, SortedMap<Integer,String>>(dt, tm);
	}
	
	private SortedMap<Integer, String> makeDT(){
		SortedMap<Integer, String> dt = new BPTree<Integer, String>(2);
		ArrayList<String> cities = new ArrayList<>();

		char[] alpha = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 
				'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
		for (int i = 0; i < 52; i++)
			cities.add(alpha[i % 26] + "" + i+1);
		
		int i = 0;
		for(String city: cities) 
			dt.put(i++, city);

		return dt;
	}
}

// The different types of keys
class NoCompNoEquals {
	public static int i = 0;
	public int id;
	public NoCompNoEquals() {
		id = i++;
	}
	
	public NoCompNoEquals(int id) {
		this.id = id;
	}
	
	public String toString() {
		return id + "";
	}
}

class CompNoEquals implements Comparable<CompNoEquals>{
	public static int i = 0;
	public int id;
	public CompNoEquals() {
		id = i++;
	}
	
	public CompNoEquals(int id) {
		this.id = id;
	}

	public int compareTo(CompNoEquals o) {
		return Integer.compare(id, o.id);
	}
	
	public String toString() {
		return id + "";
	}
}

class NoCompEquals {
	public static int i = 0;
	public int id;
	public NoCompEquals() {
		id = i++;
	}
	
	public NoCompEquals(int id) {
		this.id = id;
	}
	
	public boolean equals(Object o) {
		if(o == null)
			return false;
		return ((NoCompEquals)o).id == id; 
	}
	
	public String toString() {
		return id + "";
	}
}

class CompEquals implements Comparable<CompEquals> {
	public static int i = 0;
	public int id;
	public CompEquals() {
		id = i++;
	}
	
	public CompEquals(int id) {
		this.id = id;
	}
	
	public boolean equals(Object o) {
		if(o == null)
			return false;
		return ((CompEquals)o).id == id; 
	}

	public int compareTo(CompEquals o) {
		return Integer.compare(id, o.id);
	}
	
	public String toString() {
		return id + "";
	}
}

class NoCompNoEqualsComparator implements Comparator<NoCompNoEquals> {
	public int compare(NoCompNoEquals o1, NoCompNoEquals o2) {
		return Integer.compare(o2.id, o1.id);
	}
}

class CompNoEqualsComparator implements Comparator<CompNoEquals> {
	public int compare(CompNoEquals o1, CompNoEquals o2) {
		return Integer.compare(o2.id, o1.id);
	}
}

class NoCompEqualsComparator implements Comparator<NoCompEquals> {
	public int compare(NoCompEquals o1, NoCompEquals o2) {
		return Integer.compare(o2.id, o1.id);
	}
}

class CompEqualsComparator implements Comparator<CompEquals> {
	public int compare(CompEquals o1, CompEquals o2) {
		return Integer.compare(o2.id, o1.id);
	}
}