package tests.part1;

import static tests.TestTools.*;

import org.junit.*;

public class Part1Tests {
	
	public Part1Tests() {
		PATH = "\\cmsc420\\meeshquest\\part2\\tests\\part1\\";
	}
	
	@Test public void dump() {
		runTest("dump", "dump.input.xml", "dump.output.xml", true);
	}
	
	@Test public void prQuad() {
		runTest("map 3", "prquad_map3.input.xml", "prquad_map3.output.xml", true);
		runTest("map 4", "prquad_map4.input.xml", "prquad_map4.output.xml", true);
	}
	
	@Test public void range() {
		runTest("prQuad1", "prquad_range1.input.xml", "prquad_range1.output.xml", false);
		runTest("prQuad2", "prquad_range2.input.xml", "prquad_range2.output.xml", true);
		runTest("prQuad", "rangeCities1.input.xml", "rangeCities1.output.xml", false);
	}
	
	@Test public void saveMap() {
		runTest("saveMap", "saveMap\\save_map1.input.xml", "saveMap\\save_map1.output.xml", false);
		runTest("saveMap", "saveMap\\save_map2.input.xml", "saveMap\\save_map2.output.xml", true);
	}
	
	@Test public void nearestCity() {
		runTest("nearestCity 1", "nearestCity\\nearestCity1.input.xml", "nearestCity\\nearestCity1.output.xml", false);
		runTest("nearestCity 2", "nearestCity\\nearestCity2.input.xml", "nearestCity\\nearestCity2.output.xml", false);
		runTest("nearestCity 3", "nearestCity\\nearestCity3.input.xml", "nearestCity\\nearestCity3.output.xml", false);
		runTest("nearestCity 4", "nearestCity\\nearestCity4.input.xml", "nearestCity\\nearestCity4.output.xml", true);
		runTest("nearestCity 5", "nearestCity\\nearestCity5.input.xml", "nearestCity\\nearestCity5.output.xml", true);
		runTest("nearestCity 6", "nearestCity\\nearestCity6.input.xml", "nearestCity\\nearestCity6.output.xml", false);
		runTest("nearestCity 7", "nearestCity\\nearestCity7.input.xml", "nearestCity\\nearestCity7.output.xml", false);
		runTest("nearestCity 8", "nearestCity\\nearestCity8.input.xml", "nearestCity\\nearestCity8.output.xml", true);
		runTest("nearestCity 9", "nearestCity\\nearestCity9.input.xml", "nearestCity\\nearestCity9.output.xml", true);
		runTest("nearestCity 10", "nearestCity\\nearestCity10.input.xml", "nearestCity\\nearestCity10.output.xml", true);
	}
	
	@Test public void public1() {
			runTest("pub1", "primary.input.xml", "primary.output.xml", false);
	}
	
	
	@Test public void public2() {
			runTest("pub2", "createCity1.input.xml", "createCity1.output.xml", false);
	}
		
	@Test public void test1() {		
		runTest("Test1", "myCity1.input.xml", "myCity1.output.xml", true);
	}
	
	@Test public void vt() {
		
		runTest("vt", "vt.input.xml", "vt.output.xml", false);
		runTest("vtDuplicate", "vt_duplicate.input.xml", "vt_duplicate.output.xml", false);
		runTest("vtRange", "vt_range.input.xml", "vt_range.output.xml", false);
	}

	@Test public void mapIsEmptyPrint() {
		runTest("mapIsEmptyPrint", "mapisempty.input.xml", "mapisempty.output.xml", true);
		runTest("mapIsEmptyPrint2", "mapisempty2.input.xml", "mapisempty2.output.xml", true);
	}
	
	@Test public void mapUnmap() {
		runTest("mapUnmap", "map_unmap.input.xml", "map_unmap.output.xml", false);
		runTest("mapUnmap2", "map_unmap2.input.xml", "map_unmap2.output.xml", false);
		runTest("mapUnmap3", "map_unmap3.input.xml", "map_unmap3.output.xml", false);
		runTest("mapUnmap4", "delete3.input.xml", "delete3.output.xml", true);
	}
	
	@Test public void clearAll() {
		runTest("clearAll", "clearAll1.input.xml", "clearAll1.output.xml", true);
	}
	
	@Test public void deleteCity() {
		runTest("deleteCity", "delete1.input.xml", "delete1.output.xml", true);
		runTest("deleteCity2", "delete2.input.xml", "delete2.output.xml", true);
		runTest("delete_unmap", "delete_unmap.input.xml", "delete_unmap.output.xml", true);
		runTest("deleteCity4", "delete4.input.xml", "delete4.output.xml", false);
		runTest("deleteCity5", "delete5.input.xml", "delete5.output.xml", false);
		runTest("deleteCity6", "delete6.input.xml", "delete6.output.xml", false);
	}
	
	@Test public void fatalError() {
		runTest("fatalError", "fatal_error1.input.xml", "fatal_error1.output.xml", true);
	}
}