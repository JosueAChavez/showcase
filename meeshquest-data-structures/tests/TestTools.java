package tests;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import view.CommandParser;

public class TestTools {

	private static String LOC = System.getProperty("user.dir");
	public static String PATH = "\\tests\\part2\\";
	
	public static void runTest(String testName, String input, String output, boolean isPart1) {
		CommandParser cp = new CommandParser();
		
		ByteArrayOutputStream baos = changeStream(input);
				
		cp.processMap();
		
		String correctOutput = getOutput(output);
		String result = new String(baos.toByteArray(), StandardCharsets.UTF_8);

		if(isPart1) {
			assertEquals(testName, result.replaceAll("\\s+",""), correctOutput.replaceAll("\\s+",""));
		} else {
			assertEquals(testName, result, correctOutput);
		}
	}
	
	public static ByteArrayOutputStream changeStream(String filename) {
		
		String file = LOC + PATH + filename;
		try {
			System.setIn(new FileInputStream(new File(file)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos);
		System.setOut(ps);
		
		return baos;
	}
	
	public static String getOutput(String filename) {
			 
		String outputFile = LOC + PATH + filename;
		
		try {
			return new String(Files.readAllBytes(Paths.get(outputFile)));
		} catch (IOException e) {
			return null;
		}
	}
}
