package tests.part3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static tests.TestTools.PATH;
import static tests.TestTools.runTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Test;

import controller.comparators.CityGraphComparator;
import model.Variables;
import model.dictionary.dt.BPTree;
import model.dictionary.dt.Tuple;
import model.mappart.cities.City;


public class Part3Tests {
	
	private final static String in = ".input.xml", out = ".output.xml"; 
	
	private final String[] names = {"Suplex_City", "Death_Valley", "Japan", "Pensocola",
			"Washington", "Jabroni_Boulevard", "Parts_Unknown", "Smackdown_Hotel",
			"Mexico_City", "Hollywood", "Kenny_Omega", "Razor_Ramon"};
	
	private final String[] alpha = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", 
			"N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
	
	public Part3Tests() {
		PATH = "\\tests\\part3\\";
	}
	
	@Test public void quickTest() {
		String id = "createCity/";
		runTest("temp", id + "input.xml", id + "output.xml", false);
	}
	
	@Test public void publicTests() {
		String u = "public\\";
		runTest("pub1", u + "part3.public.airport.input.xml", u + "part3.public.airport.output.xml", false);
		runTest("pub2", u + "part3.public.listCities.input.xml", u + "part3.public.listCities.output.xml", false);
		/*runTest("pub3", u + "part3.public.mst.input.xml", u + "part3.public.mst.output.xml", false);*/
		runTest("pub4", u + "part3.public.primary.input.xml", u + "part3.public.primary.output.xml", false);
	}

	@Test public void nonFatal() {
		String id = "nonFatal", u = id + "\\", p = u + id;
		runTest(id+"1", p+"1"+in, p+"1"+out, false);
		runTest(id+"2", p+"2"+in, p+"2"+out, false);
	}
	
	
	@Test public void mapRoadTests() {
		String id = "mapRoad", u = id + "\\", p = u + id;
		runTest(id+"A", p+"A"+in, p+"A"+out, false);
		runTest(id+"B", p+"B"+in, p+"B"+out, false);
		/*runTest(id+"C", p+"C"+in, p+"C"+out, false); // intersecting roads
		/*runTest(id+"D", p+"D"+in, p+"D"+out, false);*/ // id="2338" 
		runTest(id+"E", p+"E"+in, p+"E"+out, false);
		runTest(id+"F", p+"F"+in, p+"F"+out, false);
		runTest(id+"G", p+"G"+in, p+"G"+out, false);
	}
	
	@Test public void mapTests() {
		String id = "map", u = id + "\\", p = u + id;
		runTest(id+"1", p+"1"+in, p+"1"+out, false);
		runTest(id+"2", p+"2"+in, p+"2"+out, false);
	}
	
	@Test public void unmapTests() {
		String id = "unmap", u = id + "\\", p = u + id;
		/*runTest(id+"1", p+"1"+in, p+"1"+out, false);*/ // intersecting roads
		runTest(id+"2", p+"2"+in, p+"2"+out, false);
		runTest(id+"3", p+"3"+in, p+"3"+out, false);
		runTest(id+"4", p+"4"+in, p+"4"+out, false);
		runTest(id+"5", p+"5"+in, p+"5"+out, false); // ugly test
	}
	
	@Test public void createCityTests() {
		String id = "createCity", u = id + "\\", p = u + id;
		runTest(id+"1", p+"1"+in, p+"1"+out, false);
		runTest(id+"2", p+"2"+in, p+"2"+out, false);
	}
	
	@Test public void globalRangeCitiesTests() {
		String id = "globalRangeCities", u = id + "\\", p = u + id;
		runTest(id+"1", p+"1"+in, p+"1"+out, false);
		runTest(id+"2", p+"2"+in, p+"2"+out, false);
	}
		
	@Test public void dodekaTrieTests() {
		String id = "dodekaTrie", u = id + "\\", p = u + id;
		runTest(id+"1", p+"1"+in, p+"1"+out, false);
		runTest(id+"2", p+"2"+in, p+"2"+out, false);
		runTest(id+"3", p+"3"+in, p+"3"+out, false);
		runTest(id+"4", p+"4"+in, p+"4"+out, false);
		runTest(id+"5", p+"5"+in, p+"5"+out, false);
		runTest(id+"6", p+"6"+in, p+"6"+out, false);
	}
	
	// DoedekaTrie
	@Test public void simpleDeleteTest() {
		SortedMap<Integer, String> dt = new BPTree<>(5);
		dt.put(5, "hi");
		dt.remove(5);
	}
	
	
	@Test public void distribDeleteTest() {
		SortedMap<Integer, String> dt = new BPTree<>(5);
		dt.put(1, "A");
		dt.put(2, "B");
		
		dt.put(5, "E");
		dt.put(6, "F");
		dt.put(7, "G");
		
		// fill up left side
		dt.put(3, "C");
		dt.put(4, "D");
		
		assertEquals("G", dt.remove(7));
		
		dt = new BPTree<>(5);
		
		dt.put(1, "A");
		dt.put(2, "B");
		dt.put(3, "C");
		
		dt.put(4, "D");
		dt.put(5, "E");
		
		// fill up right side
		dt.put(6, "F");
		dt.put(7, "G");
		
		assertEquals("B", dt.remove(2));
	}
	
	@Test public void internalToLeafDeleteTest() {
		SortedMap<Integer, String> dt = new BPTree<>(5);
		dt.put(1, "A");
		dt.put(2, "B");
		dt.put(3, "C");
		dt.put(4, "D");
		dt.put(5, "E");

		// should case splitting
		dt.put(6, "death");
		
		assertEquals("death", dt.remove(6));
		
		dt = new BPTree<>(5);
		dt.put(1, "A");
		dt.put(2, "B");
		dt.put(3, "C");
		dt.put(4, "D");
		dt.put(5, "E");

		// should case splitting
		dt.put(6, "death");
		
		assertEquals("A", dt.remove(1));
		
	}
	
	private SortedMap<Integer, String> fillTree(int upto, int order) {
		SortedMap<Integer, String> dt = new BPTree<>(order);
		for(int i = 1; i <= upto; i++)
			dt.put(i, "A" + i);
		return dt;
	}
	
	private void rangeDelete(int from, int upto, int order, boolean isEmpty) {
		SortedMap<Integer, String> dt;
		for(int i = from; i <= upto; i++) {
			/*System.err.println("--");*/
			dt = fillTree(upto, order);
			
			for(int j = from; j <= upto; j++) {
				if(j != i) {
					/*System.err.println("removing " + j + " -> " + dt.get(j));*/
					assertEquals("A"+j, dt.remove(j));
				}
			}
			/*System.err.println("removing " + i + " -> " + dt.get(i));*/
			assertTrue(dt.remove(i).equals("A"+i));
			if(isEmpty) assertTrue(dt.isEmpty());
		}
	}
	
	@Test public void JustOneTest() {
		int upto = 20, order = 4;
		rangeDelete(1, upto, order, true);
	}
	
	@Test public void createDestroyReversedTest() {
		int upto = 20, order = 4;
		SortedMap<Integer, String> dt;
		
		for(int i = upto; i >= 1; i--) {
			/*System.err.println("--");*/
			dt = fillTree(upto, order);
			
			for(int j = upto; j >= 1; j--) {
				if(j != i) {
					/*System.err.println("removing " + j + " -> " + dt.get(j));*/
					assertEquals("A"+j, dt.remove(j));
				}
			}
			/*System.err.println("removing " + i + " -> " + dt.get(i));*/
			assertTrue(dt.remove(i).equals("A"+i));
			assertTrue(dt.isEmpty());
		}
	}
	
	@Test public void createDestroyMiddleTest() {
		int upto = 20, order = 4;		
		rangeDelete(upto/2, upto, order, false);
		rangeDelete(1, (upto/2)-1, order, true);
	}
	
	
	@Test public void deleteTest() {
		
		Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> t = randomTwins(10000, 10000);
		SortedMap<Integer, String> dt = t.getKey();
		SortedMap<Integer, String> tm = t.getValue();
		
		assertEquals(dt, tm);
		Set <Integer> iterator = new HashSet<>(tm.keySet()); 
		
		for(Integer key: iterator) {
			tm.remove(key);
			dt.remove(key);
			assertEquals(tm, dt);
			assertEquals(dt, tm);
		}
		
		assertTrue(dt.isEmpty());
		
		dt.put(1, "done");
		assertTrue(!dt.isEmpty());
	}
	
	@Test public void theRandomTest() {
		
		int randOrderRange = (int) ((Math.random() * 10) + 1),
			randLenRange = (int) ((Math.random() * 200) + 1);
		
		double probInsert = Math.random();
		
		Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> t = randomTwins(randOrderRange, randLenRange);
		SortedMap<Integer, String> dt = t.getKey();
		SortedMap<Integer, String> tm = t.getValue();
		
		assertEquals(dt, tm);
		ArrayList<Integer> list = Collections.list(Collections.enumeration(tm.keySet()));
		Collections.shuffle(list);
		Iterator<Integer> itr = list.iterator();
		
		
		System.err.println("randOrderRange = " + randOrderRange +
				" | randLenRange = "  + randLenRange + " | P(insert) = " + probInsert);

		while(itr.hasNext()) {
			Integer key = itr.next();
			double flip = Math.random();
			
			if(Double.compare(flip, probInsert) <= 0) {
				/*System.err.println("inserting " + flip);*/
				int randKey = (int) (Math.random() * randOrderRange);
				String randVal = alpha[(int) (Math.random() * alpha.length)] + (int) (Math.random() * randOrderRange);
				dt.put(randKey, randVal);
				tm.put(randKey, randVal);
			} else {
				/*System.err.println("deleting " + flip);*/
				assertEquals(dt.remove(key), tm.remove(key));
				assertEquals(dt, tm);
				itr.remove();
			}
		}
		
		list = Collections.list(Collections.enumeration(tm.keySet()));
		
		// clean up
		for(Integer key : list) {
			assertEquals(dt.remove(key), tm.remove(key));
			assertEquals(dt, tm);
		}
		assertTrue(dt.isEmpty());
		dt.put(1, "done");
		assertTrue(!dt.isEmpty());
	}
	
	@Test public void slickTest() {
		int randOrderRange = (int) ((Math.random() * 10) + 1),
			randLenRange = (int) ((Math.random() * 200) + 1);
			
			Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> t = randomTwins(randOrderRange, randLenRange);
			SortedMap<Integer, String> dt = t.getKey(), tm = t.getValue();
			Set<Map.Entry<Integer, String>> dtes = dt.entrySet(), tmes = tm.entrySet();
			Iterator<Entry<Integer, String>> itr = dtes.iterator(), itr2 = tmes.iterator();

			while(itr.hasNext() && itr2.hasNext()) {
				Entry<Integer, String> ts = itr.next();
				Entry<Integer, String> ts2 = itr2.next();
				
				System.err.println(ts);
				assertEquals(ts, ts2);
				assertEquals(dtes.toString(), tmes.toString());
				assertTrue(itr.hasNext() == itr2.hasNext());

				assertEquals(dtes, tmes);
				itr.remove();
				itr2.remove();
				/*System.err.println(dtes);*/
				itr.hasNext();
				/*itr.remove();
				System.err.println(dtes);*/
			}
	}
	
	@Test public void entrySetIteratorRemoveTest() {
		
		int randOrderRange = (int) ((Math.random() * 10) + 1),
			randLenRange = (int) ((Math.random() * 10000) + 1);
		
		double probInsert = Math.random();
		
		Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> t = randomTwins(randOrderRange, randLenRange);
		SortedMap<Integer, String> dt = t.getKey(), tm = t.getValue();
		
		int min = (int) Math.abs((Math.random() * (dt.size() + 1)) ),
			max = min + 1000;
				
		SortedMap<Integer, String> dtsm = dt.subMap(min, max), tmsm = tm.subMap(min, max);
		Set<Map.Entry<Integer, String>> dtes = dtsm.entrySet(), tmes = tmsm.entrySet();
		
		Iterator<Entry<Integer, String>> itr = dtes.iterator(), itr2 = tmes.iterator();
		
		System.err.println("randOrderRange = " + randOrderRange +
				" | randLenRange = "  + randLenRange + " | P(delete) = " + probInsert);
		System.err.println("min: " + min + " | max: "  + max + " | size: " + dt.size());

		/*System.err.println(tmes);*/
		while(itr.hasNext() || itr2.hasNext()) {
			Entry<Integer, String> entry = itr.next();
			Entry<Integer, String> entry2 = itr2.next();
			double flip = Math.random();
			/*System.err.print("\n" + entry2 + " vs " + entry + ": ");*/
			Variables.tester(!entry2.equals(entry), "\n" +  tmes + "\n" + dtes);
			
			assertEquals(entry2, entry);
			
			// delete
			if(Double.compare(flip, probInsert) <= 0) {
				/*System.err.print("delete");*/
				itr.remove();
				itr2.remove();
			}
			assertEquals(dtes, tmes);
			assertEquals(dt, tm);
			assertEquals(dtsm, tmsm);

			// change
			if(Double.compare(flip, probInsert) > 0 && 
				Double.compare(Math.random(), probInsert) <= 0) {
				/*System.err.println("change");*/
				String str = new String("DELETE"), str2 = new String("DELETE");
				assertTrue(str != str2);
				entry.setValue(str);
				entry2.setValue(str2);
				assertEquals(entry.getValue(), dt.get(entry.getKey()));
				assertEquals(entry.getValue(), tmsm.get(entry.getKey()));
			}
						
			assertEquals(dtsm, tmsm);
			assertEquals(dtes, tmes);
			assertEquals(dt, tm);
			
			Variables.tester(itr.hasNext() != itr2.hasNext(), "\n" + itr.hasNext() + " vs " + itr2.hasNext());
			assertTrue(itr.hasNext() == itr2.hasNext());
		}
		
		itr = dtes.iterator();
		itr2 = tmes.iterator();

		// verify and clean
		while(itr2.hasNext()) {
			Entry<Integer, String> entry = itr.next();
			Entry<Integer, String> entry2 = itr2.next();
			
			/*System.err.print("\n" + entry2 + " vs " + entry);*/
			
			assertEquals(entry, entry2);
			assertEquals(entry.getKey(), entry2.getKey());
			assertEquals(entry.getValue(), entry2.getValue());

			itr.remove();
			itr2.remove();
			assertEquals(dtsm, tmsm);
			assertEquals(dtes, tmes);
			assertEquals(dt, tm);
			
			assertTrue(!dtsm.containsKey(entry.getKey()));
			/*assertTrue(!dtsm.containsKey(entry2.getKey()));
			
			assertTrue(!tmsm.containsKey(entry.getKey()));
			assertTrue(!tmsm.containsKey(entry2.getKey()));*/
			
			assertTrue(!dtes.contains(entry));
			assertTrue(!tmes.contains(entry));

			
			assertTrue(!dt.containsKey(entry.getKey()));
			assertTrue(!tm.containsKey(entry.getKey()));
			
			assertEquals(dtsm.toString(), tmsm.toString());
			assertEquals(dtes.toString(), tmes.toString());
			assertEquals(dt.toString(), tm.toString());
		}
		
		assertTrue(dtes.isEmpty());
		assertTrue(tmes.isEmpty());
		
		assertTrue(dtsm.isEmpty());
		assertTrue(tmsm.isEmpty());
	}

	
	@Test public void randomDeleteTest() {
		
		Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> t = randomTwins(10, 500);
		SortedMap<Integer, String> dt = t.getKey();
		SortedMap<Integer, String> tm = t.getValue();
		
		assertEquals(dt, tm);
		ArrayList<Integer> it = Collections.list(Collections.enumeration(tm.keySet()));

		Collections.shuffle(it);
		
		for(Integer key: it) {
			/*System.out.println(dt);*/
			/*System.err.println("removing " + key + " DT: " + dt.get(key) + " TM: " + tm.get(key));*/
			
			assertEquals(dt.remove(key), tm.remove(key));
			assertEquals(dt, tm);
			
			for(Map.Entry<Integer, String> curr: dt.entrySet())
				assertTrue(key.compareTo(curr.getKey()) != 0);
		}
		
		assertTrue(dt.isEmpty());
		
		dt.put(1, "done");
		assertTrue(!dt.isEmpty());
	}
	
	@Test public void randomEntrySetDeleteTest() {
		
		Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> t = randomTwins(10, 500);
		SortedMap<Integer, String> dt = t.getKey(), tm = t.getValue();
		Set<Map.Entry<Integer, String>> dtes = dt.entrySet(), tmes = tm.entrySet();
		
		ArrayList<Integer> it = Collections.list(Collections.enumeration(tm.keySet()));
		Collections.shuffle(it);
		
		for(Integer key: it) {
			/*System.err.println(key);*/
			
			assertEquals(dtes.remove(new Tuple<Integer, String> (key, dt.get(key))), 
					tmes.remove(new Tuple<Integer, String> (key, tm.get(key))));
			assertEquals(dtes.toString(), tmes.toString());
			assertEquals(dtes, tmes);
			assertEquals(dt, tm);
			assertEquals(dt.toString(), tm.toString());
		}
		/*System.err.println(dt);*/
		assertTrue(tm.isEmpty());
		assertTrue(dt.isEmpty());
		
		dt.put(1, "done");
		assertTrue(!dt.isEmpty());
	}
	
	@Test public void subMapEntrySetDeleteTest() {
		
		Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> t = randomTwins(10, 500);
		SortedMap<Integer, String> dt = t.getKey(), tm = t.getValue();
		
		int min = (int) (Math.random() * (dt.size() + 1)),
			max = (int) (Math.random() * (dt.size() + 1));
		
		System.err.println("min: " + min + " | max: "  + max + " | size: " + dt.size());
		SortedMap<Integer, String> dtes = dt.subMap(Math.min(min, max), Math.max(min, max)),
									tmes = tm.subMap(Math.min(min, max), Math.max(min, max));
		
		System.err.println(tmes);
		for(Map.Entry<Integer, String> ent : dtes.entrySet()) {
			Integer key = ent.getKey();
			tmes.put(key, "CHANGED");
			ent.setValue("CHANGED");
			
			assertEquals(tmes, dtes);
			assertEquals(tmes.toString(), dtes.toString());
			assertEquals(tm, dt);
		}
		System.err.println(tmes);
	}
	
	@Test public void deleteTesta() {
		

		BPTree<Integer, Integer> map = new BPTree<Integer,Integer>(1);
		Set<Entry<Integer, Integer>> entrySet = map.entrySet();
		map.put(1, 1);
		Tuple<Integer,Integer> e = new Tuple<Integer, Integer>(1,1);
		entrySet.remove(e);
		System.out.println(map);
	}
	
	@Test public void deleteTest2() {
		
		Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> t = randomTwins(10, 500);
		SortedMap<Integer, String> dt = t.getKey();
		SortedMap<Integer, String> tm = t.getValue();
		
		
		
		Iterator<Entry<Integer, String>> it = dt.entrySet().iterator();
		
		while(it.hasNext()) {
			/*System.out.println(dt);*/
			/*System.err.println("removing " + key + " DT: " + dt.get(key) + " TM: " + tm.get(key));*/
			Map.Entry<Integer, String> key = it.next();
			it.remove();
			assertTrue(!dt.containsKey(key.getKey()));
		}
		
		assertTrue(dt.isEmpty());
		
		dt.put(1, "done");
		assertTrue(!dt.isEmpty());
	}
	
	@Test public void subMapDeleteTest() {
		
		Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> t = randomTwins(10, 500);
		SortedMap<Integer, String> dt = t.getKey();
		
		int min = (int) (Math.random() * (dt.size() + 1)),
			max = (int) (Math.random() * (dt.size() + 1));
		
		System.err.println("min: " + min + " | max: "  + max + " | size: " + dt.size());
		SortedMap<Integer, String> dts = dt.subMap(Math.min(min, max), Math.max(min, max));
		Iterator<Entry<Integer, String>> it = dts.entrySet().iterator();
		/*System.err.println(dts);*/
		while(it.hasNext()) {
			Map.Entry<Integer, String> key = it.next();
			it.remove();
			assertTrue(!dt.containsKey(key.getKey()));
		}
		
		assertTrue(dts.isEmpty());
	}
	
	@Test public void dtTest() {
		SortedMap<City, String> dt = new BPTree<City, String>(2, new CityGraphComparator());
		SortedMap<City, String> tm = new TreeMap<City, String>(new CityGraphComparator());

		ArrayList<City> cities = new ArrayList<>();
		for (int i = 0; i < 52; i++) {
			cities.add(new City(alpha[i % 26] + "" + i+1, 5, i+1, 3, "Electric Blue"));
			/*System.out.println(
					" <createCity id=\"" + i + "\" name=\"" + alpha[i % 26] + "" + i + 
					"\" x=\"5\" y=\"" + i + "\" radius=\"" + 3 +"\" color=\"red\"/>");*/
		}
		assertTrue(dt.toString().equals(tm.toString()));

		assertTrue(tm.equals(dt));
		assertTrue(dt.equals(tm));
		assertTrue(dt.hashCode() == tm.hashCode());

		
		// finds
		for(City city: cities) {
			assertNull(dt.get(city));
			assertNull(tm.get(city));
			
			assertTrue(tm.equals(dt));
			assertTrue(dt.equals(tm));
			assertTrue(dt.toString().equals(tm.toString()));
			assertTrue(dt.hashCode() == tm.hashCode());
		}
		
		// inserts
		for(City city: cities) {
			assertNull(dt.get(city));
			assertNull(tm.get(city));
			dt.put(city, city.getName());
			tm.put(city, city.getName());
			assertNotNull(dt.get(city));
			assertNotNull(tm.get(city));
			
			assertTrue(tm.equals(dt));
			assertTrue(dt.equals(tm));
			assertTrue(dt.toString().equals(tm.toString()));
			assertTrue(dt.hashCode() == tm.hashCode());
			assertTrue(dt.firstKey().equals(tm.firstKey()));
			assertTrue(dt.lastKey().equals(tm.lastKey()));
		}
		
		// finds again
		for(City city: cities) {
			assertTrue(dt.get(city).equals(city.getName()));
			assertTrue(tm.equals(dt));
			assertTrue(dt.equals(tm));
			assertTrue(dt.toString().equals(tm.toString()));
			assertTrue(dt.hashCode() == tm.hashCode());
		}
		
		/*System.err.println(dt);*/
	}
	
	@Test public void entrySetTest() {
		SortedMap<String, String> m = new BPTree<String, String>(4);
		m.put("Auto","Fail");
		Set<Map.Entry<String, String> > s = m.entrySet();
		m.put("F","---");
		assertTrue(s.contains(new Tuple<String, String>("F","---")));
	}
		
	@Test public void subMapTest() {
		
		int start = 15, end = 500;
		Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> t = randomTwins(10, 25);
		SortedMap<Integer, String> dt = t.getKey();
		SortedMap<Integer, String> dt3 = t.getKey();
		SortedMap<Integer, String> tm = t.getValue();
		
		assertEquals(dt, tm);
		
		SortedMap<Integer, String> dt1 = new BPTree<>(4);
		SortedMap<Integer, String> dt2 = new BPTree<>(4);
		
		dt1.put(1, "hi");
		dt1.put(2, "bye");
		
		dt2.put(1, "hi");
		dt2.put(2, "bye");

		assertTrue(dt1.subMap(1, 2).entrySet().equals(dt2.subMap(1, 2).entrySet()));
		
		SortedMap<Integer, String> dtSub =  dt.subMap(start, end);
		SortedMap<Integer, String> dtSub3 =  dt3.subMap(start, end);
		SortedMap<Integer, String> tmSub =  tm.subMap(start, end);
		
		assertTrue(dtSub3.entrySet().equals(dtSub.entrySet()));
		assertTrue(dtSub.entrySet().equals(dtSub3.entrySet()));

		assertEquals(dtSub.toString(), tmSub.toString());
		assertEquals(dtSub.entrySet().toString(), tmSub.entrySet().toString());
		assertTrue(dtSub.entrySet().equals(tmSub.entrySet()));
		
		assertTrue(dtSub.entrySet().equals(tmSub.entrySet()));
		assertTrue(tmSub.entrySet().equals(dtSub.entrySet()));
	}
		
	// test toString
	@Test public void randomTest() {
		
		int randOrder = (int) (Math.random() * 5) + 1, 
			randLen = (int) (Math.random() * 500) + 1;
			
		SortedMap<Integer, String> tm = new TreeMap<Integer, String>();
		SortedMap<Integer, String> dt = new BPTree<Integer, String>(randOrder);
		ArrayList<String> cities = new ArrayList<>();

		for (int i = 0; i < randLen; i++) {
			cities.add(alpha[i % 26] + "" + i+1);
		}
		
		int i = 0;
		for(String city: cities) {
			dt.put(i++, city);
			tm.put(i-1, city);
			assertEquals("1", dt.toString(), tm.toString());
		}
		
		int size = dt.size();
		for(String city: cities) {
			int rand = (int) (Math.random() * size);
			dt.put(rand, city);
			tm.put(rand, city);
		}
		
		assertEquals("1", dt.toString(), tm.toString());
		
		assertEquals("1", dt.toString(), tm.toString());
	}
	
	@Test public void removeSubMapTest() {
		
		SortedMap<Integer, String> dt = new BPTree<>(20);
		SortedMap<Integer, String> tm = new TreeMap<>();

		for(int i = 0; i < 100; i++) {
			dt.put(i, "A" + i);
			tm.put(i, "A" + i);
		}
		
		SortedMap<Integer, String> dtSub = dt.subMap(20, 30);
		SortedMap<Integer, String> tmSub = tm.subMap(20, 30);
		
		dtSub.remove(25);
		tmSub.remove(25);
		
		assertEquals(tm, dt);
		assertEquals(tmSub, dtSub);
	}
	
	@Test public void emtpySubMapTest() {
		
		SortedMap<Integer, String> dt = new BPTree<>(1);
		SortedMap<Integer, String> tm = new TreeMap<>();

		
		SortedMap<Integer, String> dtSub = dt.subMap(1, 10);
		SortedMap<Integer, String> tmSub = tm.subMap(1, 10);
		
		
		assertEquals(tm, dt);
		assertEquals(tmSub, dtSub);
	}
	
	@Test public void entrySetComparatorTest() {
		// without comparators
		SortedMap<NoCompNoEquals, String> dt1 = new BPTree<>(5, new NoCompNoEqualsComparator());
		SortedMap<CompNoEquals, String> dt2 = new BPTree<>(5, new CompNoEqualsComparator());
		SortedMap<NoCompEquals, String> dt3 = new BPTree<>(5, new NoCompEqualsComparator());
		SortedMap<CompEquals, String> dt4 = new BPTree<>(5, new CompEqualsComparator());
		
		SortedMap<NoCompNoEquals, String> tm1 = new TreeMap<>(new NoCompNoEqualsComparator());
		SortedMap<CompNoEquals, String> tm2 = new TreeMap<>(new CompNoEqualsComparator());
		SortedMap<NoCompEquals, String> tm3 = new TreeMap<>(new NoCompEqualsComparator());
		SortedMap<CompEquals, String> tm4 = new TreeMap<>(new CompEqualsComparator());
		
		/*SortedMap<NoCompNoEquals, String> dt5 = new BPTree<>(5);*/
		SortedMap<CompNoEquals, String> dt6 = new BPTree<>(5);
		/*SortedMap<NoCompEquals, String> dt7 = new BPTree<>(5);*/
		SortedMap<CompEquals, String> dt8 = new BPTree<>(5);
		
		/*SortedMap<NoCompNoEquals, String> tm5 = new TreeMap<>();*/
		SortedMap<CompNoEquals, String> tm6 = new TreeMap<>();
		/*SortedMap<NoCompEquals, String> tm7 = new TreeMap<>();*/
		SortedMap<CompEquals, String> tm8 = new TreeMap<>();

		
		// bulk inserts
		for(int i = 0; i < names.length; i++) {
			NoCompNoEquals o1 = new NoCompNoEquals();
			CompNoEquals o2 = new CompNoEquals();
			NoCompEquals o3 = new NoCompEquals();
			CompEquals o4 = new CompEquals();
			
			tm1.put(o1, names[i]);
			tm2.put(o2, names[i]);
			tm3.put(o3, names[i]);
			tm4.put(o4, names[i]);
			
			dt1.put(o1, names[i]);
			dt2.put(o2, names[i]);
			dt3.put(o3, names[i]);
			dt4.put(o4, names[i]);
			
			assertEquals(dt1, tm1);
			assertEquals(dt2, tm2);
			assertEquals(dt3, tm3);
			assertEquals(dt4, tm4);
			assertEquals(dt1.toString(), tm1.toString());
			assertEquals(dt2.toString(), tm2.toString());
			assertEquals(dt3.toString(), tm3.toString());
			assertEquals(dt4.toString(), tm4.toString());
			
			tm6.put(o2, names[i]);
			tm8.put(o4, names[i]);
			
			dt6.put(o2, names[i]);
			dt8.put(o4, names[i]);
			
			assertEquals(dt6, tm6);
			assertEquals(dt8, tm8);
			
			assertEquals(dt6.toString(), tm6.toString());
			assertEquals(dt8.toString(), tm8.toString());
		}
			// comparators vs comparables
			assertNotEquals(dt2.toString(), dt6.toString());
			assertNotEquals(dt4.toString(), dt8.toString());

			assertEquals(dt2, dt6);
			assertEquals(dt4, dt8);
		// bulk change values
		for(int i = 0; i < alpha.length; i++) {
			NoCompNoEquals o1 = new NoCompNoEquals(i);
			CompNoEquals o2 = new CompNoEquals(i);
			NoCompEquals o3 = new NoCompEquals(i);
			CompEquals o4 = new CompEquals(i);
			
			tm1.put(o1, alpha[i]);
			tm2.put(o2, alpha[i]);
			tm3.put(o3, alpha[i]);
			tm4.put(o4, alpha[i]);
			
			dt1.put(o1, alpha[i]);
			dt2.put(o2, alpha[i]);
			dt3.put(o3, alpha[i]);
			dt4.put(o4, alpha[i]);
			
			assertEquals(dt1, tm1);
			assertEquals(dt2, tm2);
			assertEquals(dt3, tm3);
			assertEquals(dt4, tm4);
			assertEquals(dt1.toString(), tm1.toString());
			assertEquals(dt2.toString(), tm2.toString());
			assertEquals(dt3.toString(), tm3.toString());
			assertEquals(dt4.toString(), tm4.toString());
		}
		
	}

	
	@Test public void TheTest() {

		// without comparators
		SortedMap<CompNoEquals, String> dt2 = new BPTree<>(5, new CompNoEqualsComparator());
		SortedMap<CompEquals, String> dt4 = new BPTree<>(5, new CompEqualsComparator());
		
		SortedMap<CompNoEquals, String> tm2 = new TreeMap<>(new CompNoEqualsComparator());
		SortedMap<CompEquals, String> tm4 = new TreeMap<>(new CompEqualsComparator());
		
		SortedMap<CompNoEquals, String> dt6 = new BPTree<>(5);
		SortedMap<CompEquals, String> dt8 = new BPTree<>(5);
		
		SortedMap<CompNoEquals, String> tm6 = new TreeMap<>();
		SortedMap<CompEquals, String> tm8 = new TreeMap<>();

		
		// bulk inserts
		for(int i = 0; i < names.length; i++) {
			CompNoEquals o2 = new CompNoEquals();
			CompEquals o4 = new CompEquals();
			
			tm2.put(o2, names[i]);
			tm4.put(o4, names[i]);
			
			dt2.put(o2, names[i]);
			dt4.put(o4, names[i]);
			
			assertEquals(dt2, tm2);
			assertEquals(dt4, tm4);
			assertEquals(dt2.toString(), tm2.toString());
			assertEquals(dt4.toString(), tm4.toString());
			
			tm6.put(o2, names[i]);
			tm8.put(o4, names[i]);
			
			dt6.put(o2, names[i]);
			dt8.put(o4, names[i]);
			
			assertEquals(dt6, tm6);
			assertEquals(dt8, tm8);
			
			assertEquals(dt6.toString(), tm6.toString());
			assertEquals(dt8.toString(), tm8.toString());
		}	
		
		/*System.out.println(dt2.toString()+ "\n" + dt6.toString());*/
		// comparators vs comparables
		assertNotEquals(dt2.toString(), dt6.toString());
		assertNotEquals(dt4.toString(), dt8.toString());

		assertEquals(dt2, tm6);
		assertEquals(tm6, dt2);
		
		assertEquals(dt2, dt6);
		assertEquals(dt4, dt8);
		
		// bulk change values
		for(int i = 0; i < alpha.length; i++) {
			CompNoEquals o2 = new CompNoEquals(i);
			CompEquals o4 = new CompEquals(i);
			
			tm2.put(o2, alpha[i]);
			tm4.put(o4, alpha[i]);
			
			dt2.put(o2, alpha[i]);
			dt4.put(o4, alpha[i]);
			
			assertEquals(dt2, tm2);
			assertEquals(dt4, tm4);
			assertEquals(dt2.toString(), tm2.toString());
			assertEquals(dt4.toString(), tm4.toString());
			
			tm6.put(o2, alpha[i]);
			tm8.put(o4, alpha[i]);
			
			dt6.put(o2, alpha[i]);
			dt8.put(o4, alpha[i]);

			
			assertEquals(dt6, tm6);
			assertEquals(dt8, tm8);
			
			assertEquals(dt6.toString(), tm6.toString());
			assertEquals(dt8.toString(), tm8.toString());
		}
		
		assertNotEquals(dt2.toString(), dt6.toString());
		assertNotEquals(dt4.toString(), dt8.toString());

		assertEquals(dt2, tm6);
		assertEquals(tm6, dt2);
		
		assertEquals(dt2, dt6);
		assertEquals(dt4, dt8);
	}
	
	/**
	 * 
	 * @param randOrderRange
	 * @param randLenRange
	 */
	private Tuple<SortedMap<Integer, String>, SortedMap<Integer, String>> 
		randomTwins(int randOrderRange, int randLenRange) {
		
		int randOrder = (int) (Math.random() * randOrderRange) + 1, 
			randLen = (int) (Math.random() * randLenRange) + 1;
			
		SortedMap<Integer, String> tm = new TreeMap<Integer, String>();
		SortedMap<Integer, String> dt = new BPTree<Integer, String>(randOrder);
		ArrayList<String> cities = new ArrayList<>();

		for (int i = 0; i < randLen; i++)
			cities.add(alpha[i % 26] + "" + i);
		
		int i = 0, randNum = 10;
		ArrayList<Integer> keys = new ArrayList<>();

		  
		// randomize keys
		/*for(int j = 0; j < randLen; j++) {*/
		while(i < randLen) {
			keys.add(i);
			i += (int) (Math.random() * randNum) + 1;
		}
		
		// randomize insertion order
		Collections.shuffle(keys);
		i = 0;
		for(Integer key: keys) {
			dt.put(key, cities.get(i));
			tm.put(key, cities.get(i));
			i++;
		}
		
		return new Tuple<SortedMap<Integer,String>, SortedMap<Integer,String>>(dt, tm);
	}
	
}

// The different types of keys
class NoCompNoEquals {
	public static int i = 0;
	public int id;
	public NoCompNoEquals() {
		id = i++;
	}
	
	public NoCompNoEquals(int id) {
		this.id = id;
	}
	
	public String toString() {
		return id + "";
	}
}

class CompNoEquals implements Comparable<CompNoEquals>{
	public static int i = 0;
	public int id;
	public CompNoEquals() {
		id = i++;
	}
	
	public CompNoEquals(int id) {
		this.id = id;
	}

	public int compareTo(CompNoEquals o) {
		return Integer.compare(id, o.id);
	}
	
	public String toString() {
		return id + "";
	}
}

class NoCompEquals {
	public static int i = 0;
	public int id;
	public NoCompEquals() {
		id = i++;
	}
	
	public NoCompEquals(int id) {
		this.id = id;
	}
	
	public boolean equals(Object o) {
		if(o == null)
			return false;
		return ((NoCompEquals)o).id == id; 
	}
	
	public String toString() {
		return id + "";
	}
}

class CompEquals implements Comparable<CompEquals> {
	public static int i = 0;
	public int id;
	public CompEquals() {
		id = i++;
	}
	
	public CompEquals(int id) {
		this.id = id;
	}
	
	public boolean equals(Object o) {
		if(o == null)
			return false;
		return ((CompEquals)o).id == id; 
	}

	public int compareTo(CompEquals o) {
		return Integer.compare(id, o.id);
	}
	
	public String toString() {
		return id + "";
	}
}

class NoCompNoEqualsComparator implements Comparator<NoCompNoEquals> {
	public int compare(NoCompNoEquals o1, NoCompNoEquals o2) {
		return Integer.compare(o2.id, o1.id);
	}
}

class CompNoEqualsComparator implements Comparator<CompNoEquals> {
	public int compare(CompNoEquals o1, CompNoEquals o2) {
		return Integer.compare(o2.id, o1.id);
	}
}

class NoCompEqualsComparator implements Comparator<NoCompEquals> {
	public int compare(NoCompEquals o1, NoCompEquals o2) {
		return Integer.compare(o2.id, o1.id);
	}
}

class CompEqualsComparator implements Comparator<CompEquals> {
	public int compare(CompEquals o1, CompEquals o2) {
		return Integer.compare(o2.id, o1.id);
	}
}