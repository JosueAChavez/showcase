package cmsc420.sortedmap;

import java.util.Comparator;

import model.dictionary.dt.BPTree;
public class DodekaTrie<K, V> extends BPTree<K, V> {

	public DodekaTrie(Comparator<K> c, int leafOrder) {
		super(leafOrder, c);
	}
	
	public DodekaTrie(int leafOrder) {
		super(leafOrder);
	}
}